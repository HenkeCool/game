package GameServer;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

public class ConnectionProvider implements Runnable {
	private ServerSocket providerSocket;
	private Socket connection = null;
	private ConnectionHandler handler;
	
	private boolean is_alive = true;
	private JSONObject info;
    
	public ConnectionProvider(int port){
		try {
			providerSocket = new ServerSocket(port);
			this.handler = Server.connection_handler;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		try {
			while(this.is_alive){
		        System.out.println("Provider: Waiting for connection");
		        this.connection = providerSocket.accept();
		        System.out.println("Provider: Accepting connection");
		        
				if(this.handler.checkLogin(this.connection)){
			        this.handler.addConnection(this.connection);	
		        }else{
		        	this.connection.close();
		        	System.out.println("Error: User tried to login with wrong details.");
		        }
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void terminate(){
		System.out.println("Provider: Server shutting down!");
		this.is_alive = false;
	}
}
