package GameServer;
import java.util.HashMap;
import java.util.Map;

import Http.HttpConnector;

public class ConnectionTester {

	public static void main(String args[]) {
		/* Arguments that we can receive:
		 * port
		 * max_players
		 * safety_ip_fix
		 * coments_amount
		 * planets_amount
		 * pvp
		 * powerups
		 * random_events
		 * api
		 * api_key
		 * + more
		 */
		
		
		Map<String, String> cmds = new HashMap<String, String>();
		
		for(int i = 0;i < args.length;i += 2){
			cmds.put(args[i], args[i+1]);
		}

		Server.handler = new ServerHandler();
		Server.core = new ServerCore();
		Server.console = new ServerConsole();
		Server.connection_handler = new ConnectionHandler(64, 9001);
		Server.provider = new ConnectionProvider(9001);
		Server.web_handler = new HttpConnector(80);
		
		new Thread(Server.core).start();
		new Thread(Server.provider).start();
		new Thread(Server.connection_handler).start();
	}
}