package GameServer;

import org.json.JSONObject;

import game_objects.Asteroid;
import game_objects.GameObject;
import game_objects.Planet;
import game_objects.Player;
import physics.Circle;
import physics.Polygon;
import statics.Settings;
import statics.Variables;

public class ServerCore implements Runnable{

	private ServerHandler handler;
	
	public ServerCore(){
		this.handler = Server.handler;
		this.handler.add_game_object(new Planet(200, 200, 0, new Circle(200)));
		System.out.println("Core: Initialized");
		System.out.println(this.getObjects().toString());
	}
	
	public ServerHandler getHandle(){
		return this.handler;
	}
	
	public void addObject(GameObject obj){
		this.handler.add_game_object(obj);
	}

	public void removeObject(GameObject obj){
		this.handler.remove_game_object(obj);
	}
	
	public JSONObject getObjects(){
		return this.handler.toJSON();
	}
	
	public void run(){
		//GAMELOOP BASED ON EXAMPLE BY ELI DELVENTHAL ON http://www.java-gaming.org/
		int fps = 0;
		double OPTIMAL_TIME = 1000000000 / Settings.TARGET_FPS;
		double lastLoopTime = System.nanoTime() - OPTIMAL_TIME;
		double lastFpsTime = 0;
		double sleep_time = OPTIMAL_TIME;
		
		// keep looping round til the game ends
		while (true){
			OPTIMAL_TIME = 1000000000 / Settings.TARGET_FPS;
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop
			double now = System.nanoTime();
			double updateLength = now - lastLoopTime;
			lastLoopTime = now;
			double delta = updateLength / 10000000;
			
			//Calculate sleeptime
			sleep_time = OPTIMAL_TIME - updateLength;
			if(sleep_time < 0)sleep_time = 0;
			
			// update the frame counter
			fps++;
			// update our FPS counter if a second has passed since
			// we last recorded
			if (now - lastFpsTime >= 1000000000){
				lastFpsTime = System.nanoTime();
				Variables.fps = fps;
				fps = 0;
			}
			// update the game logic
			this.handler.tick(delta);
			// draw everything
			//render(Variables.fps);
			// we want each frame to take 10 milliseconds, to do this
			// we've recorded when we started the frame. We add 10 milliseconds
			// to this and then factor in the current time to give 
			// us our final value to wait for
			// remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
			try{
				if(sleep_time > 0)Thread.sleep((long)(sleep_time / 500000));
			}
			catch(InterruptedException e){
				System.out.println("Interrupted");
			}
		}	
	}
}


