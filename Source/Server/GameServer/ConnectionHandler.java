package GameServer;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class ConnectionHandler implements Runnable{
    private ObjectInputStream in;
	private ObjectOutputStream out;
    private ServerCore server_core;
    private DatagramSocket udp_socket = null;
	
    private ConnectionSocket sockets[];
    private int max_clients = 0;
    private int clients_connected = 0;
	private boolean is_alive = true;
	private boolean is_player = true;
	private String username;
	private int index;
	private int object_pos;
	
    private Integer spots_taken[];
	private JSONObject json = new JSONObject();
	private JSONObject jsonin = new JSONObject();
    
	public ConnectionHandler(int i/*Map<String, String> args*/, int port){
		this.sockets = new ConnectionSocket[i]; // Integer.parseInt(args.get("max_players"))
		this.spots_taken = new Integer[i];
		this.max_clients = i;
		this.username = "";
		this.server_core = Server.core;
		try {
			this.udp_socket = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		while(this.is_alive){
			for(int i = 0; i < this.max_clients; i++){
				if(this.sockets[i] != null){
					if(!this.sockets[i].isAlive()){
						this.sockets[i] = null;
						this.spots_taken[i] = null;
						this.clients_connected--;
						System.out.println("Handler: Socket slot "+i+" disconnected");
						System.out.println("Handler: Total players left: "+this.clients_connected);
						try {
							this.printUsernames();
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						try {
							this.sockets[i].sendGameData(this.server_core.getObjects().toString().getBytes());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	private int getSpot(){
		boolean got_spot = false;
		
        for(int i = 0; i < this.max_clients; i++){
        	if(this.spots_taken[i] == null && !got_spot){
        		this.spots_taken[i] = 1;
        		return i;
        	}
        }
        
		return -1;
	}
	
	public void addConnection(Socket c) throws IOException{	        
        if(this.index == -1){
        	try {
				this.json.put("type", "error");
				this.json.put("msg", "Server full");
	        	this.sendMsg();
			} catch (JSONException e) {
				e.printStackTrace();
			}
        }else{
        	this.sockets[this.index] = new ConnectionSocket(c, this.udp_socket, this.server_core, this.out, this.in, this.index, this.object_pos, this.username);
       		new Thread(this.sockets[this.index]).start();
       		System.out.println("Handler: Client connected on spot: "+this.index);
       		this.clients_connected++;
        }
        
        try {
			this.printUsernames();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void printUsernames() throws JSONException{
		System.out.println("Handler: Online users:");
		for(int i = 0;i < this.max_clients-1; i++){
			if(this.sockets[i] != null){
				System.out.println("Handler: "+this.sockets[i].getApiInfo().getString("name"));
			}
		}
	}
	
	public JSONObject getApiInfo() throws JSONException{
		JSONObject temp = new JSONObject();
		int z = 0;

		for(int i = 0; i < this.max_clients; i++){
			if(this.spots_taken[i] != null){
				temp.put(z+"", this.sockets[i].getApiInfo());
				z++;
			}
		}
		
		if(temp.toString().length() < 3){
			temp.put("Error", "No players connected");
		}

		return temp;
	}
	
	public boolean checkLogin(Socket c){
		try {
			
			System.out.println("Attemting to check cred");
			
			this.out = new ObjectOutputStream(new BufferedOutputStream(c.getOutputStream()));
			System.out.println("Gotten output");
			this.out.flush();
			this.in = new ObjectInputStream(new BufferedInputStream(c.getInputStream()));
			
			System.out.println("Handler: Checking credentials!");
			this.jsonin = new JSONObject((String)this.in.readObject());
        
	        if(this.jsonin.get("type").equals("logingame")){
	        	if(/*this.jsonin.get("username").equals("cryslacks") &&*/ this.jsonin.get("password").equals("lame")){
	        		this.username = this.jsonin.getString("username");
	        		this.index = this.getSpot();
	        		this.object_pos = this.server_core.getObjects().length();
	        		this.json.put("type", "success");
	        		this.json.put("msg", "Login was successful");
	        		this.json.put("index", this.index);
	        		this.json.put("object_position", this.object_pos);
	        		this.sendMsg();
	        		this.is_player = true;
	        		return true;
	        	}
	        }else{
	        	this.json.put("type", "error");
	        	this.json.put("msg", "Failed to login");
	        	this.sendMsg();
	        	return false;
	        }
	        
		} catch (JSONException e1) {
			System.out.println("Handler: Error user tried special login!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String getUsername(int object_id){
		for(int i = 0; i < this.sockets.length; i++){
			if(this.sockets[i] != null){
				if(this.sockets[i].getObjectPos() == object_id){
					return this.sockets[i].getUsername();
				}
			}
		}
		
		return null;
	}
	
	private void sendMsg() throws IOException{
		this.out.writeObject(this.json.toString());
		this.out.flush();
    	this.json.remove("type");
    	this.json.remove("msg");
	}
	
	public void terminate(){
		System.out.println("Handler: Server shutting down!");
		this.is_alive = false;
	}
}