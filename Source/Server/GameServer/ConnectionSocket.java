package GameServer;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import game_objects.GameObject;
import game_objects.Player;
import physics.Polygon;
import physics.Vec2;
import statics.Variables;

public class ConnectionSocket implements Runnable{
	private ObjectOutputStream out;
    private ObjectInputStream in;
    private DatagramSocket udp_socket;
    
    private InetAddress ip;
    private int port;
    
    private ServerCore server_core;
    private int socket_slot;
    private int object_position;
    
    private Player player;
    private int[] pos = {0, 0, 0}; // x, y, angle
    private String uname;
    private int score;
    private boolean left;
    private boolean right;
    private boolean accelerate;
    
    private int packettest = 0; // Test x amounts of packets sent/recieved.
    
    private boolean alive; // TODO TEMP
    
	private JSONObject json = new JSONObject();
    
	public ConnectionSocket(Socket con, DatagramSocket udp, ServerCore s, ObjectOutputStream o, ObjectInputStream i, int z, int object_pos, String name) throws IOException{
		System.out.println("Socket " + this.socket_slot + ": Started");
		this.server_core = s;
		this.alive = true;
		this.out = o;
		this.in = i;
		this.socket_slot = z;
		this.uname = name;
		this.udp_socket = udp;
		
		byte[] receiveData = new byte[1024];
		DatagramPacket rd = new DatagramPacket(receiveData, receiveData.length, this.ip, this.port);
		this.udp_socket.receive(rd);
		this.ip = rd.getAddress();
		this.port = rd.getPort();
		
		Vec2[] verts = Vec2.arrayOf(8);
		verts[0].set(-9, 10);
		verts[1].set(-9, -10);
		
		verts[2].set(-10, 9);
		verts[3].set(-10, -9);
		
		verts[4].set(6, 8);
		verts[5].set(6, -8);
		
		verts[6].set(10, 2);
		verts[7].set(10, -2);
		this.player = new Player(0, 0, 0, 0, new Polygon(verts));
		this.player.owner_id = object_pos;
		this.object_position = object_pos;
		
		this.server_core.addObject(this.player);
	}
	
	public int getObjectPos(){
		return this.object_position;
	}
	
	public String getUsername(){
		return this.uname;
	}
	
	public void run(){	

/*		Calendar calendar = Calendar.getInstance();
		long lastSec = calendar.get(Calendar.SECOND);
		System.out.println("Seconds in current minute = " + calendar.get(Calendar.SECOND));
*/
		
		while(this.isAlive()){
			try {
				JSONObject temp = new JSONObject((String)this.in.readObject());
				/*calendar = Calendar.getInstance();
				long sec = calendar.get(Calendar.SECOND);
			    if (sec != lastSec) {
			    	System.out.println("ConnectionSocket "+this.socket_slot+": Running at "+this.packettest+" p/s and have "+(System.currentTimeMillis()-temp.getLong("time"))+" ping");
			    	this.packettest = 0;
			        lastSec = sec;
			    }
			    this.packettest++;
				*/
				
				if(temp.getString("type").equals("command")){
					Server.console.execute(temp.getString("command"), this.object_position);
				}else if(temp.getString("type").equals("rcon_command")){
					Server.console.execute(temp.getString("command"), this.object_position);
				}
				
				this.json = temp;
				this.sendMsg();
			} catch(SocketException e2) {
				this.alive = false;
				this.server_core.removeObject(this.player);
				System.out.println("Socket " + this.socket_slot + ": Error connection abrupted");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	/*
	public void run(){
		try {			
			System.out.println("Socket " + this.socket_slot + ": Starting listening");
			while(this.isAlive()){
				try {
					this.last_request = new JSONObject((String)this.in.readObject());
					
					if(this.last_request.length() > 0){/*
						try{
							Thread.sleep(100);
						} catch (InterruptedException e){}
						System.out.println("Socket " + this.socket_slot + ": Got msg = " +this.last_request.toString());
						
						/*switch((String)this.last_request.get("type")){
							case "movements":
								/*this.left = this.last_request.getBoolean("left");
								this.right = this.last_request.getBoolean("right");
								this.player.accelerating = this.last_request.getBoolean("accelerate");
								this.calcPos();
								break;
								
							default:
							
								if(!this.last_request.has("me"))
									this.last_request.put("me", false);
								
								((Player)this.player).accelerating = this.last_request.getBoolean("accelerating");
								
								//JSONObject tCore = this.server_core.handler.toJSON();
								//this.last_request.put("type", "game_objects.Player");
								//tCore.put((this.socket_slot+1)+"", this.last_request);
								//this.server_core.handler.fromJSON(tCore);
								/*try {
									Thread.sleep(5000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}*/
								/*	break;
						}
						
						this.json = this.server_core.getObjects();
						JSONObject temp = this.json.getJSONObject((1+this.socket_slot)+"");
						temp.put("me", true);
						this.json.put((1+this.socket_slot)+"", temp);
						System.out.println("Socket " + this.socket_slot + ": Sending "+this.json.getJSONObject((this.socket_slot+1)+"").toString());
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Socket " + this.socket_slot + ": Sent msg");
						this.sendMsg();
						this.last_alive = LocalDateTime.now().getMinute();
					}
				} catch (ClassNotFoundException | JSONException e) {
					System.out.println("Socket " + this.socket_slot + ": Error wrong class usage!" + e.getMessage());
				}
			}
	
	        this.in.close();
	        this.out.close();
	        this.connection.close();
		}catch(SocketException e2) {
			this.alive = false;
			this.server_core.removeObject(this.player);
			System.out.println("Socket " + this.socket_slot + ": Error connection abrupted");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	
	public boolean isAccelerating(){
		return this.accelerate;
	}
	
	public Player getObject(){
		return (Player)this.player;
	}
	
	public boolean isAlive(){
		/*if(this.last_alive > 56){
			if(3 < LocalDateTime.now().getMinute()){
				return false;
			}
		}else{
			if(this.last_alive+3 < LocalDateTime.now().getMinute()){
				return false;
			}
		}*/
		
		return this.alive;
	}
	
	public JSONObject getApiInfo() throws JSONException{
		JSONObject temp = new JSONObject();
		
		temp.put("index", this.socket_slot);
		temp.put("name", this.uname);
		temp.put("accelerate", this.accelerate);
		temp.put("left", this.left);
		temp.put("right", this.right);
		temp.put("score", this.score);
		temp.put("position", this.pos);
		
		return temp;
	}
	
	public void sendGameData(byte[] c) throws IOException{
		DatagramPacket dp = new DatagramPacket(c, c.length, this.ip, this.port);
		this.udp_socket.send(dp);
	}
	
	private void sendMsg() throws IOException, JSONException{
		this.json.put("time", System.currentTimeMillis());
		this.out.writeObject(this.json.toString());
		this.out.flush();
	}
}
