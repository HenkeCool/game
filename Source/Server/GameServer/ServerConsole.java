package GameServer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

import org.json.JSONException;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import bosses.Zuldrak;
import core.ServerConnector;
import enums.GAMESTATE;
import enums.MENUSTATE;
import enums.PASSIVES;
import game_objects.Asteroid;
import game_objects.ForceField;
import game_objects.GameObject;
import game_objects.GravityField;
import game_objects.Lazer;
import game_objects.PassiveField;
import game_objects.Planet;
import game_objects.Player;
import graphics_objects.Tracer;
import modules.Module;
import passives.Passive;
import physics.Circle;
import physics.Polygon;
import physics.Vec2;
import statics.Config;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class ServerConsole{
	private ArrayList<String> commands;
	private ArrayList<Tracer> tracers;
	private boolean cheats = false;
	
	public ServerConsole() {
		this.commands = new ArrayList<String>();
		this.tracers = new ArrayList<Tracer>();
		//this.load("default.cfg");
	}

	public void execute(String c, int object){
		this.execute(c, object, false);
	}
	
	public void execute(String c, int object, boolean internal){
		if(c.equals(""))
			return;
		
		String[] t = c.split(" ");
		t[0] = t[0].toLowerCase();
		
		if(t[0].equals("cheats")){
			if(t[1].equals("1") && !this.cheats){
				System.out.println(Server.connection_handler.getUsername(object)+" has turned on cheats!");
				this.cheats = true;
			}else if(t[1].equals("0") && this.cheats){
				System.out.println(Server.connection_handler.getUsername(object)+" has turned off cheats!");
				this.cheats = false;
			}
			return;
		}
		
		switch(t[0]){
			case "toggleon":
				this.toggleon(t[1], object);
				break;
			case "toggleoff":
				this.toggleoff(t[1], object);
				break;
			case "toggle":
				this.toggle(t[1], object);
				break;
			case "load":
				this.load(t[1]);
				break;
			case "module":
				((Player)Server.handler.get_game_object(object)).activateModule(Integer.parseInt(t[1]));
				break;
			case "addmodule":
				if(this.cheats || internal){
					this.addmodule(t, object);
				}else{
					System.out.println("");
					return;
				}
				break;
			case "removemodule":
				if(this.cheats || internal){
					((Player)Server.handler.get_game_object(object)).removeModule(Integer.parseInt(t[1]));
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "addpassive":
				if(this.cheats || internal){
					this.addpassive(t, object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "removepassive":
				if(this.cheats || internal){
					((Player)Server.handler.get_game_object(object)).removePassive(PASSIVES.valueOf(t[1]));
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "tp":
				if(this.cheats || internal){
					this.teleport(t, object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "give":
				if(this.cheats || internal){
					this.give(t, object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "add":
				if(this.cheats || internal){
					this.add(t, object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "remove":
				if(this.cheats || internal){
					this.remove(t[1], object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "dev":
				if(this.cheats || internal){
					this.dev(t, object);
				}else{
					//this.box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "conf":
				this.conf(t, object);
				break;
			case "set":
				this.set(t, object);
				break;
			case "var":
				this.var(t, object);
				break;
			case "tracer":
				this.tracer(t, object);
				break;
			case "cleartracers":
				this.cleartracers();
				break;
			case "connect":
				String[] temp = t[1].split(":");
				Variables.server = new ServerConnector(temp[0], Integer.parseInt(temp[1]));	
				new Thread(Variables.server).start();
				break;
			case "rcon":
				c = c.substring(5);
				try{
					Variables.server.sendCommand(c);
				} catch (NullPointerException e){
					//this.//this.box.addText("Error you are not connected to any server!");
				}
				break;
			case "echo":
				c = c.substring(5);
				//this.//this.box.addText(c);
				break;
			case "resetgraphic":
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++)
					if(Variables.handler.get_game_object(i) instanceof Planet)
						((Planet)Variables.handler.get_game_object(i)).resetGraphic();
					
				break;
			default:
				System.out.println("Error command "+c+" not found");
				break;
		}
	}
	
	private void addpassive(String[] o, int object_id) {
		try {
			Class<Passive> A = (Class<Passive>) Class.forName("passives."+o[1]);
			((Player)Server.handler.get_game_object(object_id)).addPassive(A.newInstance());
		} catch (ClassNotFoundException e) {
			//this.//this.box.addText("Error passive not found!");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	private void cleartracers() {
		for(int i = 0; i < this.tracers.size(); i++){
			Variables.handler.remove_game_graphics_object(this.tracers.get(i));
		}
	}

	private void tracer(String[] o, int object_id) {
		ArrayList<GameObject> A = this.getObjects(o[1], object_id);
		for(int i = 0; i < A.size(); i++){
			Tracer temp = new Tracer(((Player)Server.handler.get_game_object(object_id)), A.get(i));
			this.tracers.add(temp);
			Variables.handler.add_game_graphics_object(temp);
		}
	}

	private void toggle(String c, int object_id) {
		switch(c){
			case "forward":
				if(((Player)Server.handler.get_game_object(object_id)).accelerating)
					((Player)Server.handler.get_game_object(object_id)).accelerate_stop();
				else
					((Player)Server.handler.get_game_object(object_id)).accelerate_start();
				break;
			case "left":
				if(((Player)Server.handler.get_game_object(object_id)).turning_left)
					((Player)Server.handler.get_game_object(object_id)).turn_left_stop();
				else
					((Player)Server.handler.get_game_object(object_id)).turn_left_start();
				break;
			case "right":
				if(((Player)Server.handler.get_game_object(object_id)).turning_right)
					((Player)Server.handler.get_game_object(object_id)).turn_left_stop();
				else
					((Player)Server.handler.get_game_object(object_id)).turn_right_start();
				break;
			case "shoot":
				if(((Player)Server.handler.get_game_object(object_id)).shooting)
					((Player)Server.handler.get_game_object(object_id)).shoot_stop();
				else
					((Player)Server.handler.get_game_object(object_id)).shoot_start();
				break;
			default:
				break;
		}
	}

	private void toggleoff(String c, int object_id) {
//		System.out.println("Toggling off "+c+" for "+Server.connection_handler.getUsername(object_id));
		switch(c){
			case "forward":
				((Player)Server.handler.get_game_object(object_id)).accelerate_stop();
				break;
			case "left":
				((Player)Server.handler.get_game_object(object_id)).turn_left_stop();
				break;
			case "right":
				((Player)Server.handler.get_game_object(object_id)).turn_right_stop();
				break;
			case "shoot":
				((Player)Server.handler.get_game_object(object_id)).shoot_stop();
				break;
			default:
				break;
		}	
	}

	private void toggleon(String c, int object_id) {
//		System.out.println("Toggling on "+c+" for "+Server.connection_handler.getUsername(object_id));
		switch(c){
			case "forward":
				((Player)Server.handler.get_game_object(object_id)).accelerate_start();
				break;
			case "left":
				((Player)Server.handler.get_game_object(object_id)).turn_left_start();
				break;
			case "right":
				((Player)Server.handler.get_game_object(object_id)).turn_right_start();
				break;
			case "shoot":
				((Player)Server.handler.get_game_object(object_id)).shoot_start();
				break;
			default:
				break;
		}
	}
	
	private void set(String[] o, int object_id) {
		try {
			Field[] fields = Settings.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Settings.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Settings.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Settings.class, o[2]);
							break;
					}
					//this.//this.box.addText("Settings."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void var(String[] o, int object_id) {
		try {
			Field[] fields = Variables.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Variables.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Variables.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Variables.class, o[2]);
							break;
					}
					//this.//this.box.addText("Variables."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void conf(String[] o, int object_id) {
		try {
			Field[] fields = Config.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Config.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Config.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Config.class, o[2]);
							break;
					}
					//this.//this.box.addText("Config."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void addmodule(String[] o, int object_id) {
		try {
			Class<Module> A = (Class<Module>) Class.forName("modules."+o[1]);
			((Player)Server.handler.get_game_object(object_id)).addModule(A.getDeclaredConstructor(int.class).newInstance(Integer.parseInt(o[2])));
		} catch (ClassNotFoundException e) {
			//this.//this.box.addText("Error object not found!");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	private void bind(String[] o, int object_id){
		String cmd = "";
		for(int i = 2; i < o.length; i++){
			cmd += o[i]+" ";
		}
		
		cmd = cmd.substring(0, cmd.length()-1);
		
		Variables.key_input.addKey(o[1], cmd);
	}
	
	private void load(String o){
		try {
			o = "cfg/"+o;
			String[] file = o.split("\\.");
			
			String tempEx = String.join(".", file);;
			
			if(!file[file.length-1].equals("cfg")){
				tempEx += ".cfg";
			}
			
			o = tempEx;
			
			Scanner z = new Scanner(new File(o));
			while (z.hasNextLine()) {
				String[] temp = z.nextLine().split(" ");
				this.execute(String.join(" ", temp), 0);
			}
			z.close();
		} catch (FileNotFoundException e){
			//this.//this.box.addText("Config file '"+o+"' not found");
		}
	}
	
	public ArrayList<GameObject> getObjects(String o, int object_id){
		ArrayList<GameObject> A = new ArrayList<GameObject>();
		
		try {
			String[] prefixarr = o.split("-");
			if(prefixarr.length > 1){
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
					A.add(Variables.handler.get_game_object(i));
					
					if(Integer.parseInt(prefixarr[0]) <= i && i <= Integer.parseInt(prefixarr[1]))
						System.out.println("Found "+i);
					else
						A.remove(A.indexOf(Variables.handler.get_game_object(i)));
				}
			}else if(o.equals("nearest")){
				double len = 500000;
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++){

					if(Variables.handler.get_game_object_position(((Player)Server.handler.get_game_object(object_id))) != i){
						A.add(Variables.handler.get_game_object(i));
				
						if(len > ((Player)Server.handler.get_game_object(object_id)).position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position)){
							len = ((Player)Server.handler.get_game_object(object_id)).position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position);
							GameObject B = A.get(A.indexOf(Variables.handler.get_game_object(i)));
							A = new ArrayList<GameObject>();
							A.add(B);
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}	
					}
				}
			}else{
				A.add(Variables.handler.get_game_object(Integer.parseInt(o)));
			}
			return A;
	    } catch(NumberFormatException e){}catch(NullPointerException e){}
	    
		if(A.size() < 1){
			String[] prefix = o.split("#");
			if(prefix.length > 1){
				o = prefix[0];
				String[] prefixarr = prefix[1].split("-");
				
				if(prefix[1].equals("nearest")){
					double len = 500000;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o)){
							if(len > ((Player)Server.handler.get_game_object(object_id)).position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position)){
								len = ((Player)Server.handler.get_game_object(object_id)).position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position);
								GameObject B = A.get(A.indexOf(Variables.handler.get_game_object(i)));
								A = new ArrayList<GameObject>();
								A.add(B);
							}else{
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
							}
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else if(prefixarr.length > 1){
					int index = 0;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o)){
							index++;
							if(Integer.parseInt(prefixarr[0]) <= index && index <= Integer.parseInt(prefixarr[1])){}
							else
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else if(prefixarr.length < 2){
					int pre = Integer.parseInt(prefix[1]);
					int index = 0;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o))
							index++;
							if(index != pre)
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else{
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
				
						if(!A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o))
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
					}
					return A;
				}
			}
		return A;
	}
	
	private void dev(String[] o, int object_id) {
		String command = "";
		ArrayList<GameObject> A = this.getObjects(o[1], object_id);
		try {
		    
			if(o.length < 3)
				//this.//this.box.addText("Showing variables for "+A.get(0).getClass().getSimpleName());
			
			for(int x = 0; x < A.size(); x++){
				Field[] fields = A.get(x).getClass().getFields();
				for(int c = 0; c < fields.length; c++){
					if(o.length < 3){
						//this.//this.box.addText("- "+fields[c].getName());
					}else{
						if(fields[c].getName().equals(o[2])){
							System.out.println(fields[c].getType().toString());
							switch(fields[c].getType().toString()){
								case "double":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setDouble(A.get(x), Double.parseDouble(o[3]));
									break;
								case "float":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setFloat(A.get(x), Float.parseFloat(o[3]));
									break;
								case "int":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setInt(A.get(x), Integer.parseInt(o[3]));
									break;
								case "boolean":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setBoolean(A.get(x), Boolean.parseBoolean(o[3]));
									break;
								case "class java.awt.Color":
									if(o.length < 7){
										fields[c].set(A.get(x), new Color(Float.parseFloat(o[3]), Float.parseFloat(o[4]), Float.parseFloat(o[5])));
										command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to R:"+o[3]+" G:"+o[4]+" B:"+o[5];
									}else{
										command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to R:"+o[3]+" G:"+o[4]+" B:"+o[5]+" A:"+o[6];
										fields[c].set(A.get(x), new Color(Float.parseFloat(o[3]), Float.parseFloat(o[4]), Float.parseFloat(o[5]), Float.parseFloat(o[6])));
									}
									break;
								case "class physics.Vec2":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to X:"+o[3]+" Y:"+o[4];
									fields[c].set(A.get(x), new Vec2(Double.parseDouble(o[3]), Double.parseDouble(o[4])));
									break;
									
								default:
									command = "Error couldn't find "+o[2]+" in "+o[1]+"!";
									break;
							}
						}
					}
				}
			}

//			if(o.length < 3)
				//this.//this.box.addText("");
//			else
				//this.//this.box.addText(command);
			
		} catch (Exception e) {
			//this.//this.box.addText("Error object not found!");
		}
	}

	public void add(String[] o, int object_id){
		if(o.length < 3){
			//this.box.addText("Error command not completed, usage: 'add object x y'");
			return;
		}
	/*
		try {
			Class<GameObject> A = (Class<GameObject>) Class.forName("game_objects."+o[1]);
			Class[] x = A.getConstructors()[0].getParameterTypes();
			Class[] fields = new Class[x.length-1];
			Field[] args = new Field[x.length-1];
			for(int i = 0; i < x.length; i++){
				switch(x[i].getName()){
					case "int":
						fields[i] = int.class;
//						args[i] = o[i+1];
						break;
					case "":
						
				}
			}
		} catch (ClassNotFoundException e) {
			//this.//this.box.addText("Error object "+o[1]+" not found");
		}*/
		
		
		if(o[2].equals("player")){
			String[] t = o;
			if(o.length > 3){
				o = new String[5];
				o[4] = t[3];
			} else
				o = new String[4];
			o[1] = t[1];
			o[2] = String.valueOf(((Player)Server.handler.get_game_object(object_id)).get_x_cord());
			o[3] = String.valueOf(((Player)Server.handler.get_game_object(object_id)).get_y_cord());
		}
		
		if(o.length < 4){
			//this.box.addText("Error command not completed, usage: 'add object x y'");
			return;
		}
		
		switch(o[1]){
			case "lazer":
				Variables.handler.add_game_object(new Lazer(((Player)Server.handler.get_game_object(object_id)), Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0.0, 0.0, 100, 0));
				//this.box.addText("Spawned lazer on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "ff":
				Variables.handler.add_game_object(new ForceField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, new Vec2(50.0, 0.0)));
				//this.box.addText("Spawned forcefield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "gf":
				Variables.handler.add_game_object(new GravityField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, new Vec2(1, 0.0)));
				//this.box.addText("Spawned gravityfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "hf":
				Variables.handler.add_game_object(new PassiveField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, PASSIVES.Regenerate));
				//this.box.addText("Spawned healingfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "bf":
				Variables.handler.add_game_object(new PassiveField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, PASSIVES.Boost));
				//this.box.addText("Spawned boostfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "zuldrak":
				Variables.handler.add_game_object(new Zuldrak(Double.parseDouble(o[2]), Double.parseDouble(o[3])));
				//this.//this.box.addText("Spawned boss on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "asteroid":
				Vec2[] verts2 = Vec2.arrayOf( 20 );
				for (int i = 0; i < 20; i++)
				{
					verts2[i].set( Functions.random( -40, 40 ), Functions.random( -40, 40 ) );
				}
				Variables.handler.add_game_object(new Asteroid(Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0, 0, 0, new Polygon(verts2)));
				//this.//this.box.addText("Spawned asteroid on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "planet":
				Variables.handler.add_game_object(new Planet(Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0, new Circle(200)));
				//this.//this.box.addText("Spawned planet on x: "+o[2]+" y: "+o[3]);
				break;
				
			default:
				//this.//this.box.addText("Error object "+o[1]+" not found");
				break;
		}
	}
	
	private void give(String[] o, int object_id) {
		if(!this.cheats){
//			//this.//this.box.addText("Error cheats deactivated! Cant perform: "+o);
			return;
		}
		
		if(o.length < 3){
//			//this.box.addText("Error command not completed, usage: 'give item amount'");
			return;
		}
			
		switch(o[1]){
			case "hydrogen":
				((Player)Server.handler.get_game_object(object_id)).resources[0] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" hydrogen to player");
				break;
			case "carbon":
				((Player)Server.handler.get_game_object(object_id)).resources[1] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" carbon to player");
				break;
			case "oxygen":
				((Player)Server.handler.get_game_object(object_id)).resources[2] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" oxygen to player");
				break;
			case "iron":
				System.out.println("I am a humongus faggot");
				((Player)Server.handler.get_game_object(object_id)).resources[3] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" iron to player");
				break;
			case "gold":
				((Player)Server.handler.get_game_object(object_id)).resources[4] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" gold to player");
				break;
			case "uranium":
				((Player)Server.handler.get_game_object(object_id)).resources[5] += Double.parseDouble(o[2]);
//				//this.//this.box.addText("Added "+o[2]+" uranium to player");
				break;
			default:
//				//this.//this.box.addText("Error item "+o[1]+" not found");
				break;
		}
	}
	
	public String teleport(String[] o, int object_id){
		if(!this.cheats){
			return "Error cheats deactivated! Cant perform: "+o;
		}
		
		if(o.length < 3){
			return "Error command not completed, usage: 'tp x y'";
		}
		
		GameObject temp = ((Player)Server.handler.get_game_object(object_id));
		temp.set_x_cord(Double.parseDouble(o[1]));
		temp.set_y_cord(Double.parseDouble(o[2]));
		
		return "";
	}
	
	public String remove(String p, int object_id){
		if(!this.cheats){
			System.out.println("Cheats are inactive");
			return "Cheats are deactivated";
		}	
		
		return "";
	}
}
