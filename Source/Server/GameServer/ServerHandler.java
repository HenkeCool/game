package GameServer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.LinkedList;

import org.json.JSONException;
import org.json.JSONObject;

import bosses.Boss;
import enums.GAMESTATE;
import game_objects.*;
import graphics_objects.GraphicsObject;
import physics.*;
import statics.Settings;
import statics.Functions;
import statics.Variables;

public class ServerHandler {
	public double dt;
	public int iterations = 100;
	public ArrayList<Manifold> contacts = new ArrayList<Manifold>();

	public ArrayList<GameObject> game_object = new ArrayList<GameObject>();
	private ArrayList<GraphicsObject> game_graphics_object = new ArrayList<GraphicsObject>();
	
	public void tick(double delta){
		this.tick_game(delta);
	}
	
	public void add_game_object(GameObject object){
		this.game_object.add(object);
	}
	
	public void add_game_object(int i, GameObject object){
		this.game_object.add(i, object);
	}
	
	public void remove_game_object(GameObject object){
		this.game_object.remove(object);
	}
	
	public GameObject get_game_object(int index){
		GameObject g = this.game_object.get(index);
		return g;
	}
	
	public int get_game_object_size(){
		int i = this.game_object.size();
		return i;
	}
	
	public boolean get_game_object_contains(GameObject o){
		boolean b = this.game_object.contains(o);
		return b;
	}
	
	public int get_game_object_position(GameObject o){
		return this.game_object.indexOf(o);
	}
	
	public void clear_game_object(){
		this.game_object.clear();
	}
	
	
	public void add_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.add(object);
	}
	
	public void remove_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.remove(object);
	}
	
	public void clear_game_graphics_object(){
		this.game_graphics_object.clear();
	}

	
	private void tick_game(double delta){
		for(int i = 0; i < game_object.size(); i++){
			GameObject tempObject = game_object.get(i);
			tempObject.tick(delta);
		}
		
		for(int i = 0; i < game_graphics_object.size(); i++){
			GraphicsObject tempObject = game_graphics_object.get(i);
			tempObject.tick(delta);
		}
		
		step();
	}
	
	public void step(){
		// Generate new collision info
		contacts.clear();
		for (int i = 0; i < game_object.size(); ++i){
			GameObject A = game_object.get( i );

			for (int j = i + 1; j < game_object.size(); ++j){
				GameObject B = game_object.get( j );

				if (A.invMass == 0 && B.invMass == 0){
					continue;
				}

				Manifold m = new Manifold(A, B);
				m.solve();

				if (m.contactCount > 0){
					contacts.add( m );
				}
			}
		}

		// Integrate forces
		for (int i = 0; i < game_object.size(); ++i){
			integrateForces(game_object.get(i), dt);
		}

		// Initialize collision
		for (int i = 0; i < contacts.size(); ++i){
			contacts.get(i).initialize();
		}

		// Solve collisions
		for (int j = 0; j < iterations; ++j){
			for (int i = 0; i < contacts.size(); ++i){
				contacts.get(i).applyImpulse();
			}
		}
		
		for(int i = 0; i < game_object.size(); i++){
			game_object.get(i).setOrient(Functions.normalizeOrient(game_object.get(i).orient));
		}
		
		//Apply drag
		for (int i = 0; i < game_object.size(); ++i){
			GameObject tempObject = game_object.get(i);
			
			tempObject.angularVelocity *= tempObject.drag;
			
			tempObject.velocity.muli(tempObject.drag);
				
			if(tempObject.velocity.x < 0.01 && tempObject.velocity.x > -0.01){
				tempObject.velocity.x = 0;
			}
			
			if(tempObject.velocity.y < 0.01 && tempObject.velocity.y > -0.01){
				tempObject.velocity.y = 0;
			}
			
			if(tempObject.angularVelocity < 0.001 && tempObject.angularVelocity > -0.001){
				tempObject.angularVelocity = 0;
			}
		}
		
		//Register hit
//		for (int i = 0; i < contacts.size(); ++i){
//			Manifold contact = contacts.get(i);
//			if(contact.contactCount > 0){
//				contacts.get(i).A.hit(contact.B);
//				contacts.get(i).B.hit(contact.A);
//			}
//		}

		// Integrate velocities
		for (int i = 0; i < game_object.size(); ++i){
			integrateVelocity(game_object.get(i), dt);
		}

		// Correct positions
		for (int i = 0; i < contacts.size(); ++i){
			contacts.get( i ).positionalCorrection();
		}

		// Clear all forces
		for (int i = 0; i < game_object.size(); ++i){
			GameObject b = game_object.get(i);
			b.force.set( 0, 0 );
			b.torque = 0;
		}
	}

	// Acceleration
	// F = mA
	// => A = F * 1/m

	// Explicit Euler
	// x += v * dt
	// v += (1/m * F) * dt

	// Semi-Implicit (Symplectic) Euler
	// v += (1/m * F) * dt
	// x += v * dt

	// see http://www.niksula.hut.fi/~hkankaan/Homepages/gravity.html
	public void integrateForces(GameObject b, double dt){
//		if(b->im == 0.0f)
//			return;
//		b->velocity += (b->force * b->im + gravity) * (dt / 2.0f);
//		b->angularVelocity += b->torque * b->iI * (dt / 2.0f);

		if (b.invMass == 0.0f)return;

		double dts = dt * 0.5f;

		b.velocity.addsi( b.force, b.invMass * dts );
		b.angularVelocity += b.torque * b.invInertia * dts;
	}

	public void integrateVelocity(GameObject b, double dt){
//		if(b->im == 0.0f)
//			return;
//		b->position += b->velocity * dt;
//		b->orient += b->angularVelocity * dt;
//		b->SetOrient( b->orient );
//		IntegrateForces( b, dt );

		if (b.invMass == 0.0f)return;		

		b.position.addsi(b.velocity, dt);
		b.orient += b.angularVelocity * dt;
		b.setOrient(b.orient);

		integrateForces(b, dt);
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		for(int i = 0; i < game_object.size(); i++){
			GameObject tempObject = game_object.get(i);
			JSONObject o = tempObject.toJSON();
			try {
				o.put("object_type", tempObject.getClass().getName());
				json.put(Integer.toString(i), o);
			}
			catch (JSONException e){
				e.printStackTrace();
			}
		}
		
		return json;
	}
}