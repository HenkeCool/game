package Http;


import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import GameServer.ConnectionHandler;
import GameServer.Server;
import GameServer.ServerCore;

public class HttpConnector {

	public HttpConnector(int port){
		HttpServer server;
		try {
			server = HttpServer.create(new InetSocketAddress(port), 0);
			System.out.println("HttpServer: Started at " + port);
			server.createContext("/", new RootHandler());
			server.createContext("/admin", new AdminHandler(Server.core));
			server.createContext("/api", new ApiHandler(Server.core));
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
