package Http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import GameServer.ConnectionHandler;
import GameServer.ServerCore;

public class ApiHandler implements HttpHandler {

	private JSONObject test = new JSONObject();
	private JSONObject info = new JSONObject();
	private boolean valid_key = false;
	private ServerCore server_core;
	
	public ApiHandler(ServerCore s){
		this.server_core = s;
	}
    public void handle(HttpExchange he) throws IOException {
    	String temp = he.getRequestURI().toString();
    	
    	if(!temp.equals("/api")){
    		try {
				this.test = urlDecode(he.getRequestURI());
				
				if(this.test.get("key").equals("123")){
					this.info.put("Objects", this.server_core.getObjects());
					this.valid_key = true;
				}else
					this.valid_key = false;
				
			}catch (JSONException e) {
				System.out.println("Error");
			}
		}	
    	
		JSONObject response = new JSONObject();
		try {
	    	if(this.valid_key){
	  			response = this.info;
	    		this.test = new JSONObject();
			}else{
				response.put("Error", "Wrong api key");
				System.out.println("HttpServer: Someone tried using wrong api key");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
        he.sendResponseHeaders(200, response.toString().length());
        OutputStream os = he.getResponseBody();
        os.write(response.toString().getBytes());
        os.close();
    }

    private JSONObject urlDecode(URI a) throws JSONException{
    	JSONObject z = new JSONObject();
    	String s = a.getRawQuery();
    	String[] list = s.split("&", -1);

    	
    	System.out.println("HttpServer: Got a "+s+" request!");
    	
    	for(int i = 0; i < list.length; i++){
    		String[] arg = list[i].split("=", -1);
    		if(arg[1].contains(",")){
    			z.put(arg[0], arg[1].split(",", -1));
    		}else{
    			z.put(arg[0], arg[1]);
    		}
    	}
    	
		return z;
    }

}
