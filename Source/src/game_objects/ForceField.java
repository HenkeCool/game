package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import physics.Field;
import physics.Mat2;
import physics.Polygon;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class ForceField extends GameObject{
	Vec2 force;
	double radius, life;

	public ForceField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.radius = 0;
		this.life = 1;
		this.force = null;
	}
	
	public ForceField(double x_cord, double y_cord, double radius, double life, Vec2 force) {
		super(x_cord, y_cord, 0, 0, 0, 0, 0, new Field(radius));
		this.radius = radius;
		this.force = force;
		this.life = life * 60;
	}

	public void tick(double delta) {
		this.life -= delta;
		if(this.life <= 0)
			Variables.handler.remove_game_object(this);	
	}

	public void render(Graphics g) {
		g.setColor(Color.green);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int)radius, real_y - (int)radius, (int)radius * 2, (int)radius * 2);
		if(Settings.dev_mode)
			g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
	}

	public void hit(GameObject o) {
		Vec2 pos_delta = o.position.sub(position);
		if(pos_delta.length() < radius){
			Mat2 mat = new Mat2(Math.atan2(pos_delta.y, pos_delta.x));
			if(o instanceof Lazer)
				o.applyForce(mat.mul(force.mul(Math.sqrt((radius * radius) - pos_delta.lengthSq()))));
			else
				o.applyForce(mat.mul(force.mul(0.001*o.mass * Math.sqrt((radius * radius) - pos_delta.lengthSq()))));
		}
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			json.put("force", force.toJSON());
			json.put("radius", radius);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}
	
}
