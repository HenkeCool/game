package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import physics.Field;
import physics.Mat2;
import physics.Polygon;
import physics.Vec2;
import statics.Functions;
import statics.Variables;

public class PointField extends GameObject{
	Vec2 force;
	double radius, life;
	int i = 0;
	GameObject last_hit;

	public PointField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.radius = 0;
		this.life = 1;
		this.force = null;
		this.i = 0;
		this.last_hit = null;
	}
	
	public PointField(double x_cord, double y_cord, double radius) {
		super(x_cord, y_cord, 0, 0, 0, 0, 0, new Field(radius));
		this.radius = radius;
	}

	public void tick(double delta) {}

	public void render(Graphics g) {}

	public void hit(GameObject o) {
		this.last_hit = o;
	}

	public boolean hitReady(){
		return this.last_hit != null;
	}
	
	public GameObject getHit(){
		Variables.handler.remove_game_object(this);
		return this.last_hit;
	}
	public JSONObject toJSON() {return new JSONObject();}
	
}
