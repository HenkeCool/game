package game_objects;

import java.awt.Graphics;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import physics.Field;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class CheckField extends GameObject {
	public ArrayList<GameObject> hits;
	
	public CheckField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.hits = new ArrayList<GameObject>();
	}
	
	public CheckField(double x_cord, double y_cord, double radius) {
		super(x_cord, y_cord, 0, 0, 0, 1, 0.99, new Field(radius));
		this.hits = new ArrayList<GameObject>();
	}

	public void tick(double delta) {}

	public void render(Graphics g) {
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());

		/*g.setColor(Color.red);
		g.drawOval(real_x - (int) radius, real_y - (int) radius, (int) radius * 2, (int) radius * 2);
		g.setColor(Color.red);
		g.drawString("Remover", real_x, real_y);*/
		if(Settings.dev_mode)
			g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
	}

	public void hit(GameObject o) {
		if(!this.hits.contains(o)){
			this.hits.add(o);
		}
	}
	
	public ArrayList<GameObject> getHits(){
		return this.hits;
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			// json.put("force", force.toJSON());
			json.put("radius", this.shape.radius);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}