package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import enums.PASSIVES;
import graphics_objects.GeoStationaryPopUpText;
import graphics_objects.PopUpText;
import physics.Circle;
import physics.Polygon;
import physics.Shape;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class Shield extends GameObject{
	public double health;
	public GameObject entity;
	
	private boolean quickfix = true;
	
	public Shield(GameObject e, double x_cord, double y_cord, double health) {
		super(x_cord, y_cord, 0, 0, 0, 1, 1, createShield());
		this.health = health;
		this.entity = e;
	}

	static private Polygon createShield(){
		Vec2[] verts = Vec2.arrayOf(5);
		
		verts[0].set(0, 45);
		verts[1].set(0, -45);
		verts[2].set(15, -20);
		verts[3].set(15, 20);
		
		return new Polygon(verts);
	}
	
	public void tick(double delta) {
		this.set_angle(this.entity.get_angle());
		this.set_x_cord((this.entity.get_x_cord()+Math.cos(this.get_angle())*(50+this.entity.velocity.x))+Math.cos(this.get_angle()));
		this.set_y_cord((this.entity.get_y_cord()+Math.sin(this.get_angle())*(50+this.entity.velocity.y))+Math.sin(this.get_angle()));
				
		if(this.health <= 0)
			Variables.handler.remove_game_object(this);
	}

	public void render(Graphics g) {
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){
			g.setColor(Color.green);
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
		
			g.drawLine(real_x, real_y, (int)(real_x + (10 * Math.cos(get_angle()))), (int)(real_y + (10 * Math.sin(get_angle()))));
			if(Settings.dev_mode)
				g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
		}
		
	}

	public void hit(GameObject object) {
		if(this.quickfix){
			this.quickfix = false;
			return;
		}
		
		if(object instanceof Lazer && !((Lazer)object).entity.equals(this.entity))
			this.health -= ((Lazer)object).damage;
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
}
