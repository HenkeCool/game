package game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import enums.PASSIVES;
import passives.*;
import physics.Circle;
import physics.Field;
import physics.Mat2;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class FollowingDropperField extends GameObject {
	PASSIVES name;
	double radius, life, max_width;
	GameObject entity;
	Vec2 force;
	int i = 0;

	public FollowingDropperField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.name = null;
		this.radius = 0;
		this.life = 1;
		this.max_width = 0;
		this.entity = null;
		this.force = null;
		this.i = 0;
	}
	
	public FollowingDropperField(double x_cord, double y_cord, double radius, double life, PASSIVES name, GameObject s) {
		super(x_cord, y_cord, 0, 0, 0, 2, 0.50, new Field(radius));
		this.radius = radius;
		this.name = name;
		this.life = life;
		this.entity = s;
	}

	public void tick(double delta) {
		if (this.i >= 60) {
			this.life -= delta;
			if (this.life <= 0)
				Variables.handler.remove_game_object(this);
			Variables.handler.add_game_object(new PassiveField(this.get_x_cord(), this.get_y_cord(), this.radius, 9999, this.name));

			this.i = 0;
		}


		Vec2 f = (this.entity.position.sub(position).sub(new Vec2(5, 0))).mul(0.5 * this.entity.mass);
		applyForce(f);
		
		this.i++;
	}

	public void render(Graphics g) {
		g.setColor(Color.pink);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int) radius, real_y - (int) radius, (int) radius * 2, (int) radius * 2);
		if(Settings.dev_mode)
			g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
	}

	public void hit(GameObject o) {
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			// json.put("force", force.toJSON());
			json.put("radius", radius);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}