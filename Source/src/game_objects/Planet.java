package game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import enums.GAMESTATE;
import enums.RESOURCETYPE;
import graphics_objects.Explosion;
import graphics_objects.OpenSimplexNoise;
import physics.Circle;
import physics.Field;
import menu_objects.HorizontalBox;
import menu_objects.ResourceButton;
import menu_objects.VerticalBox;
import menu_objects.TextButton;
import menu_objects.TextItem;
import physics.Shape;
import statics.Settings;
import statics.Config;
import statics.Functions;
import statics.Variables;

public class Planet extends GameObject{
	public boolean hit;
	private BufferedImage graphic;
	public int seed;
	private double[] sellability;
	private double[] baseprice;
	private double trade_variance;
	public RESOURCETYPE[] resources;

	public Planet(){
		super(0, 0, 0, 0, 0, 0, 0, new Circle(0));
		this.force = null;
		this.hit = false;
		this.seed = 0;
	}

	public Planet(double x_cord, double y_cord, double angle, Shape shape) {
		super(x_cord, y_cord, 1, 1, angle, 10, 0.9, shape);
		
		//this.setStatic();
		
		this.hit = false;
		this.seed = Functions.random(0, 10000);
		this.graphic = generateGraphic(seed, this.shape.radius, 0);
		
		Random r = new Random(seed);
		
		this.trade_variance = (r.nextDouble() * 0.5 + 0.1) * (shape.radius - 100) / 300;
		
		this.sellability = new double[6];
		this.baseprice = new double[6];
		
		for(int i = 0; i < Config.base_prices.length; i++){
			baseprice[i] = Config.base_prices[i] * (r.nextDouble() + 0.5);
		}
	}

	public void tick(double delta) {
		this.hit = false;
		
		if(get_x_cord() <= -11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_x_cord() >= 11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_y_cord() <= -11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_y_cord() >= 11000){
			Variables.handler.remove_game_object(this);
		}
	}

	public void render(Graphics g){
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){
			if(this.graphic == null)
				this.resetGraphic();
			
			BufferedImage rotated_graphic = this.graphic;
			
			AffineTransform transform = new AffineTransform();
		    transform.rotate(this.orient, rotated_graphic.getWidth()/2, rotated_graphic.getHeight()/2);
		    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		    rotated_graphic = op.filter(rotated_graphic, null);
			
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
			g.drawImage(rotated_graphic, (int)(real_x - this.shape.radius), (int)(real_y  - this.shape.radius), null);
			if(Settings.dev_mode)
				g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
		}
	}

	public void hit(GameObject object) {
		this.hit = true;
	}
	
	private static BufferedImage generateGraphic(int seed, double radius, double i) {
		BufferedImage image = new BufferedImage((int)radius * 2, (int)radius * 2, BufferedImage.TYPE_4BYTE_ABGR);
		
		Random r = new Random(seed);
		
		int cr = r.nextInt(256);
		int cg = r.nextInt(256);
		int cb = r.nextInt(256);
		
		Color color = new Color(cr, cg, cb);
		
		cr *= r.nextDouble() + 0.5;
		cg *= r.nextDouble() + 0.5;
		cb *= r.nextDouble() + 0.5;
		
		if(cr > 255)cr = 255;
		if(cg > 255)cg = 255;
		if(cb > 255)cb = 255;
		
		Color color2 = new Color(cr, cg, cb);
		
		OpenSimplexNoise osn = new OpenSimplexNoise(seed);
		double modifierx = r.nextDouble() * 5 + radius / 5;
		double modifiery = modifierx;
		
		double limit1 = 0.15;
		double limit2 = 0.05;
		
		if(r.nextBoolean()){
			modifierx *= r.nextInt(8) + 4;
			modifiery /= 2;
		}
		
		for(int x = 0; x < image.getWidth(); x++){
			for(int y = 0; y < image.getWidth(); y++){
				double distance = Math.sqrt((x - radius) * (x - radius) + (y - radius) * (y - radius)) * 1.002;
				
				if(distance > radius){
					image.setRGB(x, y, 0);
				}
				else{
					Color c;
					float noise = (float)osn.eval(x / modifierx, y / modifiery, i);
					
					if(noise > limit1){
						c = color;
					}
					else if (noise > limit2){
						float alpha = (float)(1 - (noise - limit2) / (limit1 - limit2)); 
						c = Functions.blend(color, color2, alpha);
					}
					else{
						c = color2;
					}
					
					float red = (float)c.getRed() / 255;
					float green = (float)c.getGreen() / 255;
					float blue = (float)c.getBlue() / 255;
					
					double q = (((double)r.nextInt(81) + 960) / 1000);
					double s = (Math.sqrt(radius * radius - distance * distance) / radius);
					
					red *= q;
					green *= q;
					blue *= q;
					
					red *= s;
					green *= s;
					blue *= s;
					
					if(red > 1)red = 1;
					if(green > 1)green = 1;
					if(blue > 1)blue = 1;
					
					image.setRGB(x, y, new Color(red, green, blue).getRGB());
				}
			}
		}
		return image;
	}
	
	public void resetGraphic(){
		this.graphic = generateGraphic(this.seed, this.shape.radius, 0);
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
	
	public double get_max_trade(RESOURCETYPE r){
		return Math.sqrt(-5 + Math.sqrt(25 + 10 * baseprice[r.ordinal()] / sellability[r.ordinal()]));
	}
	
	public int trade(RESOURCETYPE r, double mass){
		double sellability = this.sellability[r.ordinal()];
		double baseprice = this.baseprice[r.ordinal()];
		
		double amount = sellability + mass;
		
		Variables.player.resources[r.ordinal()] -= mass;
		
		double value = baseprice * amount - (trade_variance * amount * amount * amount * 0.33) - (trade_variance * 0.1 * amount * amount * amount * amount * amount * 0.2) - (baseprice * sellability - (trade_variance * sellability * sellability * sellability * 0.33) - (trade_variance * 0.1 * sellability * sellability * sellability * sellability * sellability * 0.2));
		Variables.player.credits += value;
		this.sellability[r.ordinal()] += mass;
		
		System.out.println(this.sellability[r.ordinal()]);
		
		return (int)value;
	}

	public void open_trade() {
		VerticalBox frame = new VerticalBox(0, 0, 400, 0, 2, new Color(200, 200, 200), new Color(100, 100, 100));
		
		HorizontalBox window = new HorizontalBox(0, 0, 0, 0, Color.white, Color.white);
		VerticalBox[] lists = {
			new VerticalBox(0, 0, 9999999, 5, 0, new Color(0,0,0,0), new Color(0,0,0,0)),
			new VerticalBox(0, 0, 9999999, 5, 0, new Color(0,0,0,0), new Color(0,0,0,0)),
			new VerticalBox(0, 0, 9999999, 5, 0, new Color(0,0,0,0), new Color(0,0,0,0)),
			new VerticalBox(0, 0, 9999999, 5, 0, new Color(0,0,0,0), new Color(0,0,0,0))
		};
		
		for(int i = 0; i < 2; i++){
			VerticalBox list = lists[i];
			list.add(new TextButton(0, 0, 300, 50, "Close", Color.cyan, 20f){
				@Override
				public void on_click(MouseEvent e) {
					Variables.menu_handler.active_list.remove(frame);
					Variables.game_state = GAMESTATE.Game;
				}
			});
			
			for(int o = 0; o < 10; o++){
				list.add(new ResourceButton(0, 0, 300, 50, Color.gray, Functions.randomEnum(RESOURCETYPE.class), this));
			}
			
			window.add(list);
		}
		frame.add(window);
		
		Variables.menu_handler.load(frame);
	}
}
