package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import physics.Field;
import physics.Mat2;
import physics.Polygon;
import physics.Vec2;
import statics.Functions;
import statics.Variables;

public class Blob extends GameObject{
	public double radius;
	public int damage;


	public Blob(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.radius = 0;
		this.radius = 0;
	}
	
	public Blob(double x_cord, double y_cord, double radius, int damage) {
		super(x_cord, y_cord, 0, 0, 0, 0, 0, new Field(radius));
		this.radius = radius;
		this.damage = damage;
	}

	public void tick(double delta) {}

	public void render(Graphics g) {
		g.setColor(Color.blue);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int)radius, real_y - (int)radius, (int)radius * 2, (int)radius * 2);
	}

	public void hit(GameObject o) {
		if(o instanceof PassiveFieldRemoverField)
			Variables.handler.remove_game_object(this);
		
		if(o instanceof Player){
			o.health -= this.damage;
			Variables.handler.remove_game_object(this);
		}
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			json.put("force", force.toJSON());
			json.put("radius", radius);
			json.put("damage", this.damage);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}
	
}
