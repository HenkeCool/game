package game_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import enums.MODULES;
import enums.PASSIVES;
import graphics_objects.HealthBar;
import graphics_objects.PlayerPopUpText;
import modules.Module;
import passives.Passive;
import physics.*;
import statics.Settings;
import statics.Functions;
import statics.Variables;

public class Player extends GameObject {
	public Color color;
	public int owner_id;
	
	public int score;
	public boolean turning_left = false;
	public boolean turning_right = false;
	public boolean accelerating = false;
	public boolean shooting = false;
	public boolean hit;

	public int credits;

	public double mass_limit;
	public int lazerdamage;
	public int lazerlife;
	public double lazerspeed;
	public double accelerate_speed;
	public double turning_speed;

	public double inventory_mass;
	
	public double[] resources;

	public Module module_list[];
	public ArrayList<Passive> passive_list;

	public Player(){
		super(0, 0, 0, 0, 0, 0, 0, new Polygon());
		this.color = Color.white;
		this.score = 0;
		this.credits = 0;
		this.mass_limit = 0;
		this.lazerdamage = 0;
		this.lazerlife = 0;
		this.lazerspeed = 0;
		this.accelerate_speed = 0;
		this.turning_speed = 0;
		this.inventory_mass = 0;
		this.module_list = new Module[5];
		this.passive_list = new ArrayList<Passive>();
		this.owner_id = 0;
	}
	
	public Player(double x_cord, double y_cord, double x_vel, double y_vel, Shape shape) {
		super(x_cord, y_cord, x_vel, y_vel, 0, 1, 0.99, shape);
		this.hit = false;
		this.color = Color.white;
		this.mass_limit = 2000;
		this.lazerdamage = 250;
		this.lazerlife = 50;
		this.lazerspeed = 25;
		this.module_list = new Module[5];
		this.passive_list = new ArrayList<Passive>();
//		Variables.handler.add_game_graphics_object(new HealthBar(this, 100, Color.green));
		this.accelerate_speed = 0.2;
		this.turning_speed = 0.05;
		this.health = 10000;
		
		this.resources = new double[6];
	}

	public void tick(double delta) {
		this.color = Color.white;
		
		if(this.velocity.length() > 0)
			this.lazerspeed = this.velocity.length()+25;
		
		if(Functions.randInt(1, 61) == 1){
			Vec2[] verts2 = Vec2.arrayOf( 20 );
			for (int i = 0; i < 20; i++)
			{
				verts2[i].set( Functions.random( -40, 40 ), Functions.random( -40, 40 ) );
			}
			
			/*if(Variables.handler != null)
				if(Variables.handler.get_game_object_size() < 250)
					Variables.handler.add_game_object(new Asteroid(Functions.randInt(-9999, 9999), Functions.randInt(-9999, 9999), 0, 0, 0, new Polygon(verts2)));
*/		}
		
		if(Variables.player != null && Variables.player.velocity.lengthSq() > 10000){
			Variables.player.velocity = new Vec2(0, 0);
		}

		for (int i = 0; i < module_list.length; i++) {
			if (module_list[i] != null)
				module_list[i].tick(delta);
		}

		for (int i = 0; i < passive_list.size(); i++) {
			if (passive_list.get(i) != null)
				passive_list.get(i).tick(delta);
		}

		if (turning_right) {
			// shape.u = (new Mat2(get_angle()).mul(new Mat2((double)Math.PI /
			// 180f)));
			// orient = get_angle() + (double)Math.PI / -90f;
			angularVelocity = -this.turning_speed;
		} else if (turning_left) {
			// shape.u = (new Mat2(get_angle()).mul(new Mat2((double)Math.PI /
			// -180f)));
			// orient = get_angle() + (double)Math.PI / 90f;
			angularVelocity = this.turning_speed;
		} else {
			angularVelocity = 0;
		}

		if (accelerating) {
			applyForce(shape.u.mul(new Vec2(this.mass * this.accelerate_speed, 0)));
		}

		if (shooting) {
			Variables.handler.add_game_object(new Lazer(this, get_x_cord()+Math.cos(this.get_angle()), get_y_cord()+Math.sin(this.get_angle()), this.lazerspeed, get_angle(), this.lazerlife, this.lazerdamage));
		}

		// Calculate inventory mass
		calculate_inventory_mass();

		// Hit detection

		if (get_x_cord() <= -10000) {
			System.out.println("Hit left");
			set_x_cord(-9999);
			set_x_vel(0);
		}

		if (get_x_cord() >= 10000) {
			System.out.println("Hit right");
			set_x_cord(9999);
			set_x_vel(0);
		}

		if (get_y_cord() <= -10000) {
			System.out.println("Hit top");
			set_y_cord(-9999);
			set_y_vel(0);
		}

		if (get_y_cord() >= 10000) {
			System.out.println("Hit bottom");
			set_y_cord(9999);
			set_y_vel(0);
		}
	}

	public void render(Graphics g) {
		if (Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(),
				this.get_y_cord()) < 760) {
			Font currentFont = g.getFont();
			Font newFont = currentFont.deriveFont(12f);
			g.setFont(newFont);

			g.setColor(this.color);
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());

			g.fillRect(real_x - 5, real_y - 5, 10, 10);
			g.drawLine(real_x, real_y, (int) (real_x + (20 * Math.cos(get_angle()))),
					(int) (real_y + (20 * Math.sin(get_angle()))));
			
			if(Settings.draw_debug_information){
				g.setColor(Color.red);
				g.drawString("x: " + (int) get_x_cord(), 10, 30);
				g.drawString("y: " + (int) get_y_cord(), 10, 50);

				g.setColor(Color.green);
				g.drawString("Health: " + Math.round(this.health), 10, 110);

				g.setColor(Color.red);
				g.drawString("Hydrogen: " + (int) resources[0], Settings.WIDTH - 120, Settings.HEIGHT - 175);
				g.drawString("Carbon: " + (int) resources[1], Settings.WIDTH - 120, Settings.HEIGHT - 160);
				g.drawString("Oxygen: " + (int) resources[2], Settings.WIDTH - 120, Settings.HEIGHT - 145);
				g.drawString("Iron: " + (int) resources[3], Settings.WIDTH - 120, Settings.HEIGHT - 130);
				g.drawString("Gold: " + (int) resources[4], Settings.WIDTH - 120, Settings.HEIGHT - 115);
				g.drawString("Uranium: " + (int) resources[5], Settings.WIDTH - 120, Settings.HEIGHT - 100);
				
				g.setColor(Color.yellow);
				g.drawString("Credits: " + (int) credits, Settings.WIDTH - 120, Settings.HEIGHT - 85);

				g.setColor(Color.orange);
				g.drawString("Module list: ", Settings.WIDTH - 120, 20);
				
				for(int i = 0; i < module_list.length; i++){
					if(module_list[i] != null){
						g.drawString(module_list[i].getName(), Settings.WIDTH - 120, 20+i*15);
						g.drawString(Double.toString(Math.round(module_list[i].getCooldown()/60)), Settings.WIDTH - 50, 20+i*15);
					}
				}

				g.setColor(Color.blue);
				g.drawString("Passive list: ", Settings.WIDTH - 120, 100);
				for (int i = 0; i < passive_list.size(); i++) {
					if (passive_list.get(i) != null) {
						if (passive_list.get(i).isBuff())
							g.setColor(Color.green);
						else
							g.setColor(Color.red);
						g.drawString(passive_list.get(i).getName(), Settings.WIDTH - 120, 115 + i * 15);
						g.drawString(Double.toString(Math.round(passive_list.get(i).getDuration() / 60)),
								Settings.WIDTH - 50, 115 + i * 15);
					}
				}
			}
		}
	}

	public void hit(GameObject object) {
		if(object instanceof Lazer){
			if(!((Lazer) object).entity.equals(this)){
				this.health -= ((Lazer) object).damage;
				Variables.handler.remove_game_object(object);
			}
		}
		
		this.color = Color.cyan;
		if (object instanceof Resource) {
			if (inventory_mass + object.mass < mass_limit) {
				calculate_inventory_mass();
				Variables.handler.remove_game_object(object);
				Resource resource = (Resource) object;
				resources[resource.type.ordinal()] += object.mass;
			} else {
				Variables.handler.add_game_graphics_object(
						new PlayerPopUpText("Mass too large", Color.red, 12, 100, new Vec2(0, 50)));
			}
		}
	}

	public void accelerate_start() {
		accelerating = true;
	}

	public void accelerate_stop() {
		accelerating = false;
	}

	public void turn_right_start() {
		turning_right = true;
	}

	public void turn_left_start() {
		turning_left = true;
	}

	public void turn_right_stop() {
		turning_right = false;
	}

	public void turn_left_stop() {
		turning_left = false;
	}

	public void shoot_start() {
		shooting = true;
	}

	public void shoot_stop() {
		shooting = false;
	}
	
	private void calculate_inventory_mass(){
		inventory_mass = 0;
		for(double mass: resources){
			inventory_mass += mass;
		}
	}
	
	public void activateModule(int i) {
		try {
			module_list[i].activate();
		} catch (NullPointerException e) {
			System.out.println("No module in position " + i);
		}
	}

	public void addModule(Module m) {
		for (int i = 0; i < module_list.length; i++) {
			if (module_list[i] != null)
				if (module_list[i].getName().equals(m.getName()))
					return;
		}

		module_list[m.getPosition()] = m;
	}

	public void removeModule(int i) {
		module_list[i] = null;
	}

	public void addPassive(Passive m) {
		if (!hasPassive(m))
			passive_list.add(m);
	}

	public void removePassive(PASSIVES n) {
		for (int i = 0; i < passive_list.size(); i++) {
			if (passive_list.get(i).getType() == n){
				//passive_list.get(i).remove();
				passive_list.remove(i);
			}
		}
	}

	public boolean hasPassive(Passive n) {
		for (int i = 0; i < passive_list.size(); i++) {
			if (passive_list.get(i).getType() == n.getType()){
				passive_list.remove(i);
				passive_list.add(i, n);
				return true;
			}
		}
		return false;
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
	
	public void empty_inventory() {
		resources = new double[6];
	}
}
