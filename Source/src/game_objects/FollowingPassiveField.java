package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import enums.PASSIVES;
import passives.*;
import physics.Circle;
import physics.Field;
import physics.Mat2;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class FollowingPassiveField extends GameObject {
	PASSIVES name;
	double radius, life, max_width;
	GameObject entity;
	Vec2 force;
	int i = 0;

	public FollowingPassiveField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.name = null;
		this.radius = 0;
		this.life = 1;
		this.max_width = 0;
		this.entity = null;
		this.force = null;
		this.i = 0;
	}
	
	public FollowingPassiveField(double x_cord, double y_cord, double radius, double life, PASSIVES name, GameObject s) {
		super(x_cord, y_cord, 0, 0, 0, 1, 0, new Field(radius));
		this.radius = radius;
		this.name = name;
		this.life = life;
		this.entity = s;
	}

	public void tick(double delta) {
		if (this.i >= 60) {
			this.life -= delta;
			if (this.life <= 0)
				Variables.handler.remove_game_object(this);
			Vec2 f = (this.entity.position.sub(position)).mul(1/(this.entity.position.distance(position))).mul(0.10);
			this.applyForce(f);
			this.i = 0;
		}

		this.i++;
	}

	public void render(Graphics g) {
		g.setColor(Color.cyan);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int) radius, real_y - (int) radius, (int) radius * 2, (int) radius * 2);
		g.setColor(Color.green);
		int leng = this.name.toString().length();
		g.drawString(this.name.toString(), real_x - leng / 2, real_y);
		if(Settings.dev_mode)
			g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
	}

	private Passive getPassive() {
		switch (this.name.toString()) {
		case "Boost":
			return new Boost();

		case "Regenerate":
			return new Regenerate();

		case "Corruption":
			return new Corruption();

		case "Deteriorate":
			return new Deteriorate();

		case "RocketBreakdown":
			return new RocketBreakdown();

		default:
			return new Corruption();
		}
	}

	public void hit(GameObject o) {
		if (o instanceof Player) {
			((Player) o).addPassive(this.getPassive());
		}
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			// json.put("force", force.toJSON());
			json.put("radius", radius);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}