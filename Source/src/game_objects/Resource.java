package game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import enums.RESOURCETYPE;
import physics.Circle;
import physics.Field;
import graphics_objects.OpenSimplexNoise;
import physics.Shape;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class Resource extends GameObject{
	public RESOURCETYPE type;
	private BufferedImage graphic;
	private int seed;

	public Resource(){
		super(0, 0, 0, 0, 0, 0, 0, new Circle(0));
		this.type = null;
	}
	
	public Resource(double x_cord, double y_cord, double x_vel, double y_vel, double angle, RESOURCETYPE type, Shape shape) {
		super(x_cord, y_cord, x_vel, y_vel, angle, type.getDensity(), 0.98, shape);
		this.type = type;
		
		this.seed = Functions.random(0, 10000);
		this.graphic = generateGraphic(seed, this.shape.radius, type, 0);
	}

	public void tick(double delta) {
		Player nearest_player = null;
		for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
			GameObject o = Variables.handler.get_game_object(i);
			if(o instanceof Player){
				if(nearest_player == null) nearest_player = (Player)o;
				if(position.distance(nearest_player.position) > position.distance(o.position)) nearest_player = (Player)o;
			}
		}
		if(nearest_player.position.distance(position) < 80 && nearest_player.inventory_mass + this.mass < nearest_player.mass_limit){
			Vec2 f = (nearest_player.position.sub(position)).mul(1/(nearest_player.position.distance(position))).mul(0.75 * this.mass);
			applyForce(f);
		}
	}

	public void render(Graphics g) {
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){
			BufferedImage rotated_graphic = this.graphic;
			
			AffineTransform transform = new AffineTransform();
		    transform.rotate(this.orient, rotated_graphic.getWidth()/2, rotated_graphic.getHeight()/2);
		    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		    rotated_graphic = op.filter(rotated_graphic, null);
			
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
			
			if(Settings.dev_mode)
				g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
			
			g.drawImage(rotated_graphic, (int)(real_x - this.shape.radius), (int)(real_y  - this.shape.radius), null);
		}
	}

	public void hit(GameObject object){
		
	}
	
	public JSONObject toJSON() {
		return toJSON(this);
	}
	
	private static BufferedImage generateGraphic(int seed, double radius, RESOURCETYPE resource, double i) {
		BufferedImage image = new BufferedImage((int)radius * 2, (int)radius * 2, BufferedImage.TYPE_4BYTE_ABGR);
		
		Random r = new Random(seed);
		
		double base_r = 100d / 255;
		double base_g = 75d / 255;
		double base_b = 40d / 255;
		
		double deviance = 0.1;
		
		double cr = base_r + r.nextDouble() * deviance - deviance / 2;
		double cg = base_g + r.nextDouble() * deviance - deviance / 2;
		double cb = base_b + r.nextDouble() * deviance - deviance / 2;
		
		if(cr > 1)cr = 1;
		if(cg > 1)cg = 1;
		if(cb > 1)cb = 1;
		
		if(cr < 0)cr = 0;
		if(cg < 0)cg = 0;
		if(cb < 0)cb = 0;
		
		Color color2 = new Color((int)(cr * 255), (int)(cg * 255), (int)(cb * 255));
		//Color color2 = new Color(90, 30, 5);
		//Color color2 = new Color((int)(base_r * 255), (int)(base_g * 255), (int)(base_b * 255));
		
		Color color;
		switch(resource){
			case carbon:
				color = new Color(0, 0, 0);
				break;
			case gold:
				color = new Color(242, 238, 2);
				break;
			case hydrogen:
				color = new Color(4, 237, 198);
				break;
			case iron:
				color = new Color(196, 211, 209);
				break;
			case oxygen:
				color = new Color(237, 255, 252);
				break;
			case uranium:
				color = new Color(104, 239, 7);
				break;
			default:
				color = color2;
				break;
		}
		
		
		OpenSimplexNoise osn = new OpenSimplexNoise(seed);
		double modifier = r.nextDouble() * 2 + radius / 8;
		
		double limit1 = 0.55;
		double limit2 = 0.4;
		
		for(int x = 0; x < image.getWidth(); x++){
			for(int y = 0; y < image.getWidth(); y++){
				double distance = Math.sqrt((x - radius) * (x - radius) + (y - radius) * (y - radius)) * 1.002;
				
				if(distance > radius){
					image.setRGB(x, y, 0);
				}
				else{
					Color c;
					float noise = (float)osn.eval(x / modifier, y / modifier, i);
					
					if(noise > limit1){
						c = color;
					}
					else if (noise > limit2){
						float alpha = (float)(1 - (noise - limit2) / (limit1 - limit2)); 
						c = Functions.blend(color, color2, alpha);
					}
					else{
						c = color2;
					}
					
					float red = (float)c.getRed() / 255;
					float green = (float)c.getGreen() / 255;
					float blue = (float)c.getBlue() / 255;
					
					double q = (((double)r.nextInt(161) + 880) / 1000);
					double s = (Math.sqrt(radius * radius - distance * distance) / radius);
					
					red *= q;
					green *= q;
					blue *= q;
					
					red *= s;
					green *= s;
					blue *= s;
					
					if(red > 1)red = 1;
					if(green > 1)green = 1;
					if(blue > 1)blue = 1;
					
					image.setRGB(x, y, new Color(red, green, blue).getRGB());
				}
			}
		}
		return image;
	}
}
