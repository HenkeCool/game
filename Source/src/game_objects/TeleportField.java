package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import physics.Field;
import physics.Mat2;
import physics.Vec2;
import statics.Functions;
import statics.Variables;

public class TeleportField extends GameObject{
	double radius, x, y, life;
	int i = 0;

	public TeleportField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.radius = 0;
		this.life = 1;
		this.x = 0;
		this.y = 0;
		this.i = 0;
	}
	
	public TeleportField(double x_cord, double y_cord, double radius, double life, double x, double y) {
		super(x_cord, y_cord, 0, 0, 0, 0, 0, new Field(radius));
		this.radius = radius;
		this.x = x;
		this.y = y;
		this.life = life*60;
	}

	public void tick(double delta) {
		if(this.i >= 60){
			this.life -= delta;
			if(this.life <= 0)
				Variables.handler.remove_game_object(this);
		}
		
		this.i++;
	}

	public void render(Graphics g) {
		g.setColor(Color.gray);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int)radius, real_y - (int)radius, (int)radius * 2, (int)radius * 2);
	}

	public void hit(GameObject o) {
		/*if(o instanceof Player){
			o.set_x_cord(this.x);
			o.set_y_cord(this.y);
		}*/

		o.set_x_cord(this.x);
		o.set_y_cord(this.y);
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			json.put("radius", radius);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}
	
}
