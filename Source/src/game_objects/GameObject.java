package game_objects;

import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import boss_moves.BossMove;
import modules.Module;
import passives.Passive;
import physics.*;

public abstract class GameObject {
	public Vec2 position = new Vec2();
	public Vec2 velocity = new Vec2();
	public Vec2 force = new Vec2();
	public int id;
	public double angularVelocity;
	public double torque;
	public double orient;
	public double mass, invMass, inertia, invInertia;
	public double staticFriction;
	public double dynamicFriction;
	public double restitution;
	public double density;
	public double drag;
	public Shape shape;
	public double health;
	
	public abstract void tick(double delta);
	public abstract void render(Graphics g);
	public abstract void hit(GameObject object);
	public abstract JSONObject toJSON();
	
	public GameObject(double x_cord, double y_cord, double x_vel, double y_vel, double angle, double density, double drag, Shape shape){
		this.shape = shape;
		this.density = density;
		this.drag = drag;

		position.set(x_cord, y_cord);
		velocity.set(x_vel, y_vel);
		angularVelocity = 0;
		torque = 0;
		orient = angle;
		force.set(0, 0);
		
		//TODO Set object specific friction and restitution
		staticFriction = 0.5;
		dynamicFriction = 0.3;
		restitution = 0.2;

		shape.game_object = this;
		shape.initialize();
	}
	
	public void set_x_cord(double x){
		this.position.set(x, this.position.y);
	}
	
	public void set_y_cord(double y){
		this.position.set(this.position.x, y);
	}
	
	public void set_x_vel(double x){
		this.velocity.set(x, this.velocity.y);
	}
	
	public void set_y_vel(double y){
		this.velocity.set(this.velocity.x, y);
	}
	
	public void set_angle(double angle){
		this.orient = angle;
	}
	
	
	public double get_x_cord(){
		return this.position.x;
	}
	
	public double get_y_cord(){
		return this.position.y;
	}
	
	public double get_x_vel(){
		return this.velocity.x;
	}
	
	public double get_y_vel(){
		return this.velocity.y;
	}
	
	public double get_angle(){
		return this.orient;
	}
	
	public static JSONObject toJSON(GameObject g){
		JSONObject json = new JSONObject();
		try{
			Field[] fields = g.getClass().getFields();
			for(int c = 0; c < fields.length; c++){
				if(fields[c].getType().toString().equals("class java.awt.Color"))
					continue;
				
				switch(fields[c].getType().toString()){
					case "double":
						json.put(fields[c].getName(), fields[c].getDouble(g));
						break;
					case "int":
						json.put(fields[c].getName(), fields[c].getInt(g));
						break;
					case "boolean":
						json.put(fields[c].getName(), fields[c].getBoolean(g));
						break;
					case "class physics.Vec2":
						Vec2 tempvec2 = (Vec2) fields[c].get(g);	
						json.put(fields[c].getName(), tempvec2.toJSON());
						break;
					case "class physics.Shape":
						JSONObject tempj = new JSONObject();
						Shape tempshape = (Shape) fields[c].get(g);
						tempj.put("shape_type", tempshape.getType().toString());
						tempj.put("content", tempshape.toJSON());
						json.put(fields[c].getName(), tempj);
						break;
					case "class java.util.ArrayList":
						JSONObject tempjsonarray = new JSONObject();
						ArrayList temparraylist = (ArrayList) fields[c].get(g);
						if(temparraylist == null)
							break;
						
						for(int i = 0; i < temparraylist.size(); i++){
							if(temparraylist.get(i) instanceof Passive){
								JSONObject z = new JSONObject();
								z.put("type", ((Passive)temparraylist.get(i)).getType());
								z.put("cd", ((Passive)temparraylist.get(i)).getDuration());
								z.put("instance", "Passive");
								tempjsonarray.put(""+i, z);
							}else if(temparraylist.get(i) instanceof BossMove){
								JSONObject z = new JSONObject();
								z.put("type", ((BossMove)temparraylist.get(i)).type);
								z.put("energy", ((BossMove)temparraylist.get(i)).energy);
								z.put("instance", "BossMove");
								tempjsonarray.put(""+i, z);
							}else{
								System.out.println("Error couldn't find arraylist formation for '"+temparraylist.get(i)+"'");
							}
						}
						
						json.put(fields[c].getName(), tempjsonarray);
						break;
					case "class [Lmodules.Module;":
						JSONObject tempjsonmod = new JSONObject();
						Module[] tempmod = (Module[]) fields[c].get(g);
						
						for(int i = 0; i < tempmod.length; i++){
							if(tempmod[i] != null){
								JSONObject z = new JSONObject();
								z.put("type", tempmod[i].getType());
								z.put("cd", tempmod[i].getCooldown());
								tempjsonmod.put(""+i, z);
							}
						}
						
						json.put(fields[c].getName(), tempjsonmod);
						break;
					case "class boss_moves.BossMove":
						JSONObject tempmove = new JSONObject();
						tempmove.put("type", ((BossMove)fields[c].get(g)).type);
						tempmove.put("energy", ((BossMove)fields[c].get(g)).energy);
						
						json.put(fields[c].getName(), tempmove);
						break;
					case "class java.util.Random":
						break;
					case "class enums.ASTEROIDTYPE":
						json.put(fields[c].getName(), fields[c].get(g));
						break;
					case "class enums.RESOURCETYPE":
						json.put(fields[c].getName(), fields[c].get(g));
						break;
					case "class enums.BOSSES":
						json.put(fields[c].getName(), fields[c].get(g));
						break;
					default:
						System.out.println("Error couldn't find formation for '"+fields[c].getType()+"'");
						json.put(fields[c].getName(), fields[c].get(g));
						break;
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
	public void applyForce( Vec2 f )
	{
		// force += f;
		force.addi( f );
	}

	public void applyImpulse( Vec2 impulse, Vec2 contactVector )
	{
		// velocity += im * impulse;
		// angularVelocity += iI * Cross( contactVector, impulse );

		velocity.addsi( impulse, invMass );
		angularVelocity += invInertia * Vec2.cross( contactVector, impulse );
	}

	public void setStatic()
	{
		inertia = 0.0f;
		invInertia = 0.0f;
		mass = 0.0f;
		invMass = 0.0f;
	}

	public void setOrient(double radians)
	{
		orient = radians;
		shape.setOrient(radians);
	}
}
