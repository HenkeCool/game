package game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;
import enums.ASTEROIDTYPE;
import graphics_objects.HealthBar;
import graphics_objects.OpenSimplexNoise;
import passives.Deteriorate;
import physics.Circle;
import physics.Mat2;
import physics.Shape;
import physics.Vec2;
import physics.Polygon;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class Asteroid extends GameObject{
	public boolean hit;
	public ASTEROIDTYPE type;
	
	private BufferedImage graphic;
	public int seed;
	
	public Vec2 longest;

	public Asteroid(){
		super(0, 0, 0, 0, 0, 0, 0, new Polygon());
		this.hit = false;
		this.health = 1;
		this.type = null;
		this.seed = 0;
		this.graphic = null;
		this.longest = null;
	}
	
	public Asteroid(double x_cord, double y_cord, double x_vel, double y_vel, double angle, Shape shape) {
		super(x_cord, y_cord, x_vel, y_vel, angle, 1, 0.99, shape);
		this.hit = false;
		this.health = (int)(this.mass);
		this.type = Functions.randomEnum(enums.ASTEROIDTYPE.class);
		
		this.seed = Functions.random(0, 10000);
		this.graphic = generateGraphic(seed, (Polygon)shape, 0);
		
		longest = ((Polygon)shape).vertices[0];
		
		for(Vec2 vert:((Polygon)shape).vertices){
			if(vert.length() > longest.length())longest = vert;
		}
//		Variables.handler.add_game_graphics_object(new HealthBar(this, 100, Color.green));
	}

	public void tick(double delta) {
		this.hit = false;
		
		if(get_x_cord() <= -11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_x_cord() >= 11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_y_cord() <= -11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(get_y_cord() >= 11000){
			Variables.handler.remove_game_object(this);
		}
		
		if(health < 0)Variables.handler.remove_game_object(this);
	}

	public void render(Graphics g) {
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){
			BufferedImage rotated_graphic = this.graphic;
			
			AffineTransform transform = new AffineTransform();
		    transform.rotate(this.orient, rotated_graphic.getWidth()/2, rotated_graphic.getHeight()/2);
		    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		    rotated_graphic = op.filter(rotated_graphic, null);
			
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
			g.drawImage(rotated_graphic, (int)(real_x - this.longest.length()), (int)(real_y - this.longest.length()), null);
			if(Settings.dev_mode)
				g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
		}
	}

	public void hit(GameObject object){
		if(object instanceof Lazer){
			this.health -= ((Lazer)object).damage;
			if(Functions.random(0, 1d) < 0.25){
				//TODO Spawn rate av resources beror p� type
				Variables.handler.add_game_object(new Resource(object.get_x_cord(), object.get_y_cord(), -object.get_x_vel() / 12, -object.get_y_vel() / 12, -object.get_angle(), Functions.randomEnum(enums.RESOURCETYPE.class), new Circle(Functions.random(4, 10))));
			}
		}
		
		if(object instanceof Player){
			Variables.player.health -= 13;
			Variables.player.addPassive(new Deteriorate());
		}
		this.hit = true;
	}
	
	private static BufferedImage generateGraphic(int seed, Polygon shape, double i) {
		Vec2 longest = shape.vertices[0];
		
		for(Vec2 vert:shape.vertices){
			if(vert.length() > longest.length())longest = vert;
		}
		
		double longest_length = longest.length();
		
		BufferedImage image = new BufferedImage((int)longest_length * 2, (int)longest_length * 2, BufferedImage.TYPE_4BYTE_ABGR);
		
		Random r = new Random(seed);
		
		int v = r.nextInt(31) + 120;
		Color color = new Color(v, v, v);
		
		v *= 0.85;
		Color color_darker = new Color(v, v, v);
		
		OpenSimplexNoise osn = new OpenSimplexNoise(seed);
		double modifier = r.nextDouble() * 2 + 8;
		
		final double limit1 = -0.1;
		final double limit2 = -1;
		
		for(int x = 0; x < image.getWidth(); x++){
			loop:
			for(int y = 0; y < image.getWidth(); y++){
				if(!Functions.isInPolygon(new Vec2(x - longest_length, y - longest_length), shape)){
					image.setRGB(x, y, 0);
				}
				else{
					Color c = color;
					double noise = osn.eval(x / modifier, y / modifier, i) * 2;
					
					if(noise < limit1 && noise > limit2){
						c = color_darker;
					}
					
					if (noise < limit2){
						noise -= limit2;
					}
					
//					else if (noise > limit2){
//						float alpha = (float)(1 - (noise - limit2) / (limit1 - limit2)); 
//						c = Functions.blend(color, color2, alpha);
//					}
					
					float red = (float)c.getRed() / 255;
					float green = (float)c.getGreen() / 255;
					float blue = (float)c.getBlue() / 255;
					
					Vec2 pos = new Vec2(x - longest_length, y - longest_length);
					pos.normalize();
					Vec2 vert1 = null;
					Vec2 vert2 = null;
					
					for(int n = 0; n < shape.vertexCount; n++){
				    	Vec2 vert;
				    	vert = new Vec2(shape.vertices[n].x, shape.vertices[n].y);
				    	vert.normalize();
				    	
						if(pos.sub(vert).length() > pos.length()){
							continue;
						}
						
						vert = shape.vertices[n];
						
						if(pos.cross(vert) >= 0){
							if(vert1 == null){
								vert1 = vert;
								continue;
							}
							if(pos.cross(vert) < Math.abs(pos.cross(vert1)))vert1 = vert;
						}
						else {
							if(vert2 == null){
								vert2 = vert;
								continue;
							}
							if(pos.cross(vert) < Math.abs(pos.cross(vert2)))vert2 = vert;
						}
					}
					
					if(vert1 == null || vert2 == null){
						for(int n = 0; n < shape.vertexCount; n++){
					    	Vec2 vert;
					    	vert = new Vec2(shape.vertices[n].x, shape.vertices[n].y);
					    	vert.normalize();
							
							vert = shape.vertices[n];
							
							if(pos.cross(vert) >= 0){
								if(vert1 == null){
									vert1 = vert;
									continue;
								}
								if(pos.cross(vert) > Math.abs(pos.cross(vert1)))vert1 = vert;
							}
							else {
								if(vert2 == null){
									vert2 = vert;
									continue;
								}
								if(pos.cross(vert) > Math.abs(pos.cross(vert2)))vert2 = vert;
							}
						}
					}
					
					Vec2 temp;
					Vec2 pos_vert;
					double edge_distance;
					double center_distance;
					
					try{
						temp = vert1.sub(vert2);
					}
					catch (NullPointerException e){
						continue loop;
					}
					temp.normalize();
					temp.rotate(-Math.PI / 2);
					
//					pos_vert = new Vec2(vert1.x - x, vert1.y - y);
//					distance = (temp.mul(temp.dot(pos_vert))).sub(pos_vert).length();
//					
//					temp.rotate(Math.PI);
//					
//					if((temp.mul(temp.dot(pos_vert))).sub(pos_vert).length() > distance){
//						distance = (temp.mul(temp.dot(pos_vert))).sub(pos_vert).length();
//					}
					
					double step = 0.5;
					
					double dx = 0;
					double dy = 0;
					double angle = Math.atan2(temp.x, temp.y);
					
					while(Functions.isInPolygon(new Vec2(x - longest_length + dx,  y - longest_length + dy), shape)){
						dx += Math.sin(angle) * step;
						dy += Math.cos(angle) * step;
					}
					
					edge_distance = Math.sqrt(dx * dx + dy * dy);
					center_distance = Math.sqrt((x - longest_length) * (x - longest_length) + (y - longest_length) * (y - longest_length));
					
					double q = (((double)r.nextInt(161) + 880) / 1000);
					double s = (((longest_length - center_distance) * 0.5 + edge_distance * 1.5)) / (longest_length * 2);
					//double s = ((longest_length - center_distance)) / (longest_length);
					//double s = ((edge_distance)) / (longest_length);
					
					noise = osn.eval((x / longest_length) * 0.1, (y / longest_length) * 0.1, i) * 0.5 + 1;
					
					red *= noise;
					green *= noise;
					blue *= noise;
					
					red *= q;
					green *= q;
					blue *= q;
					
					red *= s;
					green *= s;
					blue *= s;
					
					if(red > 1)red = 1;
					if(green > 1)green = 1;
					if(blue > 1)blue = 1;
					
					if(red < 0)red = 0;
					if(green < 0)green = 0;
					if(blue < 0)blue = 0;
					
					image.setRGB(x, y, new Color(red, green, blue).getRGB());
				}
			}
		}
		return image;
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
}
