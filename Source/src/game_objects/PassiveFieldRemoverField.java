package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import enums.PASSIVES;
import passives.*;
import physics.Circle;
import physics.Field;
import physics.Mat2;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class PassiveFieldRemoverField extends GameObject {
	double radius, max_width;
	GameObject entity;
	Vec2 force;
	int i = 0;

	public PassiveFieldRemoverField(){
		super(0, 0, 0, 0, 0, 0, 0, new Field(0));
		this.entity = null;
		this.radius = 0;
		this.max_width = 1;
		this.force = null;
		this.i = 0;
	}

	public PassiveFieldRemoverField(double x_cord, double y_cord, double radius, double max_width) {
		super(x_cord, y_cord, 0, 0, 0, 1, 0.99, new Field(radius));
		this.radius = radius;
		this.max_width = max_width;

	}

	public void tick(double delta) {
		if(this.radius > this.max_width)
			Variables.handler.remove_game_object(this);
		else{
			this.radius += 1;
			this.shape = new Field(this.radius);
		}
	}

	public void render(Graphics g) {
		g.setColor(Color.red);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int) radius, real_y - (int) radius, (int) radius * 2, (int) radius * 2);
		g.setColor(Color.red);
		g.drawString("Remover", real_x, real_y);
		if(Settings.dev_mode)
			g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
	}

	public void hit(GameObject o) {
		if(o instanceof Player)
			return;
			
		if(o instanceof PassiveField || o instanceof FollowingDropperField || o instanceof FollowingPassiveField){
			Variables.handler.remove_game_object(o);
		}
	}

	public JSONObject toJSON() {
		JSONObject json = GameObject.toJSON(this);
		try {
			// json.put("force", force.toJSON());
			json.put("radius", radius);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}