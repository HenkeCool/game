package game_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import graphics_objects.Explosion;
import graphics_objects.Line;
import physics.Field;
import physics.Polygon;
import statics.Functions;
import statics.Variables;

public class Lazer extends GameObject{
	public int starting_life;
	public int life;
	public int damage;
	public GameObject entity;

	public Lazer(){
		super(0, 0, 0, 0, 0, 0, 0, new Polygon());
		this.starting_life = 1;
		this.life = 1;
		this.entity = null;
		this.damage = 0;
	}
	
	public Lazer(GameObject e, double x_cord, double y_cord, double vel, double angle, int life, int damage) {
		super(x_cord, y_cord, Math.cos(angle) * vel, Math.sin(angle) * vel, angle, 1, 1, new Polygon(7, 0.5));
		this.life = life;
		this.starting_life = life;
		this.damage = damage;
		this.entity = e;
	}
	
	public void tick(double delta) {
		this.shape.game_object.set_angle(Math.atan2(this.get_y_vel(), this.get_x_vel()));
		life--;
		
		if(life <= 0){
			Variables.handler.remove_game_object(this);
		}else{
		//	Variables.handler.add_game_graphics_object(new Line(this.get_x_cord(), this.get_y_cord(), this.get_angle(), Color.blue));
		}
	}

	public void render(Graphics g) {
		g.setColor(Color.green);
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){			
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
				
			g.drawLine(real_x, real_y, (int)(real_x + (10 * Math.cos(Math.atan2(this.get_y_vel(), this.get_x_vel())))), (int)(real_y + (10 * Math.sin(Math.atan2(this.get_y_vel(), this.get_x_vel())))));
		}
	}

	public void hit(GameObject object) {
		if(!(object instanceof Player) && !(object instanceof Lazer) && !(object.shape instanceof Field) && !(object.equals(this.entity))){
			Variables.handler.remove_game_object(this);
		}
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
}
