package game_objects;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import enums.PASSIVES;
import graphics_objects.GeoStationaryPopUpText;
import graphics_objects.PopUpText;
import physics.Circle;
import physics.Field;
import physics.Polygon;
import physics.Shape;
import physics.Vec2;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class Rocket extends GameObject{
	public double starting_life;
	public double life;
	public int damage;
	public int fragments;
	
	public boolean quickfix = true;

	public Rocket(){
		super(0, 0, 0, 0, 0, 0, 0, new Polygon());
		this.life = 1;
		this.starting_life = 1;
		this.damage = 0;
		this.fragments = 0;
	}
	
	public Rocket(double x_cord, double y_cord, double vel, double angle, double life, int damage, int fragments) {
		super(x_cord, y_cord, Math.cos(angle) * vel, Math.sin(angle) * vel, angle, 1, 1, createRocket());
		this.life = life*60;
		this.starting_life = life*60;
		this.damage = damage;
		this.fragments = fragments;
	}

	static private Polygon createRocket(){
		Vec2[] verts = Vec2.arrayOf(5);
		
		verts[0].set(33, 0);
		verts[1].set(-5, 5);
		verts[2].set(5, 5);
		verts[3].set(-5, -5);
		verts[4].set(5, -5);
		
		return new Polygon(verts);
	}
	
	public void tick(double delta) {
		life -= delta;
		
		if(life <= 0)
			this.explode();
	}

	public void render(Graphics g) {
		if(Functions.point_distance(Variables.camera.x, Variables.camera.y, this.get_x_cord(), this.get_y_cord()) < 760){
			g.setColor(Color.green);
			int real_x = Functions.game_x_cord(get_x_cord());
			int real_y = Functions.game_y_cord(get_y_cord());
		
			g.drawLine(real_x, real_y, (int)(real_x + (10 * Math.cos(get_angle()))), (int)(real_y + (10 * Math.sin(get_angle()))));
			if(Settings.dev_mode)
				g.drawString(Variables.handler.get_game_object_position(this)+"", real_x, real_y);
		}
		
	}

	public void hit(GameObject object) {
		if(this.quickfix){
			this.quickfix = false;
			return;
		}
		
		if(object instanceof Asteroid || object instanceof Planet)
			this.explode();
			
		if(object instanceof Player && Variables.player != object)
			this.explode();
			
	}

	public void explode(){
		Variables.handler.remove_game_object(this);
		for(int i = 0; i <= this.fragments; i++){
			int angle = (360 / this.fragments) * i - 180;
			Variables.handler.add_game_object(new Lazer(Variables.player, this.get_x_cord(), this.get_y_cord(), 0, angle, 100, 1));
			Variables.handler.add_game_object(new ForceField(this.get_x_cord(), this.get_y_cord(), 100, 0.2, new Vec2(0.02, 0)));
		}
	}

	public JSONObject toJSON() {
		return toJSON(this);
	}
}
