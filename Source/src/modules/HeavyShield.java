package modules;

import enums.MODULES;
import game_objects.Lazer;
import game_objects.Rocket;
import game_objects.Shield;
import statics.Variables;

public class HeavyShield extends Module	{
	
	public HeavyShield(int i) {
		super(MODULES.HeavyShield, 0, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;
		
		Variables.handler.add_game_object(new Shield(Variables.player, Variables.player.get_x_cord(), Variables.player.get_y_cord(), 10));
		
		return false;
	}
}
