package modules;

import java.util.ArrayList;

import enums.MODULES;
import enums.PASSIVES;
import passives.Boost;
import passives.Passive;
import statics.Variables;

public class Disspell extends Module{

	public Disspell(int i) {
		super(MODULES.Disspell, 60, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;

		ArrayList<Passive> temp = Variables.player.passive_list;
		System.out.println(temp.size());
		
		for(int i = 0; i < temp.size(); i++){
			if(!temp.get(i).isBuff()){
				Variables.player.removePassive(temp.get(i).getType());
			}
			System.out.println("Removed: "+temp.get(i).getName());
		}
		
		System.out.println("All debuffs removed");
		
		return true;
	}
}
