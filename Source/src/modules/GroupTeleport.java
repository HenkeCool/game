package modules;

import enums.MODULES;
import game_objects.*;
import physics.Vec2;
import statics.Variables;

public class GroupTeleport extends Module	{
	
	public GroupTeleport(int i) {
		super(MODULES.GroupTeleport, 60, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;
		
	//	Variables.player.get_x_cord(), Variables.player.get_y_cord()
		
		Variables.handler.add_game_object(new GravityField(Variables.player.get_x_cord() + (250 * Math.cos(Variables.player.get_angle())), Variables.player.get_y_cord() + (250 * Math.sin(Variables.player.get_angle())), 100, 10, new Vec2(1, 0)));
		Variables.handler.add_game_object(new TeleportField(Variables.player.get_x_cord() + (250 * Math.cos(Variables.player.get_angle())), Variables.player.get_y_cord() + (250 * Math.sin(Variables.player.get_angle())), 10, 10 , Variables.player.get_x_cord() + (2500 * Math.cos(Variables.player.get_angle())), Variables.player.get_y_cord() + (2500 * Math.sin(Variables.player.get_angle()))));
		
		return false;
	}
}
