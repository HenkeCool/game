package modules;

import enums.MODULES;
import game_objects.Lazer;
import game_objects.Rocket;
import statics.Variables;

public class PlanetDestroyer extends Module	{
	
	public PlanetDestroyer(int i) {
		super(MODULES.PlanetDestroyer, 0, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;
		
		Variables.handler.add_game_object(new Rocket(Variables.player.get_x_cord(), Variables.player.get_y_cord(), 2, Variables.player.get_angle(), 1.5, 25, 10));
		
		return false;
	}
}
