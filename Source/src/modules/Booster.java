package modules;

import enums.MODULES;
import enums.PASSIVES;
import passives.Boost;
import passives.DeBoost;
import statics.Variables;

public class Booster extends Module {
	
	public Booster(int i){
		super(MODULES.Booster, 5, i);
	}
	
	public boolean activate() {
		if(!this.moduleReady())	
			return false;

		if(!Variables.player.hasPassive(new DeBoost()))
			Variables.player.addPassive(new Boost());
		else
			this.cooldown = 0;
		
		return true;
	}
	
	
}
