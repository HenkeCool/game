package modules;

import enums.MODULES;
import game_objects.Lazer;
import game_objects.Rocket;
import game_objects.Shield;
import game_objects.TestField;
import physics.Vec2;
import statics.Variables;

public class Gathering extends Module	{
	
	public Gathering(int i) {
		super(MODULES.Gathering, 0, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;
		
		Variables.handler.add_game_object(new TestField(Variables.player, Variables.player.get_x_cord(), Variables.player.get_y_cord(), 250, Integer.MAX_VALUE, new Vec2(5, 0)));
		
		return false;
	}
}
