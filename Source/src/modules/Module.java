package modules;

import enums.MODULES;

abstract public class Module {
	public MODULES type;
	public double maxcooldown;
	public double cooldown;
	public int position;
	
	public Module(int i){
		this(null, 0, -1);
	}
	
	/**
	 * @param name Module Name
	 * @param cd Cooldown in seconds
	 * @param type Type of the module
	 */
	public Module(MODULES name, double cd, int position){
		this.type = name;
		this.maxcooldown = cd*60;
		this.position = position;
	}

	abstract public boolean activate();

	public void tick(double delta){
		if(this.cooldown > 0)
			this.cooldown = this.cooldown - delta;
		else
			this.cooldown = 0;
	}
	
	public boolean moduleReady(){
		if(this.cooldown > 0){
			return false;
		}
		
		this.cooldown = this.maxcooldown;
		return true;
	}

	
	public String getName(){
		return this.type.name();
	}

	
	public MODULES getType(){
		return this.type;
	}
	
	public double getMaxCooldown(){
		return this.maxcooldown;
	}
	
	public double getCooldown(){
		return this.cooldown;
	}
	
	public int getPosition(){
		return this.position;
	}
}
