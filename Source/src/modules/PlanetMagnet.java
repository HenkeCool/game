package modules;

import enums.MODULES;
import game_objects.GravityField;
import game_objects.Lazer;
import game_objects.Rocket;
import physics.Vec2;
import statics.Variables;

public class PlanetMagnet extends Module	{
	
	public PlanetMagnet(int i) {
		super(MODULES.PlanetMagnet, 0, i);
	}

	public boolean activate() {
		if(!this.moduleReady())	
			return false;
		
		Variables.handler.add_game_object(new GravityField(Variables.player.get_x_cord(), Variables.player.get_y_cord(), 250, 0, new Vec2(50, 0)));
		
		return false;
	}
}
