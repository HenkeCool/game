package core;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import enums.GAMESTATE;
import game_objects.*;
import graphics_objects.GraphicsObject;
import physics.*;
import statics.Settings;
import statics.Functions;
import statics.Variables;

public class Handler {
	public double dt;
	private boolean formating_ready = true;
	public int iterations = 100;
	public ArrayList<Manifold> contacts = new ArrayList<Manifold>();

	public ArrayList<GameObject> game_object = new ArrayList<GameObject>();
	private ArrayList<GraphicsObject> game_graphics_object = new ArrayList<GraphicsObject>();
	
	public void tick(double delta){		
		this.dt = delta;
		
		if(Variables.game_state == GAMESTATE.Game){
			tick_game(delta);
			Variables.menu_handler.tick(delta);
		}
		else if(Variables.game_state == GAMESTATE.Menu){
			Variables.menu_handler.tick(delta);
		}
		else{
			tick_game(delta);
			Variables.menu_handler.tick(delta);
		}
	}
	
	public void render(Graphics g){
		if(Variables.game_state == GAMESTATE.Game){
			render_game(g);
			Variables.menu_handler.render(g);
		}
		else if(Variables.game_state == GAMESTATE.Menu){
			Variables.menu_handler.render(g);
		}
		else{
			render_game(g);
			Variables.menu_handler.render(g);
		}
	}
	
	public void add_game_object(GameObject object){
		this.game_object.add(object);
	}
	
	public void add_game_object(int i, GameObject object){
		this.game_object.add(i, object);
	}
	
	public void remove_game_object(GameObject object){
		this.game_object.remove(object);
	}
	
	public GameObject get_game_object(int index){
		GameObject g = this.game_object.get(index);
		return g;
	}
	
	public int get_game_object_size(){
		int i = this.game_object.size();
		return i;
	}
	
	public boolean get_game_object_contains(GameObject o){
		boolean b = this.game_object.contains(o);
		return b;
	}
	

	
	public int get_game_object_position(GameObject o){
		return this.game_object.indexOf(o);
	}
	
	public void clear_game_object(){
		this.game_object.clear();
	}
	
	
	public void add_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.add(object);
	}
	
	public void remove_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.remove(object);
	}
	
	public void clear_game_graphics_object(){
		this.game_graphics_object.clear();
	}
	
	
	private void tick_game(double delta){
		for(int i = 0; i < game_object.size(); i++){
			GameObject tempObject = game_object.get(i);
			tempObject.tick(delta);
		}
		
		for(int i = 0; i < game_graphics_object.size(); i++){
			GraphicsObject tempObject = game_graphics_object.get(i);
			tempObject.tick(delta);
		}
		
		step();
	}
	
	private void render_game(Graphics g){
		for(int i = 0; i < game_graphics_object.size(); i++){
			GraphicsObject tempObject = game_graphics_object.get(i);
			tempObject.render(g);
		}
		
		for(int i = 0; i < game_object.size(); i++){
			GameObject tempObject = game_object.get(i);
			tempObject.render(g);
		}
		
		if(Settings.draw_hitboxes)draw_hitboxes(g);
		if(Settings.draw_velocities)draw_velocities(g);
		if(Settings.draw_forces)draw_forces(g);
		if(Settings.draw_verticies)draw_verticies(g);
		
	}
	
	public void step(){
		// Generate new collision info
		contacts.clear();
		for (int i = 0; i < game_object.size(); ++i){
			GameObject A = game_object.get( i );

			for (int j = i + 1; j < game_object.size(); ++j){
				GameObject B = game_object.get( j );

				if (A.invMass == 0 && B.invMass == 0){
					continue;
				}

				Manifold m = new Manifold(A, B);
				m.solve();

				if (m.contactCount > 0){
					contacts.add( m );
				}
			}
		}

		// Integrate forces
		for (int i = 0; i < game_object.size(); ++i){
			integrateForces(game_object.get(i), dt);
		}

		// Initialize collision
		for (int i = 0; i < contacts.size(); ++i){
			contacts.get(i).initialize();
		}

		// Solve collisions
		for (int j = 0; j < iterations; ++j){
			for (int i = 0; i < contacts.size(); ++i){
				contacts.get(i).applyImpulse();
			}
		}
		
		for(int i = 0; i < game_object.size(); i++){
			game_object.get(i).setOrient(Functions.normalizeOrient(game_object.get(i).orient));
		}
		
		//Apply drag
		for (int i = 0; i < game_object.size(); ++i){
			GameObject tempObject = game_object.get(i);
			
			tempObject.angularVelocity *= (tempObject.drag);
			
			Vec2 drag = new Vec2(tempObject.velocity.x, tempObject.velocity.y);
			drag.rotate(Math.PI);
			drag = drag.muli(12).muli(tempObject.drag).muli(tempObject.mass * 0.001);
			
			tempObject.applyForce(drag);
				
			if(tempObject.velocity.x < 0.01 && tempObject.velocity.x > -0.01){
				tempObject.velocity.x = 0;
			}
			
			if(tempObject.velocity.y < 0.01 && tempObject.velocity.y > -0.01){
				tempObject.velocity.y = 0;
			}
			
			if(tempObject.angularVelocity < 0.0001 && tempObject.angularVelocity > -0.0001){
				tempObject.angularVelocity = 0;
			}
		}
		
		//Register hit
//		for (int i = 0; i < contacts.size(); ++i){
//			Manifold contact = contacts.get(i);
//			if(contact.contactCount > 0){
//				contacts.get(i).A.hit(contact.B);
//				contacts.get(i).B.hit(contact.A);
//			}
//		}

		// Integrate velocities
		for (int i = 0; i < game_object.size(); ++i){
			integrateVelocity(game_object.get(i), dt);
		}

		// Correct positions
		for (int i = 0; i < contacts.size(); ++i){
			contacts.get( i ).positionalCorrection();
		}

		// Clear all forces
		for (int i = 0; i < game_object.size(); ++i){
			GameObject b = game_object.get(i);
			b.force.set( 0, 0 );
			b.torque = 0;
		}
	}

	// Acceleration
	// F = mA
	// => A = F * 1/m

	// Explicit Euler
	// x += v * dt
	// v += (1/m * F) * dt

	// Semi-Implicit (Symplectic) Euler
	// v += (1/m * F) * dt
	// x += v * dt

	// see http://www.niksula.hut.fi/~hkankaan/Homepages/gravity.html
	public void integrateForces(GameObject b, double dt){
//		if(b->im == 0.0f)
//			return;
//		b->velocity += (b->force * b->im + gravity) * (dt / 2.0f);
//		b->angularVelocity += b->torque * b->iI * (dt / 2.0f);

		if (b.invMass == 0.0f)return;

		double dts = dt * 0.5f;

		b.velocity.addsi( b.force, b.invMass * dts );
		b.angularVelocity += b.torque * b.invInertia * dts;
	}

	public void integrateVelocity(GameObject b, double dt){
//		if(b->im == 0.0f)
//			return;
//		b->position += b->velocity * dt;
//		b->orient += b->angularVelocity * dt;
//		b->SetOrient( b->orient );
//		IntegrateForces( b, dt );

		if (b.invMass == 0.0f)return;		

		b.position.addsi(b.velocity, dt);
		b.orient += b.angularVelocity * dt;
		b.setOrient(b.orient);

		integrateForces(b, dt);
	}
	
	private void draw_hitboxes(Graphics g){
		Graphics2D gr = (Graphics2D)g;
		for (GameObject b : game_object){
			if (b.shape instanceof Circle){
				Circle c = (Circle)b.shape;

				double rx = (double)StrictMath.cos(b.orient) * c.radius;
				double ry = (double)StrictMath.sin(b.orient) * c.radius;
				
				int real_x = Functions.game_x_cord(b.get_x_cord());
				int real_y = Functions.game_y_cord(b.get_y_cord());
				 
				gr.setColor( Color.red );
				gr.draw(new Ellipse2D.Double( real_x - c.radius, real_y - c.radius, c.radius * 2, c.radius * 2));
				gr.draw(new Line2D.Double(real_x, real_y, real_x + rx, real_y + ry));
			}
			else if (b.shape instanceof Polygon){
				Polygon p = (Polygon)b.shape;

				Path2D.Double path = new Path2D.Double();
				for (int i = 0; i < p.vertexCount; i++){
					Vec2 v = new Vec2(p.vertices[i]);
					b.shape.u.muli(v);
					//v.addi(b.position);
					int real_x = Functions.game_x_cord(b.get_x_cord());
					int real_y = Functions.game_y_cord(b.get_y_cord());

					if (i == 0){
						path.moveTo(real_x + v.x, real_y + v.y);
					}
					else{
						path.lineTo(real_x + v.x, real_y + v.y);
					}
				}
				path.closePath();

				gr.setColor( Color.blue );
				gr.draw( path );
			}
		}

		gr.setColor(Color.white);
		for (Manifold m : contacts){
			for (int i = 0; i < m.contactCount; i++){
				Vec2 v = m.contacts[i];
				Vec2 n = m.normal;
				int real_x = Functions.game_x_cord(v.x);
				int real_y = Functions.game_y_cord(v.y);

				gr.draw(new Line2D.Double(real_x, real_y, real_x + n.x * 4.0, real_y + n.y * 4.0));
			}
		}
	}
	
	private void draw_velocities(Graphics g){
		for (GameObject o : game_object){
			int x = Functions.game_x_cord(o.position.x);
			int y = Functions.game_y_cord(o.position.y);
			
			g.setColor(Color.green);
			g.drawLine(x, y, x + (int)(o.velocity.x * 10), (int)(y + o.velocity.y * 10));
		}
	}
	
	private void draw_forces(Graphics g){
		for (GameObject o : game_object){
			int x = Functions.game_x_cord(o.position.x);
			int y = Functions.game_y_cord(o.position.y);
			
			g.setColor(Color.blue);
			g.drawLine(x, y, x + (int)(o.force.x), (int)(y + o.force.y));
		}
	}
	
	private void draw_verticies(Graphics g){
		for (GameObject o : game_object){
			if(o.shape instanceof Polygon){
				int x = Functions.game_x_cord(o.position.x);
				int y = Functions.game_y_cord(o.position.y);
				
				Polygon p = (Polygon)o.shape;
				for(int i = 0; i < p.vertexCount; i++){
					g.setColor(Color.red);
					g.drawLine(x, y, x + (int)(p.u.mul(p.vertices[i]).x), (int)(y + p.u.mul(p.vertices[i]).y));
				}
			}
		}
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		for(int i = 0; i < game_object.size(); i++){
			GameObject tempObject = game_object.get(i);
			JSONObject o = tempObject.toJSON();
			try {
				o.put("object_type", tempObject.getClass().getName());
				json.put(Integer.toString(i), o);
			}
			catch (JSONException e){
				e.printStackTrace();
			}
		}
		
		return json;
	}
	
	public void purge(){
		this.game_object = new ArrayList<GameObject>();
	}
	
	public void fromJSON(JSONObject o){
		if(!this.formating_ready)
			return;
		this.formating_ready = false;
		
		if(this.get_game_object_size() != o.length() && this.get_game_object_size() > 0)
			this.purge();
		
		ArrayList<GameObject> gms = new ArrayList<GameObject>();
		for(int c = 0; c < this.get_game_object_size(); c++)
			gms.add(this.get_game_object(c));
		
		for(int i = 0; i < o.length(); i++){
			try {
				JSONObject game_object = (JSONObject)o.get(Integer.toString(i));
				GameObject gm; 
				
				String name = game_object.getString("object_type");
				switch(name){
					case "game_objects.Player":
						if(gms.size() == o.length() && gms.get(i).getClass().equals("game_objects.Player"))
							gm = Functions.object_fromJSON(game_object, gms.get(i));
						else
							gm = Functions.object_fromJSON(game_object, new Player());
						
						if(gm != null)
							this.add_game_object(i, gm);
						break;
					case "game_objects.Asteroid":
						if(gms.size() == o.length() && gms.get(i).getClass().equals("game_objects.Asteroid"))
							gm = Functions.object_fromJSON(game_object, gms.get(i));
						else
							gm = Functions.object_fromJSON(game_object, new Asteroid());
						if(gm != null)
							this.add_game_object(i, gm);
						break;
					case "game_objects.Lazer":
						if(gms.size() == o.length() && gms.get(i).getClass().equals("game_objects.Lazer"))
							gm = Functions.object_fromJSON(game_object, gms.get(i));
						else
							gm = Functions.object_fromJSON(game_object, new Lazer());
						if(gm != null)
							this.add_game_object(i, gm);
						break;
					case "game_objects.Planet":
						if(gms.size() == o.length() && gms.get(i).getClass().equals("game_objects.Planet")){
							System.out.println("Remoduling");
							gm = Functions.object_fromJSON(game_object, gms.get(i));
						}else{
							System.out.println("Building "+this.get_game_object_size());
							gm = Functions.object_fromJSON(game_object, new Planet());
							System.out.println(gm.toJSON());
						}	
						
						if(gm != null)
							this.add_game_object(i, gm);
						break;
					case "game_objects.Resource":
						if(gms.size() == o.length() && gms.get(i).getClass().equals("game_objects.Resource"))
							gm = Functions.object_fromJSON(game_object, gms.get(i));
						else
							gm = Functions.object_fromJSON(game_object, new Resource());
						if(gm != null)
							this.add_game_object(i, gm);
						break;
				}
			}
			catch (JSONException e){
				e.printStackTrace();
			}
		}
		this.formating_ready = true;
	}

	public GameObject get_game_object_by_id(int id) {
		for(int i = 0; i < this.get_game_object_size(); i++){
			if(this.get_game_object(i).id == id)
				return this.get_game_object(i);
		}
		return null;
	}
}