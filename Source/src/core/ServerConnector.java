package core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Queue;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.JOptionPane;
import javax.swing.plaf.synth.SynthSpinnerUI;

import org.json.JSONException;
import org.json.JSONObject;

import core.Game;
import core.Handler;
import enums.GAMESTATE;
import enums.MENUSTATE;
import enums.MOVEMENTS;
import statics.Variables;

public class ServerConnector implements Runnable{
	private Socket request_socket;
	private DatagramSocket udp_socket;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private JSONObject last_update;
	
	private Queue<JSONObject> request_queue = new LinkedList<JSONObject>();
	
	private JSONObject info;
	private JSONObject outfo;
	
	private String ip;
	private int port;
	
	private boolean accelerating = false;
	private boolean turning_left = false;
	private boolean turning_right = false;
	private boolean shooting = false;
	
	private int index;
	private int object_position;
	private int tries = 0;
	
	private boolean got_connection = false;
	
	public ServerConnector(String ip, int port){
		this.ip = ip;
		this.port = port;
	}
	
	public boolean isConnected(){
		return this.got_connection;
	}
	
	public int getObjectPosition(){
		return this.object_position;
	}
	
	public void connect() throws UnknownHostException, IOException, InterruptedException{
		while(!this.isConnected()){
			try{
				Variables.console.execute("echo Attemting to connect to "+this.ip+":"+this.port, true);
				this.request_socket = new Socket(this.ip, this.port);
				this.udp_socket = new DatagramSocket();
				
				byte[] x = "connected".getBytes();
				
				DatagramPacket dp = new DatagramPacket(x, x.length, InetAddress.getByName(this.ip), this.port);
				this.udp_socket.send(dp);
				
			    System.out.println("Client: Connected");
				
				this.out = new ObjectOutputStream(new BufferedOutputStream(this.request_socket.getOutputStream()));
				System.out.println("Gotten output");
				this.out.flush();
				this.in = new ObjectInputStream(new BufferedInputStream(this.request_socket.getInputStream()));

				//new Thread(new ServerCollector(this.handler, this.in)).start();

				System.out.println("Client: Attemting to send credentials");
				
				this.outfo = new JSONObject();
				
				try {
					this.outfo.put("type", "logingame");
					this.outfo.put("username", "cryslacks-"+Math.random());
					this.outfo.put("password", "lame");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				this.add(this.outfo);
				this.sendRequest();
				Variables.handler.purge();
				
				JSONObject temp = new JSONObject(this.in.readObject().toString());
				
				if(temp.get("type").equals("success")){
					this.index = temp.getInt("index");
					this.object_position = temp.getInt("object_position");
				}
				
				System.out.println("Client: Recieved connection and got index "+this.index);

				
				Variables.game_state = GAMESTATE.Game;
				Variables.menu_handler.load_lists(MENUSTATE.Game);
				this.got_connection = true;
				this.checkChecksum();
			} catch (ConnectException | ClassNotFoundException | JSONException | UnknownHostException e){
				System.out.println("Client: Connection error, "+this.ip+":"+this.port+"!");
				Variables.server = null;
				Variables.console.execute("echo Error, couldn't connect to "+this.ip+":"+this.port, true);
				return;
					
			}
			
		}
	}
	
	public void run(){
		if(!this.isConnected()){
			try {
				this.connect();
			} catch (IOException | InterruptedException e2) {
				e2.printStackTrace();
			}
		}
		
		while(this.got_connection){
			try {
	            byte[] buffer = new byte[65536];
	            DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
	            this.udp_socket.receive(incoming);
	            byte[] data = incoming.getData();
	            this.last_update = new JSONObject(new String(data, 0, incoming.getLength()));
	            
				if(this.sendRequest()){
					this.info = new JSONObject((String)this.in.readObject());
					
					if(this.info.length() > 0){
						if(this.info.has("handler")){
							JSONObject temp = new JSONObject();
							int c = 0;
							
							temp = this.info.getJSONObject("handler");
							
							for(int i = 0; i < temp.length(); i++){
								if(temp.getJSONObject(i+"").get("object_type").equals("game_objects.Player")){
									if(c == this.index){
										temp.getJSONObject(i+"").put("me", "true");
									}else{
										temp.getJSONObject(i+"").put("me", "false");
									}
									c++;
								}
							}
							System.out.println(temp.toString()+"|||||||"+Variables.handler.toJSON());
						}else{
							if(this.info.getString("type").equals("success"))
								System.out.println("<<<<<<<<<<<<<<<<<<<<<<SUCCESS>>>>>>>>>>>>>>>>>>>>>>");
						}
						try{
							Thread.sleep(10);
						} catch (InterruptedException e){}
					}
				}
			} catch (ClassNotFoundException e) {
				System.out.println("Class error!");
				return;
			} catch (SocketException e) {
				System.out.println("Server disappeared!");
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public JSONObject getLastUpdate(){
		return this.last_update;
	}
	
	private String getChecksum(String name) {
		FileInputStream fis;
		try {
			File f = new File(name);
			fis = new FileInputStream(f);
			String temp = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
			fis.close();
			return temp;
		} catch (IOException e) {}
		
		return "";
	}
	
	public String extractFile(InputStream in) throws IOException{
		OutputStream temp = new FileOutputStream("â�–");
		try {
		    byte[] buffer = new byte[in.available()];
		    for (int i = 0; i != -1; i = in.read(buffer)) {
		        temp.write(buffer, 0, i);
		    }
		} finally {
		    in.close();
		    temp.close();
		    return "â�–";
		}
	}
	
	public void checkChecksum() throws IOException, JSONException{
		JSONObject comp = new JSONObject();
		String[] packages = {"boss_moves", "bosses", "core", "enums", "game_objects", "graphics_objects", "input", "menu_objects", "modules", "passives", "physics", "statics"};
		
		String filename = new File(Game.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
		try {
			File f = new File(filename);
			for(int i = 0; i < packages.length; i++){
				JSONObject temp = new JSONObject();
				
				JarFile jar = new JarFile(f);
				Enumeration<JarEntry> entries = jar.entries();
				while(entries.hasMoreElements()){
					JarEntry entry = entries.nextElement();
					if(entry.toString().startsWith(packages[i]) && !entry.toString().equals(packages[i]+"/")){
						temp.put(entry.toString(), this.getChecksum(this.extractFile(jar.getInputStream(entry))));
					}
				}
				
				comp.put(packages[i], temp);
			}
			
			JSONObject temp = new JSONObject();
			temp.put("type", "checksum");
			temp.put("info", comp);
			this.add(temp);
			Files.deleteIfExists(Paths.get("â�–"));
		} catch (IOException e) {}
	}
	
	private void add(JSONObject j){
		this.request_queue.add(j);
	}
	
	public void sendCommand(String cmd){
		if(cmd.startsWith("rcon")){
			this.sendRconCommand(cmd);
			return;
		}
		
		System.out.println("Sending "+cmd+" to server!");
		
		JSONObject temp = new JSONObject();
		try{
			temp.put("type", "command");
			temp.put("command", cmd);
		} catch (JSONException e){}
		
		this.add(temp);
	}
	
	public void sendRconCommand(String cmd){
		System.out.println("Sending the RconCommand "+cmd+" to server!");
		JSONObject temp = new JSONObject();
		try{
			temp.put("type", "rcon_command");
			temp.put("command", cmd);
		} catch (JSONException e){}
		
		this.add(temp);
	}

	private boolean sendRequest() throws IOException{
		JSONObject temp = new JSONObject();
		temp = this.request_queue.poll();
		
		if(temp == null)
			return false;

		this.out.writeObject(temp.toString());
		this.out.flush();
		return true;
	}
}
