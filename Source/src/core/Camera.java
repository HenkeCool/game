
package core;

import game_objects.GameObject;

public class Camera {
	public double x;
	public double y;
	
	public double target_x;
	public double target_y;
	
	private GameObject tracked_object;
	
	public Camera() {
		
	}
	
	public Camera(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public Camera(GameObject o){
		this.tracked_object = o;
	}
	
	public void update(double delta){
		this.x += 0.1 * delta * (this.target_x - this.x) * (-Math.pow(2, (-(Math.abs(this.target_x - this.x)))) + 1);
		this.y += 0.1 * delta * (this.target_y - this.y) * (-Math.pow(2, (-(Math.abs(this.target_y - this.y)))) + 1);
		
		if(tracked_object != null){
			this.target_x = this.tracked_object.get_x_cord() + this.tracked_object.get_x_vel() * 15;
			this.target_y = this.tracked_object.get_y_cord() + this.tracked_object.get_y_vel() * 15;
		}
	}
	
	public void jump_to(double x, double y){
		this.target_x = x;
		this.target_y = y;
		this.x = x;
		this.y = y;
	}
	
	public void go_to(double x, double y){
		this.target_x = x;
		this.target_y = y;
	}
	
	public void track_object(GameObject o){
		this.tracked_object = o;
	}
	
	public void un_track_object(){
		this.tracked_object = null;
	}
}
