package core;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;

import input.*;
import org.json.JSONException;

import enums.MENUSTATE;
import enums.RESOURCETYPE;
import input.*;
import menu_objects.Console;
import menu_objects.ResourceButton;
import modules.PlanetMagnet;
import physics.Circle;
import physics.Polygon;
import physics.Vec2;
import statics.*;
import game_objects.*;
import graphics_objects.GeoStationaryPopUpText;
import graphics_objects.Star;
import graphics_objects.StarField;

public class Game extends Canvas implements Runnable{
	private static final long serialVersionUID = 9138981190511913342L;
	
	private Thread thread;
	private boolean running;
	
	public static void main(String[] args) {
		new Game();
	}
	
	public Game(){
		//Objects often used
		Variables.game = this;
		Variables.handler = new Handler();
		Handler handler = Variables.handler;
		Variables.menu_handler = new MenuHandler();
		Vec2[] verts = Vec2.arrayOf(8);
		verts[0].set(-9, 10);
		verts[1].set(-9, -10);
		
		verts[2].set(-10, 9);
		verts[3].set(-10, -9);
		
		verts[4].set(6, 8);
		verts[5].set(6, -8);
		
		verts[6].set(10, 2);
		verts[7].set(10, -2);
		Variables.player = new Player(0, 0, 0, 0, new Polygon(verts));
		Variables.camera = new Camera(Variables.player);

		//Declaring window
		Variables.key_input = new KeyInput();
		Variables.mouse = new MouseInput();
		
		//Prepare menu
		Variables.menu_handler.initialize_menu();
		
		this.addKeyListener(Variables.key_input);
		this.addMouseListener(Variables.mouse);
		this.addMouseWheelListener(Variables.mouse);

		this.addComponentListener(new ComponentInput());
		
		//Add game objects
		
		float r = 40;
		int vertCount = 20;

		Vec2[] verts2 = Vec2.arrayOf( vertCount );
		for (int i = 0; i < vertCount; i++)
		{
			verts2[i].set( Functions.random( -r, r ), Functions.random( -r, r ) );
		}
		
		Variables.handler.add_game_object(Variables.player);
		//handler.add_game_object(new Asteroid(-100, 100, 0, 0, 0, new Polygon(verts2)));
		
		/*
		for (int i = 0; i < 15; i++){
			for (int o = 0; o < 15; o++){
				handler.add_game_graphics_object(new Star(Settings.WIDTH / 15 * o + Functions.randInt(-15, 15), Settings.HEIGHT / 15 * i + Functions.randInt(-15, 15), 0, 0, 0, 1));
			}
		}
		
		for (int i = 0; i < 10; i++){
			for (int o = 0; o < 10; o++){
				handler.add_game_graphics_object(new Star(Settings.WIDTH / 10 * o + Functions.randInt(-30, 30), Settings.HEIGHT / 10 * i + Functions.randInt(-30, 30), 0, 0, 0, 2));
			}
		}
		
		for (int i = 0; i < 5; i++){
			for (int o = 0; o < 5; o++){
				handler.add_game_graphics_object(new Star(Settings.WIDTH / 5 * o + Functions.randInt(-60, 60), Settings.HEIGHT / 5 * i + Functions.randInt(-60, 60), 0, 0, 0, 3));
			}
		}*/
		
		handler.add_game_graphics_object(new StarField(1337, 1));
		handler.add_game_graphics_object(new StarField(1338, 2));
		handler.add_game_graphics_object(new StarField(1339, 3));
		
		Variables.handler.add_game_graphics_object(new GeoStationaryPopUpText(0, -30, "Welcome :D", Color.white, 15f, 200));
		handler.game_object.add(new Planet(100, 100, 0, new Circle(100)));
		handler.add_game_object(new ForceField(-150, -150, 150, 20, new Vec2(-1, 0.1)));
		new Window(Settings.WIDTH, Settings.HEIGHT, "Game", this);
	}

	public void run() {
		//GAMELOOP BASED ON EXAMPLE BY ELI DELVENTHAL ON http://www.java-gaming.org/
		int fps = 0;
		double OPTIMAL_TIME = 1000000000 / Settings.TARGET_FPS;
		double lastLoopTime = System.nanoTime() - OPTIMAL_TIME;
		double lastFpsTime = 0;
		double sleep_time = OPTIMAL_TIME;
		
		// keep looping round til the game ends
		while (running){
			OPTIMAL_TIME = 1000000000 / Settings.TARGET_FPS;
			this.requestFocusInWindow();
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop
			double now = System.nanoTime();
			double updateLength = now - lastLoopTime;
			lastLoopTime = now;
			double delta = updateLength / 10000000;
			
			//Calculate sleeptime
			sleep_time = OPTIMAL_TIME - updateLength;
			if(sleep_time < 0)sleep_time = 0;
			
			// update the frame counter
			fps++;
			// update our FPS counter if a second has passed since
			// we last recorded
			if (now - lastFpsTime >= 1000000000){
				lastFpsTime = System.nanoTime();
				Variables.fps = fps;
				fps = 0;
			}
			// update the game logic
			tick(delta);
			// draw everything
			render(Variables.fps);
			// we want each frame to take 10 milliseconds, to do this
			// we've recorded when we started the frame. We add 10 milliseconds
			// to this and then factor in the current time to give 
			// us our final value to wait for
			// remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
			try{
				if(sleep_time > 0)Thread.sleep((long)(sleep_time / 490000));
				//Borde vara
				//sleep_time / 1000000
				//men nej... det verkar inte fungera
			}
			catch(InterruptedException e){
				System.out.println("Interrupted");
			}
		}
	}
	
	private void tick(double delta){
		if(Variables.server != null && Variables.server.isConnected() && Variables.server.getLastUpdate() != null)
			Variables.handler.fromJSON(Variables.server.getLastUpdate());
	
		
		Variables.handler.tick(delta);
		Variables.camera.update(delta);
		
		int asteroids = 0;
		int planets = 0;
		for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
			if(Variables.handler.get_game_object(i) instanceof Asteroid)asteroids++;
			if(Variables.handler.get_game_object(i) instanceof Planet)planets++;
		}
//		while(asteroids < 100){
//			float r = Functions.random(20, 50);
//			int vertCount = Functions.random(10, Polygon.MAX_POLY_VERTEX_COUNT);
//
//			Vec2[] verts = Vec2.arrayOf( vertCount );
//			for (int i = 0; i < vertCount; i++)
//			{
//				verts[i].set( Functions.random( -r, r ), Functions.random( -r, r ) );
//			}
//			
//			asteroids++;
//			Variables.handler.add_game_object(new Asteroid(Functions.random(-Config.area_width / 2, Config.area_width / 2), Functions.random(-Config.area_height / 2, Config.area_height / 2), 0, 0, Functions.random(-Math.PI, Math.PI), new Polygon(verts)));
//			//Variables.handler.add_game_object(new Asteroid(0, 0, 0, 0, Functions.random(-Math.PI, Math.PI), new Circle(Functions.random(20, 50))));
//		}
//		while(planets < 50){
//			planets++;
//			Variables.handler.add_game_object(new Planet(Functions.randInt(-10000, 10000), Functions.randInt(-10000, 10000), 0, new Circle(Functions.randInt(100, 300))));
//		}
	}
	
	private void render(double latest_frames){
		BufferStrategy bs = this.getBufferStrategy();
		
		if(bs == null){
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		
		g.setClip(new Rectangle(0, 0, Settings.WIDTH, Settings.HEIGHT));
		
		g.setColor(Color.black);
		g.fillRect(0, 0, Settings.WIDTH, Settings.HEIGHT);
		
		Variables.handler.render(g);
		
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(12f);
		g.setFont(newFont);
		
		if(Settings.draw_debug_information){
			g.setColor(Color.green);
			g.drawString("FPS: " + latest_frames, 10, 10);
			
			g.setColor(Color.blue);
			g.drawString("Camera x: " + (int)Variables.camera.x, 10, 70);
			g.drawString("Camera y: " + (int)Variables.camera.y, 10, 90);
			
			
			g.drawString("Game objects: " + Variables.handler.game_object.size(), 10, 150);
			//g.drawString("Graphics objects: " + Variables.handler.game_graphics_object.size(), 10, 170);
			
			
			//g.setColor(Color.cyan);
		}
		
		g.dispose();
		bs.show();
	}
	
	public synchronized void start(){
		thread = new Thread(this);
		thread.start();
		this.running = true;
	}
}
