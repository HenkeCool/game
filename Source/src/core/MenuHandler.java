package core;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Stack;

import enums.GAMESTATE;
import enums.MENUSTATE;
import graphics_objects.GraphicsObject;
import menu_objects.Console;
import menu_objects.HorizontalBox;
import menu_objects.MenuObject;
import menu_objects.MenuOverlay;
import menu_objects.TextBox;
import menu_objects.TextButton;
import menu_objects.TextInput;
import menu_objects.VerticalBox;
import statics.Settings;
import statics.Variables;

public class MenuHandler {
	public Stack<MENUSTATE> previous_state = new Stack<MENUSTATE>();
	
	public MenuObject active_item;
	public MenuObject focus_item;
	
	public LinkedList<MenuObject> active_list = new LinkedList<MenuObject>();
	public LinkedList<GraphicsObject> active_graphics_list = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> menu_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> menu_graphics_object = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> pause_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> pause_graphics_object = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> settings_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> settings_graphics_object = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> console_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> console_graphics_object = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> connect_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> connect_graphics_object = new LinkedList<GraphicsObject>();
	
	private LinkedList<MenuObject> game_object = new LinkedList<MenuObject>();
	private LinkedList<GraphicsObject> game_graphics_object = new LinkedList<GraphicsObject>();
	
	public MenuHandler(){
		load_lists(MENUSTATE.Menu);
	}
	
	public void load(MenuObject o){
		this.active_list.add(o);
	}

	public void load_lists(MENUSTATE state){
		previous_state.push(state);
		switch(state){
			case Connect:
				this.active_list = connect_object;
				this.active_graphics_list = connect_graphics_object;
				break;
			case Game:
				this.active_list = game_object;
				this.active_graphics_list = game_graphics_object;
				break;
			case Menu:
				this.active_list = menu_object;
				this.active_graphics_list = menu_graphics_object;
				break;
			case Pause:
				this.active_list = pause_object;
				this.active_graphics_list = pause_graphics_object;
				break;
			case Settings:
				this.active_list = settings_object;
				this.active_graphics_list = settings_graphics_object;
				break;
			case Console:
				this.active_list = console_object;
				this.active_graphics_list = console_graphics_object;
				break;
		}
	}
	
	public void previous_list(){
		previous_state.pop();
		load_lists(previous_state.pop());
	}

	public void tick(double delta){
		for(int i = 0; i < active_list.size(); i++){
			MenuObject tempObject = active_list.get(i);
			tempObject.tick(delta);
		}
		
		for(int i = 0; i < active_graphics_list.size(); i++){
			GraphicsObject tempObject = active_graphics_list.get(i);
			tempObject.tick(delta);
		}
	}

	public void render(Graphics g){
		for(int i = 0; i < active_list.size(); i++){
			MenuObject tempObject = active_list.get(i);
			tempObject.render(g);
		}
		
		for(int i = 0; i < active_graphics_list.size(); i++){
			GraphicsObject tempObject = active_graphics_list.get(i);
			tempObject.render(g);
		}
	}
	
	//==============Get/set=============
	
	//Menu

	private void add_menu_object(MenuObject object){
		this.menu_object.add(object);
	}
	
	private void remove_menu_object(MenuObject object){
		this.menu_object.remove(object);
	}
	
	
	private void add_menu_graphics_object(GraphicsObject object){
		this.menu_graphics_object.add(object);
	}
	
	private void remove_menu_graphics_object(GraphicsObject object){
		this.menu_graphics_object.remove(object);
	}
	
	//Pause menu
	
	private void add_pause_menu_object(MenuObject object){
		this.pause_object.add(object);
	}
	
	private void remove_pause_menu_object(MenuObject object){
		this.pause_object.remove(object);
	}
	
	
	private void add_pause_menu_graphics_object(GraphicsObject object){
		this.pause_graphics_object.add(object);
	}
	
	private void remove_pause_menu_graphics_object(GraphicsObject object){
		this.pause_graphics_object.remove(object);
	}
	
	//Settings
	
	private void add_settings_object(MenuObject object){
		this.settings_object.add(object);
	}
	
	private void remove_settings_object(MenuObject object){
		this.settings_object.remove(object);
	}
	
	
	private void add_settings_graphics_object(GraphicsObject object){
		this.settings_graphics_object.add(object);
	}
	
	private void remove_settings_graphics_object(GraphicsObject object){
		this.settings_graphics_object.remove(object);
	}

	
	//Console	
	
	public void add_console_object(MenuObject object){
		this.console_object.add(object);
	}
	
	public void remove_console_object(MenuObject object){
		this.console_object.remove(object);
	}
	
	
	public void add_console_graphics_object(GraphicsObject object){
		this.console_graphics_object.add(object);
	}
	
	public void remove_console_graphics_object(GraphicsObject object){
		this.console_graphics_object.remove(object);
	}
	
	//Connect
	
	private void add_connect_object(MenuObject object){
		this.connect_object.add(object);
	}
	
	private void remove_connect_object(MenuObject object){
		this.connect_object.remove(object);
	}
	
	
	private void add_connect_graphics_object(GraphicsObject object){
		this.connect_graphics_object.add(object);
	}
	
	private void remove_connect_graphics_object(GraphicsObject object){
		this.connect_graphics_object.remove(object);
	}
	
	//Game
	
	private void add_game_object(MenuObject object){
		this.game_object.add(object);
	}
	
	private void remove_game_object(MenuObject object){
		this.game_object.remove(object);
	}
	
	
	private void add_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.add(object);
	}
	
	private void remove_game_graphics_object(GraphicsObject object){
		this.game_graphics_object.remove(object);
	}
	
	//================Initialize================

	public void initialize_menu() {
		
		//Menu
		add_settings_object(new MenuOverlay(Color.black, 0.5));
		add_menu_object(new TextButton(0, -100, 200, 80, "Play", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.game_state = GAMESTATE.Game;
				Variables.menu_handler.load_lists(MENUSTATE.Game);
			}
		});
		
		add_menu_object(new TextButton(0, 0, 200, 80, "Play online", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.menu_handler.load_lists(MENUSTATE.Connect);
			}
		});
		
		add_menu_object(new TextButton(0, 100, 200, 80, "Exit", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				System.exit(1);
			}
		});
		
		//Pause menu
		
		add_pause_menu_object(new MenuOverlay(Color.black, 0.5));
		
		add_pause_menu_object(new TextButton(0, -100, 200, 80, "Resume", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.game_state = GAMESTATE.Game;
				Variables.menu_handler.load_lists(MENUSTATE.Game);
			}
		});
		
		add_pause_menu_object(new TextButton(0, 0, 200, 80, "Settings", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.menu_handler.load_lists(MENUSTATE.Settings);
			}
		});
		add_pause_menu_object(new TextButton(0, 100, 200, 80, "Main menu", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.menu_handler.load_lists(MENUSTATE.Menu);
				Variables.game_state = GAMESTATE.Menu;
			}
		});
		
		//Console
		
		Variables.console = new Console(0, -50, 500, 250, 20, 10);
		
		add_console_object(new MenuOverlay(Color.black, 0.5));
		
		add_console_object(Variables.console);
		
		add_console_object(new TextButton(0, 150, 200, 80, "Back", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.menu_handler.load_lists(MENUSTATE.Game);
				Variables.menu_handler.focus_item = null;
				Variables.game_state = GAMESTATE.Game;
			}
		});
		
		//Settings
		
		add_settings_object(new MenuOverlay(Color.black, 0.5));
		
		VerticalBox settings_box = new VerticalBox(0, 0, 9999, 10, 0, new Color(0,0,0,0), new Color(0,0,0,0));
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle debug information", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.draw_debug_information = !Settings.draw_debug_information;
			}
		});
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle hitboxes", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.draw_hitboxes = !Settings.draw_hitboxes;
			}
		});
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle velocities", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.draw_velocities = !Settings.draw_velocities;
			}
		});
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle forces", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.draw_forces = !Settings.draw_forces;
			}
		});
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle verticies", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.draw_verticies = !Settings.draw_verticies;
			}
		});
		
		settings_box.add(new TextButton(0, 0, 180, 40, "Toggle dev_mode", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.dev_mode = !Settings.dev_mode;
			}
		});
		
		HorizontalBox fps_box = new HorizontalBox(0, 0, 5, 0, new Color(0,0,0,0), new Color(0,0,0,0));
		
		fps_box.add(new TextButton(0, 0, 85, 40, "+ FPS", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.TARGET_FPS += 10;
			}
		});
		
		fps_box.add(new TextButton(0, 0, 85, 40, "- FPS", Color.gray, 15){
			@Override public void on_click(MouseEvent e){
				Settings.TARGET_FPS -= 10;
			}
		});
		
		settings_box.add(fps_box);
		
		settings_box.add(new TextButton(0, 0, 180, 60, "Back", Color.gray, 20){
			@Override
			public void on_click(MouseEvent e){
				Variables.menu_handler.previous_list();
			}
		});
		
		add_settings_object(settings_box);
		
		//Connect
		add_connect_object(new MenuOverlay(Color.black, 0.5));
		
		TextInput input = new TextInput(0, -100, 300, 100, Color.gray, 5, 15f, 300);
		add_connect_object(input);
		
		add_connect_object(new TextButton(0, 0, 200, 80, "Play", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.ip = input.getString();
				// SERVER THINGS
				Variables.server = new ServerConnector(Variables.ip, 63529);					
				new Thread(Variables.server).start();
			}
		});
		
		add_connect_object(new TextButton(0, 100, 200, 80, "Back", Color.gray, 20){
			@Override public void on_click(MouseEvent e){
				Variables.menu_handler.previous_list();
			}
		});
		
		//Game
		add_game_object(new TextButton(-Settings.WIDTH / 2 + 40, Settings.HEIGHT / 2 - 60 , 50, 30, "Pause", Color.gray, 10){
			@Override public void on_click(MouseEvent e){
				Variables.game_state = GAMESTATE.Paused;
				Variables.menu_handler.load_lists(MENUSTATE.Pause);
			}
		});
	}

	public void on_click(MouseEvent e){
		for(int i = active_list.size() - 1; i >= 0; i--){
			MenuObject tempObject = active_list.get(i);
			if(tempObject.occupies(e.getX(), e.getY())){
				active_item = tempObject;
				tempObject.on_click(e);
				break;
			}
		}
	}

	public void on_key(KeyEvent e) {
		if(active_item != null){
			active_item.on_key(e);
		}
	}

	public void on_scroll(MouseWheelEvent e){
		for(int i = active_list.size() - 1; i >= 0; i--){
			MenuObject tempObject = active_list.get(i);
			if(tempObject.occupies(e.getX(), e.getY())){
				tempObject.on_scroll(e);
				break;
			}
		}
	}
        
}