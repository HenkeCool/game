package core;

import org.json.JSONException;
import org.json.JSONObject;

import game_objects.*;
import statics.Functions;
import statics.Variables;

public class Update {
	private JSONObject json = new JSONObject();
	
	public void updateHandler(){
		for(int i = 0; i < this.json.length(); i++){
			try {
				JSONObject game_object = (JSONObject)this.json.get(Integer.toString(i));
				String name = game_object.getString("object_type");
				switch(name){
					case "game_objects.Player":
						Variables.handler.add_game_object(i, Functions.object_fromJSON(game_object, new Player()));
						break;
					case "game_objects.Asteroid":
						Variables.handler.add_game_object(i, Functions.object_fromJSON(game_object, new Asteroid()));
						break;
					case "game_objects.Lazer":
						Variables.handler.add_game_object(i, Functions.object_fromJSON(game_object, new Lazer()));
						break;
					case "game_object.Planet":
						Variables.handler.add_game_object(i, Functions.object_fromJSON(game_object, new Planet()));
						break;
					case "game_objects.Resource":
						Variables.handler.add_game_object(i, Functions.object_fromJSON(game_object, new Resource()));
						break;
				}
			}
			catch (JSONException e){
				e.printStackTrace();
			}
		}
	}
	
	public void setHandler(JSONObject j){
		this.json = j;
	}
}
