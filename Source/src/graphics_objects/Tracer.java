package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;

import game_objects.GameObject;
import game_objects.Lazer;
import physics.Mat2;
import physics.Vec2;
import statics.Functions;
import statics.Variables;

public class Tracer extends GraphicsObject{

	private GameObject a,b;
	private Vec2 c,d;
	private Color rgb;
	private int life = 120;
	
	public Tracer(Vec2 a, Vec2 b) {
		super(0, 0, 0, 0, 0);
		this.c = a;
		this.d = b;
		this.rgb = Color.green;
	}
	
	public Tracer(Vec2 a, Vec2 b, Color col) {
		super(0, 0, 0, 0, 0);
		this.c = a;
		this.d = b;
		this.rgb = col;
	}
	
	public Tracer(GameObject a, GameObject b) {
		super(0, 0, 0, 0, 0);
		this.a = a;
		this.b = b;
		this.c = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.a)).position;
		this.d = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.b)).position;
		this.rgb = Color.green;
	}
	
	public Tracer(GameObject a, GameObject b, Color col) {
		super(0, 0, 0, 0, 0);
		this.a = a;
		this.b = b;
		this.c = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.a)).position;
		this.d = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.b)).position;
		this.rgb = col;
	}


	public void tick(double delta) {
		if(this.a != null || this.b != null){
			if(!Variables.handler.get_game_object_contains(this.a) || !Variables.handler.get_game_object_contains(this.b))
				Variables.handler.remove_game_graphics_object(this);
		
			this.c = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.a)).position;
			this.d = Variables.handler.get_game_object(Variables.handler.get_game_object_position(this.b)).position;

		}else{
			if(this.life < 0)
				Variables.handler.remove_game_graphics_object(this);				
			this.life--;
		}	
	}

	public void render(Graphics g) {
		g.setColor(this.rgb);
		int a_real_x = Functions.game_x_cord(c.x);
		int a_real_y = Functions.game_y_cord(c.y);
		int b_real_x = Functions.game_x_cord(d.x);
		int b_real_y = Functions.game_y_cord(d.y);
			
		Vec2 atemp = new Vec2(a_real_x, a_real_y);
		Vec2 btemp = new Vec2(b_real_x, b_real_y);
		
		g.drawLine(a_real_x, a_real_y, b_real_x, b_real_y);
		g.drawString(String.valueOf(Math.round(atemp.distance(btemp))), (a_real_x+b_real_x)/2, (a_real_y+b_real_y)/2);
	}
	
}
