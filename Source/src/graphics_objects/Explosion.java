package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import statics.Functions;
import statics.Variables;

public class Explosion extends GraphicsObject {
	Random r;
	double rad, duration;
	Color col;
	
	public Explosion(double x_cord, double y_cord, double duration, double rad, Color col) {
		super(x_cord, y_cord, 0, 0, 0);
		this.r = new Random();
		this.col = col;
		this.rad = rad;
		this.duration = duration*60;
	}

	public void tick(double delta) {
		this.duration -= delta;
		if(this.duration <= 0)
			Variables.handler.remove_game_graphics_object(this);
	}

	public void render(Graphics g) {
		g.setColor(this.col);
		for(int i = 0; i < 10; i++){
			double offset_x = r.nextInt((int)rad+3) - rad;
			double offset_y = r.nextInt((int)rad+3) - rad;

			double real_x = Functions.game_x_cord(x_cord+offset_x);
			double real_y = Functions.game_y_cord(y_cord+offset_y);

			g.fillRect((int)real_x, (int)real_y, (int)rad/5, (int)rad/5);
		}
		
	}

}
