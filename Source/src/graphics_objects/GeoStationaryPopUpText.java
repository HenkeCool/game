package graphics_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import statics.Functions;

public class GeoStationaryPopUpText extends PopUpText {

	public GeoStationaryPopUpText(double x_cord, double y_cord, String text, Color color, float size) {
		super(x_cord, y_cord, text, color, size);
	}
	
	public GeoStationaryPopUpText(double x_cord, double y_cord, String text, Color color, float size, int displayTime) {
		super(x_cord, y_cord, text, color, size, displayTime);
	}

	public void render(Graphics g) {
		g.setColor(color);
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(size);
		g.setFont(newFont);
		int width = g.getFontMetrics().stringWidth(text);
		int height = g.getFontMetrics().getHeight();
		g.drawString(text, (int)(Functions.game_x_cord(x_cord) - width / 2), (int)(Functions.game_y_cord(y_cord) - height / 2));
	}

	public void tick(double delta) {
		super.tick(delta);
	}
}
