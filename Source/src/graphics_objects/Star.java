package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;

import statics.Settings;
import statics.Functions;
import statics.Variables;

public class Star extends GraphicsObject{
	private int level;
	private double last_c_cord_x, last_c_cord_y;

	public Star(double x_cord, double y_cord, double x_vel, double y_vel, double angle, int level) {
		super(x_cord, y_cord, x_vel, y_vel, angle);
		this.level = level;
	}

	public void tick(double delta) {
		double delta_x = Variables.camera.x - last_c_cord_x;
		double delta_y = Variables.camera.y - last_c_cord_y;
		
		if(!(delta_x > 1000 || delta_x < -1000 || delta_y > 1000 || delta_y < -1000)){
			set_x_vel((delta_x * -1) / 3 * level * 1);
			set_y_vel((delta_y * -1) / 3 * level * 1);
		}
		
		set_x_cord(get_x_cord() + get_x_vel());
		set_y_cord(get_y_cord() + get_y_vel());
		
		
		//Re
		
		if(x_vel != 0){
			set_x_vel(get_x_vel() * 0.975f);
			
			if(x_vel < 0.1 && x_vel > -0.1){
				set_x_vel(0);
			}
		}
		
		if(y_vel != 0){
			set_y_vel(get_y_vel() * 0.975f);
			
			if(y_vel < 0.1 && y_vel > -0.1){
				set_y_vel(0);
			}
		}
		
		//Hit detection
		
		if(get_x_cord() <= 0){
			set_x_cord(Settings.WIDTH + get_x_cord() + Functions.random(-2, 3));
		}
		
		if(get_x_cord() >= Settings.WIDTH){
			set_x_cord(Settings.WIDTH - get_x_cord() + Functions.random(-2, 3));
		}
		
		if(get_y_cord() <= 0){
			set_y_cord(Settings.HEIGHT + get_y_cord() + Functions.random(-2, 3));
		}
		
		if(get_y_cord() >= Settings.HEIGHT){
			set_y_cord(Settings.HEIGHT - get_y_cord() + Functions.random(-2, 3));
		}
		
		last_c_cord_x = Variables.camera.x;
		last_c_cord_y = Variables.camera.y;
	}

	public void render(Graphics g) {
		int value = 63 * level + 65;
		Color color = new Color(value, value, value);
		
		g.setColor(color);
		g.fillOval((int)x_cord - 1, (int)y_cord - 1, 2, 2);
	}
}
