package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;

import statics.Functions;

public class Line extends GraphicsObject{

	private Color color;
	public Line(double x_cord, double y_cord, double angle, Color c) {
		super(x_cord, y_cord, 0, 0, angle);
		this.color = c;
	}

	public void tick(double delta) {}

	public void render(Graphics g) {
		g.setColor(this.color);
		g.drawLine(Functions.game_x_cord(this.get_x_cord()), Functions.game_y_cord(this.get_y_cord()),(int)(Functions.game_x_cord(this.get_x_cord()) + (10 * Math.cos(Math.atan2(this.get_y_vel(), this.get_x_vel())))), (int)(Functions.game_y_cord(this.get_y_cord()) + (10 * Math.sin(Math.atan2(this.get_y_vel(), this.get_x_vel())))));
	}

}
