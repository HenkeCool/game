package graphics_objects;
import java.awt.Graphics;

public abstract class GraphicsObject {
	protected double x_cord, y_cord;
	protected double x_vel, y_vel;
	protected double angle;
	
	public abstract void tick(double delta);
	public abstract void render(Graphics g);
	
	public GraphicsObject(double x_cord, double y_cord, double x_vel, double y_vel, double angle){
		this.x_cord = x_cord;
		this.y_cord = y_cord;
		this.x_vel = x_vel;
		this.y_vel = y_vel;
		this.angle = angle;
	}
	
	public void set_x_cord(double x){
		this.x_cord = x;
	}
	
	public void set_y_cord(double y){
		this.y_cord = y;
	}
	
	public void set_x_vel(double x){
		this.x_vel = x;
	}
	
	public void set_y_vel(double y){
		this.y_vel = y;
	}
	
	public void set_angle(double angle){
		this.angle = angle;
	}
	
	
	
	public double get_x_cord(){
		return this.x_cord;
	}
	
	public double get_y_cord(){
		return this.y_cord;
	}
	
	public double get_x_vel(){
		return this.x_vel;
	}
	
	public double get_y_vel(){
		return this.y_vel;
	}
	
	public double get_angle(){
		return this.angle;
	}
}
