package graphics_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import physics.Vec2;
import statics.Functions;
import statics.Variables;

public class PlayerPopUpText extends PopUpText {
	Vec2 offset;

	public PlayerPopUpText(String text, Color color, float size, Vec2 offset) {
		super(Variables.player.position.x + offset.x, Variables.player.position.y + offset.y, text, color, size);
		this.offset = offset;
	}

	public PlayerPopUpText(String text, Color color, float size, double displayTime, Vec2 offset) {
		super(Variables.player.position.x + offset.x, Variables.player.position.y + offset.y, text, color, size, displayTime);
		this.offset = offset;
	}

	public void render(Graphics g) {
		g.setColor(color);
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(size);
		g.setFont(newFont);
		int width = g.getFontMetrics().stringWidth(text);
		int height = g.getFontMetrics().getHeight();
		g.drawString(text, (int)(Functions.game_x_cord(Variables.player.position.x + offset.x) - width / 2), (int)(Functions.game_y_cord(Variables.player.position.y + offset.y) - height / 2));
	}

	public void tick(double delta) {
		super.tick(delta);
	}
}
