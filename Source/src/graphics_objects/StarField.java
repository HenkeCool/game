package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import statics.Settings;
import statics.Variables;

public class StarField extends GraphicsObject {
	private Random r;
	private OpenSimplexNoise osn;
	private double i;
	private boolean itter_direction;
	private int level;
	
	public StarField(int seed, int level) {
		super(0,0,0,0,0);
		r = new Random(seed);
		osn = new OpenSimplexNoise(seed);
		i = 0;
		itter_direction = true;
		this.level = level;
	}
	
	public void tick(double delta) {
		if(itter_direction)
			i += delta / 1000;
		else
			i -= delta / 1000;
		
		if(r.nextInt(10000) < 2 || i > Double.MAX_VALUE - 10 || i < -Double.MAX_VALUE + 10){
			itter_direction = !itter_direction;
		}
	}

	public void render(Graphics g){
		int devider = (int)(10 + 2 * (4 - level));
		
		int x_limit = (int)Settings.WIDTH / devider;
		int y_limit = (int)Settings.HEIGHT / devider;
		
		double rel_cam_x = Variables.camera.x / (level + 1) * 3;
		double rel_cam_y = Variables.camera.y / (level + 1) * 3;
		
		double offset_x = rel_cam_x - ((int)(rel_cam_x / devider)) * devider;
		double offset_y = rel_cam_y - ((int)(rel_cam_y / devider)) * devider;
		
		for(double x = 0; x < x_limit; x += 1){
			for(double y = 0; y < y_limit; y += 1){
				double val = osn.eval((x * devider + rel_cam_x - offset_x) / (devider), (y * devider + rel_cam_y - offset_y) / (devider), i);
				double limit = -0.05 * level + 0.7;
				if(val > limit){
					val -= (limit - (0.005 * (4 - level)));
					val *= 1 / (1 - (limit - (0.005 * (4 - level))));
					val *= 2;
					if (val > 1)
						val = 1;
					
					g.setColor(new Color(255, 255, 255, (int)(255 * val)));
					g.fillOval((int)((x * devider) - 1 - offset_x), (int)((y * devider) - 1 - offset_y), 3, 3);
				}
			}
		}
	}
}
