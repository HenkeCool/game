package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;

import statics.Variables;

public abstract class PopUpText extends GraphicsObject {
	public String text;
	public Color color;
	public float size;
	public Double displayTime;

	public PopUpText(double x_cord, double y_cord, String text, Color color, float size){
		super(x_cord, y_cord, 0, 0, 0);
		this.text = text;
		this.color = color;
		this.size = size;
		this.displayTime = null;
	}
	
	public PopUpText(double x_cord, double y_cord, String text, Color color, float size, double displayTime){
		super(x_cord, y_cord, 0, 0, 0);
		this.text = text;
		this.color = color;
		this.size = size;
		this.displayTime = displayTime;
	}

	public void tick(double delta){
		if(displayTime != null){
			displayTime -= delta;
		}
		if(displayTime < 0){
			Variables.handler.remove_game_graphics_object(this);
		}
	}
	public abstract void render(Graphics g);

}
