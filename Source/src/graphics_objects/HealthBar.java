package graphics_objects;

import java.awt.Color;
import java.awt.Graphics;

import bosses.Boss;
import game_objects.GameObject;
import statics.Functions;
import statics.Variables;

public class HealthBar extends GraphicsObject {
	private GameObject entity;
	private Color rgb;
	private double size;
	private int startHp;
	
	
	public HealthBar(GameObject ent, double size, Color col) {
		super(ent.get_x_cord(), ent.get_y_cord(), 0, 0, 0);
		this.entity = ent;
		this.size = size;
		this.rgb = col;
		this.startHp = (int)ent.health;
	}
	
	public void tick(double delta) {
		if(this.entity.health <= 0)
			Variables.handler.remove_game_graphics_object(this);
	}
	
	public void render(Graphics g) {
		double hp = this.entity.health;
		int x = Functions.game_x_cord(this.entity.get_x_cord());
		int y = Functions.game_y_cord(this.entity.get_y_cord());
		
		double pro = (double)hp/this.startHp;
		
		int rad = (int)this.entity.shape.radius;
		
		g.setColor(Color.gray);
		g.drawRect(x-(int)this.size/2, y-((int)this.size/2)-rad/4, (int)this.size, (int)(this.size/10));
		g.setColor(this.rgb);
		g.fillRect(x-(int)this.size/2+1, y-((int)this.size/2)-rad/4+1, (int)((this.size-1)*pro), (int)((this.size/10)-1));
		g.setColor(Color.gray);
		g.setFont(g.getFont().deriveFont(10f));
		if(this.entity instanceof Boss)
			g.drawString(String.valueOf(hp), x-(int)this.size/10, y-((int)this.size/2)-rad/15);
		else
			g.drawString(String.valueOf(hp), x-(int)this.size/10, y-((int)this.size/2)+9);
	}

}
