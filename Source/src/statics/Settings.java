package statics;

public class Settings {
	public static int WIDTH = 1200;
	public static int HEIGHT = ((WIDTH / 16) * 9);
	public static boolean draw_debug_information = true;
	public static boolean draw_hitboxes = false;
	public static boolean draw_velocities = false;
	public static boolean draw_forces = false;
	public static boolean dev_mode = false;
	public static boolean draw_verticies = false;
	public static double TARGET_FPS = 80;
	public static int area_width = 20000;
	public static int area_height = 20000;
}