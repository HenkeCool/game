package statics;

import core.Camera;
import core.Game;
import core.Handler;
import core.MenuHandler;
import core.ServerConnector;
import enums.GAMESTATE;
import game_objects.Player;
import input.KeyInput;
import input.MouseInput;
import menu_objects.Console;

public class Variables {
	public static int fps;
	public static int lastFpsTime;
	public static String ip;
	
	public static Handler handler;
	public static MenuHandler menu_handler;
	public static Console console;
	public static KeyInput key_input;
	public static MouseInput mouse;
	
	public static GAMESTATE game_state = GAMESTATE.Menu;
	public static Player player;
	public static Camera camera;
	public static Game game;
	
	public static ServerConnector server;
}
