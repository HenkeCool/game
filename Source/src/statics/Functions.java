package statics;

import java.awt.Color;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import boss_moves.BossMove;
import enums.MODULES;
import game_objects.*;
import modules.Module;
import passives.Passive;
import physics.Circle;
import physics.Mat2;
import physics.NoShape;
import physics.Polygon;
import physics.Shape;
import physics.Vec2;

public class Functions {
	public static double normalizeAngle(double angle){
		double newAngle = angle;
	    while (newAngle <= -180) newAngle += 360;
	    while (newAngle > 180) newAngle -= 360;
	    return newAngle;
	}
	
	public static double normalizeOrient(double orient){
		double newAngle = orient;
	    while (newAngle <= -Math.PI) newAngle += Math.PI*2;
	    while (newAngle > Math.PI) newAngle -= Math.PI*2;
	    return newAngle;
	}
	
	public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = new Random().nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public static int game_x_cord(double cord) {
		return (int)(cord - Variables.camera.x + Settings.WIDTH / 2);
	}
	
	public static int game_y_cord(double cord) {
		return (int)(cord - Variables.camera.y + Settings.HEIGHT / 2);
	}
	
	public static double point_distance(double x1, double y1, double x2, double y2){
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static double random(double min, double max){
		return (double)((max - min) * Math.random() + min);
	}

	public static int random(int min, int max){
		return (int)((max - min + 1) * Math.random() + min);
	}
	
	public static Color blend(Color c1, Color c2, float ratio) {
	    if ( ratio > 1f ) ratio = 1f;
	    else if ( ratio < 0f ) ratio = 0f;
	    float iRatio = 1.0f - ratio;

	    int i1 = c1.getRGB();
	    int i2 = c2.getRGB();

	    int a1 = (i1 >> 24 & 0xff);
	    int r1 = ((i1 & 0xff0000) >> 16);
	    int g1 = ((i1 & 0xff00) >> 8);
	    int b1 = (i1 & 0xff);

	    int a2 = (i2 >> 24 & 0xff);
	    int r2 = ((i2 & 0xff0000) >> 16);
	    int g2 = ((i2 & 0xff00) >> 8);
	    int b2 = (i2 & 0xff);

	    int a = (int)((a1 * iRatio) + (a2 * ratio));
	    int r = (int)((r1 * iRatio) + (r2 * ratio));
	    int g = (int)((g1 * iRatio) + (g2 * ratio));
	    int b = (int)((b1 * iRatio) + (b2 * ratio));

	    return new Color( a << 24 | r << 16 | g << 8 | b );
	}
	
	private static boolean ccw(Vec2 A, Vec2 B, Vec2 C){
		return (C.y-A.y)*(B.x-A.x) > (B.y-A.y)*(C.x-A.x);
	}
	
	public static boolean intersect(Vec2 A, Vec2 B, Vec2 C, Vec2 D){
		return ccw(A,C,D) != ccw(B,C,D) && ccw(A,B,C) != ccw(A,B,D);
	}
	
	public static boolean isInPolygon(Vec2 point, Polygon shape){
		    String previous_side = null;
		    int n_vertices = shape.vertexCount;
		    for(int n = 0; n < n_vertices; n++){
		    	Vec2 a = shape.vertices[n];
		    	Vec2 b = shape.vertices[(n+1)%n_vertices];
		        Vec2 affine_segment = b.sub(a);
		        Vec2 affine_point = point.sub(a);
		        String current_side = get_side(affine_segment, affine_point);
		        if (current_side == null){
		        	return false;
		        }
		        else if (previous_side == null){
		        	 previous_side = current_side;
		        }
		        else if (previous_side != current_side){
		        	 return false;
		        }
		    }
		    return true;
	}
	
	public static String get_side(Vec2 a, Vec2  b){
		double x = a.cross(b);
	    if (x < 0){
	    	return "LEFT";
	    }
	    else if(x > 0){
	    	return "RIGHT";
	    }
	    else{
	    	 return null;
	    }
	}
	    
	
	public static GameObject object_fromJSON(JSONObject o, GameObject go){
		try {
			boolean owner = false;
			GameObject t = go;
			
			Field[] fields = t.getClass().getFields();
			for(Field field : fields){
				try{
					field = t.getClass().getDeclaredField(field.getName());
				} catch (NoSuchFieldException e) {
					field = t.getClass().getSuperclass().getDeclaredField(field.getName());
				}
				
				if(o.has(field.getName())){
					switch(field.getType().toString()){
						case "int":
							if((o.getInt(field.getName()) == Variables.server.getObjectPosition()) && field.getName().equals("owner_id"))
								owner = true;
							field.setInt(t, o.getInt(field.getName()));
							break;
						case "double":
							field.setDouble(t, o.getDouble(field.getName()));
							break;
						case "boolean":
							field.setBoolean(t, o.getBoolean(field.getName()));
							break;
						case "String":
							field.set(t, o.getString(field.getName()));
							break;
						case "class physics.Vec2":
							field.set(t, new Vec2(o.getJSONObject(field.getName()).getDouble("x"), o.getJSONObject(field.getName()).getDouble("y")));
							break;
						case "class [Lmodules.Module;":
							JSONObject z = o.getJSONObject(field.getName());
							
							for(int c = 0; c < z.length(); c++){
								Module x = ((Class<Module>)Class.forName("modules."+o.getJSONObject(c+"").get("type").toString())).getConstructor(Integer.class).newInstance(c);
								x.cooldown = o.getJSONObject(c+"").getInt("cd");
								
								((Player)t).addModule(x);
							}
							break;
						case "class java.util.ArrayList":
							JSONObject jo = o.getJSONObject(field.getName());
							ArrayList<Passive> p = new ArrayList<Passive>();
							ArrayList<BossMove> bm = new ArrayList<BossMove>();
							
							for(int c = 0; c < jo.length(); c++){
								switch(jo.getJSONObject(c+"").getString("instance")){
									case "Passive":
										Passive px = ((Class<Passive>)Class.forName("passives."+o.getJSONObject(c+"").get("type").toString())).getConstructor().newInstance();
										px.duration = o.getJSONObject(c+"").getInt("cd");
										p.add(px);
										break;
									case "BossMove":
										BossMove bmx = ((Class<BossMove>)Class.forName("boss_moves."+o.getJSONObject(c+"").get("type").toString())).getConstructor().newInstance();
										bmx.time = o.getJSONObject(c+"").getInt("cd");
										bm.add(bmx);
										break;
								}
							}
							
							if(bm.size() > 1)
								field.set(t, bm);
							else if(p.size() > 1)
								field.set(t, p);
							break;
						case "class java.awt.Color":
							break;
							
						case "class physics.Shape":
							switch(o.getJSONObject(field.getName()).getString("shape_type")){
								case "Circle":
									field.set(t, Functions.circle_fromJSON(o.getJSONObject(field.getName()).getJSONObject("content")));
									break;
								case "Field":
									field.set(t, Functions.field_fromJSON(o.getJSONObject(field.getName()).getJSONObject("content")));
									break;
								case "Poly":
									Shape q = Functions.polygon_fromJSON(o.getJSONObject(field.getName()).getJSONObject("content"));
									field.set(t, q);
									break;
								case "Noshape":
									field.set(t, Functions.noshape_fromJSON(o.getJSONObject(field.getName()).getJSONObject("content")));
									break;
							}
							break;
						default:
							System.out.println("Couldn't set "+field.getType()+" "+field.getName());
						
					}
				}
			}
			
			if(owner)
				Variables.player = (Player)t;
			
			
			return t;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		} catch (SecurityException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Shape circle_fromJSON(JSONObject o){
		try {
			Circle c = new Circle(o.getDouble("radius"));
			c.u.set(Mat2.fromJSON(o.getJSONObject("u")));
			return c;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static Shape field_fromJSON(JSONObject o){
		try {
			physics.Field c = new physics.Field(o.getDouble("radius"));
			c.u.set(Mat2.fromJSON(o.getJSONObject("u")));
			return c;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static Shape noshape_fromJSON(JSONObject o){
		try {
			NoShape c = new NoShape();
			c.u.set(Mat2.fromJSON(o.getJSONObject("u")));
			return c;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static Shape polygon_fromJSON(JSONObject o){
		Polygon p = new Polygon();
		try {
			int vertexCount = o.getInt("vertexCount");
			p.u.set(Mat2.fromJSON(o.getJSONObject("u")));
			for (int i = 0; i < vertexCount; i++){
				p.vertices[i].set(Vec2.fromJSON(o.getJSONObject("vertices").getJSONObject(Integer.toString(i))));
				p.normals[i].set(Vec2.fromJSON(o.getJSONObject("normals").getJSONObject(Integer.toString(i))));
			}
			p.vertexCount = vertexCount;

			return p;
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		return null;
	}
}
