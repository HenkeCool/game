package statics;

public class Config {
	public static int area_width = 20000;
	public static int area_height = 20000;
	public static double[] base_prices = {20, 10, 15, 5, 10, 7.5};
}
