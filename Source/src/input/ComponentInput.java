package input;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import statics.Settings;

public class ComponentInput extends ComponentAdapter{

	public void componentHidden(ComponentEvent e) {
	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentResized(ComponentEvent e) {
		Settings.HEIGHT = e.getComponent().getHeight();
		Settings.WIDTH = e.getComponent().getWidth();
	}

	public void componentShown(ComponentEvent e) {
	}

}
