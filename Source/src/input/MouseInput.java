package input;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;

import enums.GAMESTATE;
import game_objects.ForceField;
import game_objects.GameObject;
import game_objects.GravityField;
import game_objects.Lazer;
import game_objects.PointField;
import graphics_objects.Tracer;
import physics.Vec2;
import statics.Functions;import statics.Settings;
import statics.Variables;

public class MouseInput extends MouseAdapter{
	
	private ArrayList<Integer> buttons = new ArrayList<Integer>();
	private ArrayList<String> command = new ArrayList<String>();
	
	public void mouseClicked(MouseEvent e){
		e.consume();
		
		if(Variables.game_state == GAMESTATE.Game){
			for(int i = 0; i < this.buttons.size(); i++){
				if(this.buttons.get(i) == e.getButton()){
					Variables.console.execute(this.command.get(i));
				}
			}
		}
//			if(e.getButton() == MouseEvent.BUTTON1){
//				Variables.camera.un_track_object();
//				Variables.camera.go_to(Variables.camera.x + e.getX() - Config.WIDTH / 2, Variables.camera.y + e.getY() - Config.HEIGHT / 2);
//			}
//			else{
//				int n_lazers = 50;
//				for(int i = 0; i <= n_lazers; i++){
//					int angle = (360 / n_lazers) * i - 180;
//					Variables.handler.add_game_object(new Lazer(Variables.camera.x + e.getX() - Config.WIDTH / 2, Variables.camera.y + e.getY() - Config.HEIGHT / 2, 2, angle, 250, 100));
//				}
//			}
		/*
		if(Variables.game_state == GAMESTATE.Game){
			if(e.getButton() == MouseEvent.BUTTON1){
				PointField temp = new PointField(Variables.camera.x + e.getX() - Settings.WIDTH / 2, Variables.camera.y + e.getY() - Settings.HEIGHT / 2, 5);
				Variables.handler.add_game_object(temp);
				int i = 0;
				while(!temp.hitReady()){
					try {
						Thread.sleep(1);
						if(i > 50){
							Variables.handler.remove_game_object(temp);
							return;
						}
						i++;
					} catch (InterruptedException e1) {}
				}
				
				Variables.handler.add_game_graphics_object(new Tracer(Variables.player, ((PointField)Variables.handler.get_game_object(Variables.handler.get_game_object_position(temp))).getHit()));
				
			}*/
		
		Variables.menu_handler.on_click(e);
	}
	
	public void addButton(String str, String cmd){
		str = str.toLowerCase();
		int code;
		
		switch(str){
			case "mouse1":
				code = 1;
				break;
			case "mouse2":
				code = 3;
				break;
			case "mouse3":
				code = 2;
				break;
			case "mouse4":
				code = 4;
				break;
			case "mouse5":
				code = 5;
				break;
			default:
				return;
		}
		
		if(this.buttons.contains(code)){
			this.buttons.set(this.buttons.indexOf(code), code);
			this.command.set(this.buttons.indexOf(code), cmd);
		}else{
			this.buttons.add(code);
			this.command.add(cmd);			
		}

		System.out.println("The command '"+cmd+"' was bound to "+str);
	}
	
	public void mouseWheelMoved(MouseWheelEvent e){
		e.consume();
		Variables.menu_handler.on_scroll(e);
	}
}
