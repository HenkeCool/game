package boss_moves;

import enums.BOSSMOVES;
import passives.*;
import statics.Variables;

public class DreamWay extends BossMove{

	public DreamWay() {
		super(BOSSMOVES.DreamWay, 40, 500);
	}

	public void activate() {
		Variables.player.addPassive(new RocketBreakdown());
	}

	public void reset() {
		
	}

	public void remove() {
		
	}

}
