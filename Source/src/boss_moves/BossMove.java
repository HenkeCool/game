package boss_moves;

import enums.BOSSMOVES;

abstract public class BossMove {
	public double energy, time;
	public BOSSMOVES type;
	
	public BossMove(BOSSMOVES type, double e, double t){
		this.energy = e;
		this.type = type;
		this.time = t*60;
	}
	
	public void tick(double delta){
		this.time -= delta;

		if(this.time < 0)
			this.remove();
	}
	
	public double getEnergyRequired(){
		return this.energy;
	}
	
	public BOSSMOVES getType(){
		return this.type;
	}
	
	abstract public void activate();
	abstract public void reset();
	abstract public void remove();
}
