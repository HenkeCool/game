package boss_moves;

import java.util.ArrayList;

import bosses.Boss;
import enums.BOSSMOVES;
import enums.PASSIVES;
import game_objects.FollowingDropperField;
import statics.Variables;

public class Premutation extends BossMove{
	public Boss entity;
	public ArrayList<FollowingDropperField> fdf;
	public double deftime;
	
	public Premutation(Boss e, double time) {
		super(BOSSMOVES.Premutation, 10, time);
		this.entity = e;
		this.deftime = time*60;
		this.fdf = new ArrayList<FollowingDropperField>();
	}
	
	public void activate() {
		this.fdf.add(new FollowingDropperField(this.entity.get_x_cord(), this.entity.get_y_cord(), 50, 8, PASSIVES.Deteriorate, Variables.player));
		Variables.handler.add_game_object(this.fdf.get(this.fdf.size()-1));
	}

	public void reset() {
		this.time = this.deftime;
	}
	
	public void remove(){
		for(int i = 0; i < this.fdf.size(); i++)
			Variables.handler.remove_game_object(this.fdf.get(i));
		
		this.entity.removeActive();
	}
	
}
