package boss_moves;

import java.util.ArrayList;

import bosses.Boss;
import enums.BOSSMOVES;
import enums.PASSIVES;
import game_objects.FollowingDropperField;
import game_objects.PassiveFieldRemoverField;
import statics.Variables;

public class ExpandingParadox extends BossMove{
	public Boss entity;
	public ArrayList<PassiveFieldRemoverField> pfrf;
	public double deftime;
	
	public ExpandingParadox(Boss e, double time) {
		super(BOSSMOVES.Premutation, 10, time);
		this.entity = e;
		this.deftime = time*60;
		this.pfrf = new ArrayList<PassiveFieldRemoverField>();
	}
	
	public void activate() {
		this.pfrf.add(new PassiveFieldRemoverField(this.entity.get_x_cord(), this.entity.get_y_cord(), 500, 510));
		Variables.handler.add_game_object(this.pfrf.get(this.pfrf.size()-1));
	}

	public void reset() {
		this.time = this.deftime;
	}
	
	public void remove(){
		for(int i = 0; i < this.pfrf.size(); i++)
			Variables.handler.remove_game_object(this.pfrf.get(i));
		
		this.entity.removeActive();
	}
	
}
