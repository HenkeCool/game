package boss_moves;

import java.util.ArrayList;

import bosses.Boss;
import enums.BOSSMOVES;
import enums.PASSIVES;
import game_objects.Blob;
import game_objects.FollowingDropperField;
import game_objects.GameObject;
import game_objects.PassiveFieldRemoverField;
import statics.Variables;

public class BlobsOfRevenge extends BossMove{
	public Boss entity;
	public ArrayList<GameObject> b;
	public double deftime;
	
	public BlobsOfRevenge(Boss e, double time) {
		super(BOSSMOVES.BlobsOfRevenge, 10, time);
		this.entity = e;
		this.deftime = time*60;
		this.b = new ArrayList<GameObject>();
	}
	
	public void activate() {
		int frags = 3;
		for(int i = 0; i < frags; i++){
			int offset = 150;
			this.b.add(new Blob(this.entity.get_x_cord()+Math.cos((2*Math.PI/frags)*i)*offset, this.entity.get_y_cord()+Math.sin((2*Math.PI/frags)*i)*offset, 10, 250));
			Variables.handler.add_game_object(this.b.get(this.b.size()-1));
		}
	}

	public void reset() {
		this.time = this.deftime;
	}
	
	public void remove(){		
		this.entity.removeActive();
		for(GameObject i : this.b)
			Variables.handler.remove_game_object(i);
		
		this.b = new ArrayList<GameObject>();
	}
	
}
