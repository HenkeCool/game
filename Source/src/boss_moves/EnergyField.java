package boss_moves;

import java.util.ArrayList;

import bosses.Boss;
import enums.BOSSMOVES;
import enums.PASSIVES;
import game_objects.Blob;
import game_objects.FollowingDropperField;
import game_objects.GameObject;
import game_objects.GravityField;
import game_objects.PassiveFieldRemoverField;
import physics.Vec2;
import statics.Variables;

public class EnergyField extends BossMove{
	public Boss entity;
	public ArrayList<GameObject> b;
	public double deftime, z;
	public int frags, offset;
	
	public EnergyField(Boss e, double time) {
		super(BOSSMOVES.BlobsOfRevenge, 10, time);
		this.entity = e;
		this.deftime = time*60;
		this.b = new ArrayList<GameObject>();
	}
	
	public EnergyField(Boss e) {
		super(BOSSMOVES.BlobsOfRevenge, 10, -1);
		this.entity = e;
		this.deftime = -1;
		this.b = new ArrayList<GameObject>();
		this.activate();
	}
	
	public void tick(double delta){
		if(this.deftime >= 0)
			super.tick(delta);
		
		for(int i = 0; i < this.frags; i++){
			if(this.b.isEmpty())
				return;
			
			this.b.get(i).set_x_cord(this.entity.get_x_cord()+Math.cos((2*Math.PI/this.frags)*i+z)*this.offset);
			this.b.get(i).set_y_cord(this.entity.get_y_cord()+Math.sin((2*Math.PI/this.frags)*i+z)*this.offset);
		}
		this.z += 0.05;
	}
	
	public void activate() {
		this.frags = 3;
		for(int i = 0; i < this.frags; i++){
			this.offset = 150;
			this.b.add(new GravityField(this.entity.get_x_cord()+Math.cos((2*Math.PI/this.frags)*i)*this.offset, this.entity.get_y_cord()+Math.sin((2*Math.PI/this.frags)*i)*this.offset, 60, 250, new Vec2(2, 0), this.entity));
			Variables.handler.add_game_object(this.b.get(this.b.size()-1));
		}
	}

	public void reset() {
		this.time = this.deftime;
	}
	
	public void remove(){		
		this.entity.removeActive();
		
		for(GameObject i : this.b)
			Variables.handler.remove_game_object(i);
		
		this.b = new ArrayList<GameObject>();
	}
	
}
