package boss_moves;

import java.util.ArrayList;

import bosses.Boss;
import enums.BOSSMOVES;
import enums.PASSIVES;
import game_objects.Blob;
import game_objects.CheckField;
import game_objects.FollowingDropperField;
import game_objects.GameObject;
import game_objects.PassiveFieldRemoverField;
import statics.Variables;

public class BlobsCheck extends BossMove{
	public Boss entity;
	public ArrayList<GameObject> b;
	public double deftime;
	public CheckField cf;
	
	public BlobsCheck(Boss e, double time) {
		super(BOSSMOVES.BlobsCheck, 10, time);
		this.entity = e;
		this.deftime = time*60;
		this.b = new ArrayList<GameObject>();
	}
	
	public void activate() {
		this.cf = new CheckField(this.entity.get_x_cord(), this.entity.get_y_cord(), 200);
		Variables.handler.add_game_object(this.cf);
	}

	public void reset() {
		this.time = this.deftime;
	}
	
	public void remove(){
		if(this.b.isEmpty())
			return;
		
		this.b = this.cf.getHits();
		System.out.println(this.b.size());
		for(int i = 0; i < this.b.size(); i++){
			if(this.b.get(i) instanceof Blob){
				this.entity.damage_multiplier += 1;
				Variables.handler.remove_game_object(this.b.get(i));
			}
		}
		
		this.entity.removeActive();
	}
	
}
