package passives;

import java.awt.Color;
import java.util.ArrayList;

import enums.PASSIVES;
import game_objects.GameObject;
import graphics_objects.Tracer;
import physics.Vec2;
import statics.Variables;

public class Autoplay extends Passive{
	private int i = 0;
	private Tracer trace;
	
	public Autoplay() {
		super(PASSIVES.Autoplay, 999999, true);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
		

		ArrayList<GameObject> temp = Variables.console.getObjects("Asteroid#nearest");
		
		Variables.player.lazerdamage = 1000;
		
		if(temp.size() < 1){
			Variables.player.accelerate_stop();
			Variables.player.shoot_stop();
			Variables.player.turn_left_stop();
			Variables.player.turn_right_stop();
		}
		
		for(int i = 0; i < temp.size(); i++){	
			if(temp.get(i).position.x > 10000 || temp.get(i).position.y > 10000)
				break;
			
			double x = Variables.player.get_x_cord()-temp.get(i).position.x;
			double y = Variables.player.get_y_cord()-temp.get(i).position.y;
			
			double force_x = temp.get(i).get_x_vel();
			double force_y = temp.get(i).get_y_vel();

			double offset = Variables.player.position.distance(temp.get(i).position)/Variables.player.lazerspeed;
			
			x -= (offset*force_x);
			y -= (offset*force_y);
			
			double orient = Math.atan2(-y, -x);
			double turn_angle = Variables.player.orient-orient;
			
			if(turn_angle < -Math.PI)
				turn_angle = Math.abs(turn_angle);
			
			if(turn_angle > 0){
				Variables.player.turn_left_stop();
				Variables.player.turn_right_start();
			}else if(turn_angle < 0){
				Variables.player.turn_right_stop();
				Variables.player.turn_left_start();
			}else{}
			
			
			Variables.handler.remove_game_graphics_object(this.trace);
			this.trace = new Tracer(Variables.player.position, Variables.player.position.add(new Vec2(-x, -y)), Color.red);
			if(Variables.player.position.distance(temp.get(i).position) > 1000){
				Variables.player.accelerate_start();
				Variables.player.shoot_stop();
			}else{
				Variables.player.accelerate_stop();
				Variables.player.shoot_start();
			}
			Variables.handler.add_game_graphics_object(this.trace);
		}
	}

	public void remove(){
		Variables.handler.remove_game_graphics_object(this.trace);
		Variables.player.accelerate_stop();
		Variables.player.shoot_stop();
		Variables.player.turn_left_stop();
		Variables.player.turn_right_stop();
	}
	
}
