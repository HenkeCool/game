package passives;

import enums.PASSIVES;
import statics.Variables;

public class Corruption extends Passive{

	public Corruption() {
		super(PASSIVES.Corruption, 10, false);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
	}

	public void remove(){
		super.remove();
		Variables.player.health -= 20;
	}
	
}
