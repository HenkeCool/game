package passives;

import enums.PASSIVES;
import statics.Variables;

public abstract class Passive {
	public PASSIVES type;
	public boolean buff;
	public double duration;
	
	
	public Passive(PASSIVES n, double d, boolean b){
		this.type = n;
		this.duration = d*60;
		this.buff = b;
	}
	
	abstract public void tick(double delta);

	public String getName(){
		return this.type.name();
	}

	public PASSIVES getType(){
		return this.type;
	}
	
	public double getDuration(){
		return this.duration;
	}
	
	public boolean isBuff(){
		return this.buff;
	}

	public void remove(){
		Variables.player.removePassive(this.type);
	}
	
}
