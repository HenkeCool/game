package passives;

import enums.PASSIVES;
import physics.Vec2;
import statics.Variables;

public class Boost extends Passive{

	public Boost() {
		super(PASSIVES.Boost, 0.5, true);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
		
		Variables.player.applyForce(Variables.player.shape.u.mul(new Vec2(Variables.player.mass * 2.5, 0f)));
	}

	public void remove(){
		super.remove();
		Variables.player.addPassive(new DeBoost());
	}
	
}
