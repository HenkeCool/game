package passives;

import enums.PASSIVES;
import physics.Vec2;
import statics.Variables;

public class Regenerate extends Passive{

	public Regenerate() {
		super(PASSIVES.Regenerate, 10, true);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
		
		Variables.player.health += 1 * delta * 0.01;
	}

}
