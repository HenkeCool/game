package passives;

import enums.PASSIVES;

public class DeBoost extends Passive{

	public DeBoost() {
		super(PASSIVES.DeBoost, 10, false);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
	}

}
