package passives;

import enums.PASSIVES;
import game_objects.Lazer;
import game_objects.Rocket;
import statics.Variables;

public class RocketBreakdown extends Passive{
	private double speed;
	private double zspeed;
	
	
	public RocketBreakdown() {
		super(PASSIVES.RocketBreakdown, 2, false);
		this.speed = Variables.player.accelerate_speed;
		this.zspeed = Variables.player.turning_speed;
		System.out.println("Before: "+this.speed + " : "+this.zspeed);
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
		
		Variables.player.accelerate_speed = 0.08;
		Variables.player.turning_speed = 0.008;
		
	}
	
	public void remove(){
		Variables.player.accelerate_speed = this.speed;
		Variables.player.turning_speed = this.zspeed;
		System.out.println("After: "+Variables.player.accelerate_speed + " : "+Variables.player.turning_speed);
		super.remove();
	}
}
