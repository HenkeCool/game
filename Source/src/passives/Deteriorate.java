package passives;

import java.util.Random;

import enums.PASSIVES;
import statics.Variables;

public class Deteriorate extends Passive{
	private Random r;
	private int t;
	
	public Deteriorate(){
		super(PASSIVES.Deteriorate, 50, false);
		this.r = new Random();
		this.t = 0;
	}

	public void tick(double delta) {
		if(this.duration > 0)
			this.duration = this.duration - delta;
		else
			this.remove();
		if(this.t > 60){
			if((this.r.nextInt(10)+1) == 1){
				Variables.player.addPassive(new Corruption());
				this.remove();
			}
			this.t = 0;
		}
		
		Variables.player.health -= 0.01666666666;
		this.t++;
	}
}
