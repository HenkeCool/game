package enums;

public enum PASSIVES {
	Boost,
	DeBoost,
	Corruption,
	Regenerate,
	Deteriorate,
	RocketBreakdown,
	Autoplay;
}
