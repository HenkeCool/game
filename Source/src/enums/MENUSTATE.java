package enums;

public enum MENUSTATE {
	Pause,
	Menu,
	Connect,
	Settings,
	Console,
	Game;
}
