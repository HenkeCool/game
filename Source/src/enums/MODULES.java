package enums;

public enum MODULES {
	Booster,
	Disspell,
	PlanetDestroyer,
	PlanetMagnet,
	GroupTeleport,
	HeavyShield,
	Gathering;
}
