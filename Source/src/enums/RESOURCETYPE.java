package enums;

public enum RESOURCETYPE {
	hydrogen,
	carbon,
	oxygen,
	iron,
	gold,
	uranium;
	
	public double getDensity(){
		switch(ordinal()){
		case 1:
			return 0.0084;
		case 2:
			return .351;
		case 3:
			return 0.0133;
		case 4:
			return 0.787;
		case 5:
			return 1.932;
		case 6:
			return 1.897;
		}
		return 0;
	}
}
