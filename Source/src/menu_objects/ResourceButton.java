package menu_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.net.URISyntaxException;

import enums.RESOURCETYPE;
import game_objects.Planet;
import statics.Variables;

public class ResourceButton extends Button {
	public RESOURCETYPE resource;
	private Planet planet;
	
	public NumberInput input;
	public TextButton accept;
	public TextButton name;
	public HorizontalBox sub_box;
	public HorizontalBox box;

	public ResourceButton(double x_cord, double y_cord, double width, double height, Color color, RESOURCETYPE resource, Planet planet) {
		super(x_cord, y_cord, width, height, color);
		
		this.planet = planet;
		this.resource = resource;
		
		input = new NumberInput(0, 0, width * 6 / 9 - 10, height - 10, Color.gray, 2, 15f, 25);
		accept = new TextButton(0, 0, width / 9 - 10, height - 10, "Trade", Color.gray, 15f);
		name = new TextButton(0, 0, width / 3 - 10, height - 10, resource.name(), new Color(0, 0, 0, 0), 20f);
		sub_box = new HorizontalBox(0, 0, 0, 0, new Color(0,0,0,0), new Color(0,0,0,0));
		box = new HorizontalBox(x_cord, y_cord, 5, 0, color, color);
		
		sub_box.add(input);
		sub_box.add(accept);
		box.add(name);
		box.add(sub_box);
	}

	public void tick(double delta) {
		box.tick(delta);
	}

	public void render(Graphics g) {
		box.render(g);
	}
	
	public void set_x_cord(double x){
		super.set_x_cord(x);
		box.set_x_cord(x);
	}
	
	public void set_y_cord(double y){
		super.set_y_cord(y);
		box.set_y_cord(y);
	}
	
	public void on_click(MouseEvent e){
		if(input.occupies(e.getX(), e.getY())){
			input.on_click(e);
		}
		if(accept.occupies(e.getX(), e.getY())){
			if(make_trade())
				input.clear();
		}
	}

	public void on_key(KeyEvent e) {
		if(KeyEvent.VK_ENTER == e.getKeyCode()){
			if(make_trade()){
				input.clear();
			}
		}
		else{
			input.on_key(e);
		}
	}

	private boolean make_trade(){
		double amount;
		try{
			amount = Double.parseDouble(input.text);
		}
		catch(NumberFormatException e){
			return false;
		}
		if(amount > planet.get_max_trade(resource)){
			return false;
		}
		if(amount > Variables.player.resources[resource.ordinal()]){
			return false;
		}
		
		planet.trade(resource, amount);
		return true;
	}
	
	public void on_scroll(MouseWheelEvent e) {

	}

	public boolean contains(MenuObject o) {
		if(box.contains(o))return true;
		return false;
	}
}
