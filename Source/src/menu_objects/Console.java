package menu_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.json.JSONException;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import bosses.Zuldrak;
import core.ServerConnector;
import enums.GAMESTATE;
import enums.MENUSTATE;
import enums.PASSIVES;
import game_objects.Asteroid;
import game_objects.ForceField;
import game_objects.GameObject;
import game_objects.GravityField;
import game_objects.Lazer;
import game_objects.PassiveField;
import game_objects.Planet;
import graphics_objects.Tracer;
import modules.Module;
import passives.Passive;
import physics.Circle;
import physics.Polygon;
import physics.Vec2;
import statics.Config;
import statics.Functions;
import statics.Settings;
import statics.Variables;

public class Console extends MenuObject{
	private ArrayList<String> commands;
	private ArrayList<String> alias_command;
	private ArrayList<String> alias;
	private ArrayList<Tracer> tracers;
	private int position;
	public TextBox box;
	public TextInput text;
	public boolean cheats = false;
	public String[] client_only_commands = {"loadui", "bind", "savebinds", "alias", "load"};
	private VerticalBox scroll;
	
	public Console(double x_cord, double y_cord, double width, double box_height, double text_height, int padding) {
		super(x_cord, y_cord, width + padding * 2, box_height + text_height + padding * 4);
		this.scroll = new VerticalBox(x_cord, y_cord, 9999, padding, 0, new Color(0, 0, 0, 0), new Color(0, 0, 0, 0));
		this.box = new TextBox(0, 0, width, box_height, Color.gray, 0, 10f, 300);
		this.text = new TextInput(0, 0, width, text_height, Color.gray, 0, 10f, 300);
		scroll.add(box);
		scroll.add(text);
		this.commands = new ArrayList<String>();
		this.alias_command = new ArrayList<String>();
		this.alias = new ArrayList<String>();
		this.position = 0;
		this.tracers = new ArrayList<Tracer>();
		this.load("default.cfg");
	}

	public void execute(String c){
		this.execute(c, false);
	}
	
	public void execute(String c, boolean fromconsole){
		this.execute(c, fromconsole, false);
	}
	
	public void execute(String c, boolean fromconsole, boolean internal){
		if(c.equals(""))
			return;
		
		boolean isClientCommand = false;
		String[] t = c.split(" ");
		t[0] = t[0].toLowerCase();
		
		if(!t[0].equals("bind") && !t[0].equals("alias")){
			String[] doublecmd = c.split(";");
			if(doublecmd.length > 1){
				for(int i = 0; i < doublecmd.length; i++){
					this.execute(doublecmd[i]);
				}
				return;
			}
		}
		
		for(int z = 0; z < this.client_only_commands.length; z++)
			if(this.client_only_commands[z].equals(t[0]))
				isClientCommand = true;
		
		if(Variables.server != null && Variables.server.isConnected() && !t[0].equals("rcon") && !fromconsole && !isClientCommand){
			if(!this.getCommands(c).equals("")){
				if(this.alias.indexOf(c) != -1){
					String[] ctemp = this.getCommands(c).split(";");
					for(int x = 0; x < ctemp.length; x++)
						Variables.server.sendCommand(ctemp[x]);
				}
			}else
				Variables.server.sendCommand(c);
			return;
		}
		
		if(t[0].equals("cheats")){
			if(t[1].equals("1") && !this.cheats){
				this.cheats = true;
				this.box.addText("Cheats activated");
			}else if(t[1].equals("0") && this.cheats){
				this.cheats = false;
				this.box.addText("Cheats deactivated");
			}
			return;
		}
		
		for(int i = 0; i < this.alias.size(); i++){
			if(t[0].equals(this.alias.get(i))){
				this.execute(this.alias_command.get(i));
				return;
			}
		}
		
		switch(t[0]){
			case "alias":
				this.alias.add(t[1]);
				String command = "";
				for(int i = 2; i < t.length; i++){
					command += t[i]+" ";
				}
				this.alias_command.add(command);
				System.out.println("The command "+command+" was renamed to "+t[1]);
				break;
			case "toggleon":
				this.toggleon(t[1]);
				break;
			case "toggleoff":
				this.toggleoff(t[1]);
				break;
			case "toggle":
				this.toggle(t[1]);
				break;
			case "bind":
				this.bind(t);
				break;
			case "savebinds":
				this.savebinds(t[1]);
				break;
			case "load":
				this.load(t[1]);
				break;
			case "loadui":
				this.loadui(t[1]);
				break;
			case "module":
				Variables.player.activateModule(Integer.parseInt(t[1]));
				break;
			case "addmodule":
				if(this.cheats || internal){
					this.addmodule(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "removemodule":
				if(this.cheats || internal){
					Variables.player.removeModule(Integer.parseInt(t[1]));
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "addpassive":
				if(this.cheats || internal){
					this.addpassive(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "removepassive":
				if(this.cheats || internal){
					Variables.player.removePassive(PASSIVES.valueOf(t[1]));
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "tp":
				if(this.cheats || internal){
					this.teleport(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "give":
				if(this.cheats || internal){
					this.give(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "add":
				if(this.cheats || internal){
					this.add(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "remove":
				if(this.cheats || internal){
					this.remove(t[1]);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "dev":
				if(this.cheats || internal){
					this.dev(t);
				}else{
					box.addText("Error cheats deactivated! Cant perform: "+c);
					return;
				}
				break;
			case "conf":
				this.conf(t);
				break;
			case "set":
				this.set(t);
				break;
			case "var":
				this.var(t);
				break;
			case "tracer":
				this.tracer(t);
				break;
			case "cleartracers":
				this.cleartracers();
				break;
			case "connect":
				String[] temp = t[1].split(":");
				Variables.server = new ServerConnector(temp[0], Integer.parseInt(temp[1]));	
				new Thread(Variables.server).start();
				break;
			case "rcon":
				c = c.substring(5);
				try{
					Variables.server.sendRconCommand(c);
				} catch (NullPointerException e){
					this.box.addText("Error you are not connected to any server!");
				}
				break;
			case "echo":
				c = c.substring(5);
				this.box.addText(c);
				break;
			case "resetgraphic":
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++)
					if(Variables.handler.get_game_object(i) instanceof Planet)
						((Planet)Variables.handler.get_game_object(i)).resetGraphic();
				break;
			case "openshop":
				if(Variables.game_state == GAMESTATE.Game){
					ArrayList<GameObject> shop = this.getObjects("Planet#nearest");
					((Planet)shop.get(0)).open_trade();
					Variables.game_state = GAMESTATE.Paused;
				}
				break;
			default:
				this.box.addText("Error command "+c+" not found");
				break;
		}
	}
	
	private void addpassive(String[] o) {
		try {
			Class<Passive> A = (Class<Passive>) Class.forName("passives."+o[1]);
			Variables.player.addPassive(A.newInstance());
		} catch (ClassNotFoundException e) {
			this.box.addText("Error passive not found!");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	private void cleartracers() {
		for(int i = 0; i < this.tracers.size(); i++){
			Variables.handler.remove_game_graphics_object(this.tracers.get(i));
		}
	}

	private void tracer(String[] o) {
		ArrayList<GameObject> A = this.getObjects(o[1]);
		for(int i = 0; i < A.size(); i++){
			Tracer temp = new Tracer(Variables.player, A.get(i));
			this.tracers.add(temp);
			Variables.handler.add_game_graphics_object(temp);
		}
	}

	private void loadui(String o) {
		MENUSTATE temp = Enum.valueOf(MENUSTATE.class, o);
		if(Variables.menu_handler.previous_state.peek().equals(temp)){
			Variables.game_state = GAMESTATE.Game;
			Variables.menu_handler.previous_state.pop();
			Variables.menu_handler.load_lists(Variables.menu_handler.previous_state.pop());
		}else{
			Variables.game_state = GAMESTATE.Paused;
			Variables.menu_handler.load_lists(temp);
		}
	}

	private void savebinds(String o) {
		try { 
			String[] file = o.split("\\.");
			
			String tempEx = String.join(".", file);;
			
			if(!file[file.length-1].equals("cfg")){
				tempEx += ".cfg";
			}
			
			o = tempEx;
			String[] z = Variables.key_input.getBinds(); 
			
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(o));
			for(int i = 0; i < z.length; i++){
				outputWriter.write(z[i]);
				outputWriter.newLine();
			}
			
			outputWriter.flush();
			outputWriter.close();
			this.box.addText("Saved keybindings to "+o);
		} catch (IOException e) {}
	}

	private void toggle(String c) {
		switch(c){
			case "forward":
				if(Variables.player.accelerating)
					Variables.player.accelerate_stop();
				else
					Variables.player.accelerate_start();
				break;
			case "left":
				if(Variables.player.turning_left)
					Variables.player.turn_left_stop();
				else
					Variables.player.turn_left_start();
				break;
			case "right":
				if(Variables.player.turning_right)
					Variables.player.turn_left_stop();
				else
					Variables.player.turn_right_start();
				break;
			case "shoot":
				if(Variables.player.shooting)
					Variables.player.shoot_stop();
				else
					Variables.player.shoot_start();
				break;
			default:
				break;
		}
	}

	private void toggleoff(String c) {
		switch(c){
			case "forward":
				Variables.player.accelerate_stop();
				break;
			case "left":
				Variables.player.turn_left_stop();
				break;
			case "right":
				Variables.player.turn_right_stop();
				break;
			case "shoot":
				Variables.player.shoot_stop();
				break;
			default:
				break;
		}	
	}

	private void toggleon(String c) {
		switch(c){
			case "forward":
				Variables.player.accelerate_start();
				break;
			case "left":
				Variables.player.turn_left_start();
				break;
			case "right":
				Variables.player.turn_right_start();
				break;
			case "shoot":
				Variables.player.shoot_start();
				break;
			default:
				break;
		}
	}
	
	private void set(String[] o) {
		try {
			Field[] fields = Settings.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Settings.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Settings.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Settings.class, o[2]);
							break;
					}
					this.box.addText("Settings."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void var(String[] o) {
		try {
			Field[] fields = Variables.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Variables.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Variables.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Variables.class, o[2]);
							break;
					}
					this.box.addText("Variables."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void conf(String[] o) {
		try {
			Field[] fields = Config.class.getFields();
			for(int i = 0; i < fields.length; i++){
				if(fields[i].getName().equals(o[1])){
					switch(fields[i].getType().toString()){
						case "int":
							fields[i].setInt(Config.class, Integer.parseInt(o[2]));
							break;
						case "boolean":
							fields[i].setBoolean(Config.class, Boolean.parseBoolean(o[2]));
							break;
						case "String":
							fields[i].set(Config.class, o[2]);
							break;
					}
					this.box.addText("Config."+fields[i].getName()+" was set to "+o[2]);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void addmodule(String[] o) {
		try {
			Class<Module> A = (Class<Module>) Class.forName("modules."+o[1]);
			Variables.player.addModule(A.getDeclaredConstructor(int.class).newInstance(Integer.parseInt(o[2])));
		} catch (ClassNotFoundException e) {
			this.box.addText("Error object not found!");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	private void bind(String[] o){
		String cmd = "";
		for(int i = 2; i < o.length; i++){
			cmd += o[i]+" ";
		}
		
		cmd = cmd.substring(0, cmd.length()-1);
		
		Variables.key_input.addKey(o[1], cmd);
	}
	
	private void load(String o){
		try {
			o = "cfg/"+o;
			String[] file = o.split("\\.");
			
			String tempEx = String.join(".", file);;
			
			if(!file[file.length-1].equals("cfg")){
				tempEx += ".cfg";
			}
			
			o = tempEx;
			
			Scanner z = new Scanner(new File(o));
			while (z.hasNextLine()) {
				String[] temp = z.nextLine().split(" ");
				if(temp[0].equals("bind")){
					this.bind(temp);
				}else{
					this.execute(String.join(" ", temp));
				}
			}
			z.close();
		} catch (FileNotFoundException e){
			this.box.addText("Config file '"+o+"' not found");
		}
	}
	
	public ArrayList<GameObject> getObjects(String o){
		ArrayList<GameObject> A = new ArrayList<GameObject>();
		
		try {
			String[] prefixarr = o.split("-");
			if(prefixarr.length > 1){
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
					A.add(Variables.handler.get_game_object(i));
					
					if(Integer.parseInt(prefixarr[0]) <= i && i <= Integer.parseInt(prefixarr[1]))
						System.out.println("Found "+i);
					else
						A.remove(A.indexOf(Variables.handler.get_game_object(i)));
				}
			}else if(o.equals("nearest")){
				double len = 500000;
				for(int i = 0; i < Variables.handler.get_game_object_size(); i++){

					if(Variables.handler.get_game_object_position(Variables.player) != i){
						A.add(Variables.handler.get_game_object(i));
				
						if(len > Variables.player.position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position)){
							len = Variables.player.position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position);
							GameObject B = A.get(A.indexOf(Variables.handler.get_game_object(i)));
							A = new ArrayList<GameObject>();
							A.add(B);
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}	
					}
				}
			}else{
				A.add(Variables.handler.get_game_object(Integer.parseInt(o)));
			}
			return A;
	    } catch(NumberFormatException e){}catch(NullPointerException e){}
	    
		if(A.size() < 1){
			String[] prefix = o.split("#");
			if(prefix.length > 1){
				o = prefix[0];
				String[] prefixarr = prefix[1].split("-");
				
				if(prefix[1].equals("nearest")){
					double len = 500000;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o)){
							if(len > Variables.player.position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position)){
								len = Variables.player.position.distance(A.get(A.indexOf(Variables.handler.get_game_object(i))).position);
								GameObject B = A.get(A.indexOf(Variables.handler.get_game_object(i)));
								A = new ArrayList<GameObject>();
								A.add(B);
							}else{
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
							}
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else if(prefixarr.length > 1){
					int index = 0;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o)){
							index++;
							if(Integer.parseInt(prefixarr[0]) <= index && index <= Integer.parseInt(prefixarr[1])){}
							else
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}else{
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else if(prefixarr.length < 2){
					int pre = Integer.parseInt(prefix[1]);
					int index = 0;
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
					
						if(A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o))
							index++;
							if(index != pre)
								A.remove(A.indexOf(Variables.handler.get_game_object(i)));
						}
					}
					return A;
				}else{
					for(int i = 0; i < Variables.handler.get_game_object_size(); i++){
						A.add(Variables.handler.get_game_object(i));
				
						if(!A.get(A.indexOf(Variables.handler.get_game_object(i))).getClass().toString().equals("class game_objects."+o))
							A.remove(A.indexOf(Variables.handler.get_game_object(i)));
					}
					return A;
				}
			}
		return A;
	}
	
	private void dev(String[] o) {
		String command = "";
		ArrayList<GameObject> A = this.getObjects(o[1]);
		try {
		    
			if(o.length < 3)
				this.box.addText("Showing variables for "+A.get(0).getClass().getSimpleName());
			
			for(int x = 0; x < A.size(); x++){
				Field[] fields = A.get(x).getClass().getFields();
				for(int c = 0; c < fields.length; c++){
					if(o.length < 3){
						this.box.addText("- "+fields[c].getName());
					}else{
						if(fields[c].getName().equals(o[2])){
							System.out.println(fields[c].getType().toString());
							switch(fields[c].getType().toString()){
								case "double":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setDouble(A.get(x), Double.parseDouble(o[3]));
									break;
								case "float":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setFloat(A.get(x), Float.parseFloat(o[3]));
									break;
								case "int":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setInt(A.get(x), Integer.parseInt(o[3]));
									break;
								case "boolean":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to "+o[3];
									fields[c].setBoolean(A.get(x), Boolean.parseBoolean(o[3]));
									break;
								case "class java.awt.Color":
									if(o.length < 7){
										fields[c].set(A.get(x), new Color(Float.parseFloat(o[3]), Float.parseFloat(o[4]), Float.parseFloat(o[5])));
										command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to R:"+o[3]+" G:"+o[4]+" B:"+o[5];
									}else{
										command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to R:"+o[3]+" G:"+o[4]+" B:"+o[5]+" A:"+o[6];
										fields[c].set(A.get(x), new Color(Float.parseFloat(o[3]), Float.parseFloat(o[4]), Float.parseFloat(o[5]), Float.parseFloat(o[6])));
									}
									break;
								case "class physics.Vec2":
									command = A.get(x).getClass().getSimpleName()+"s "+fields[c].getName()+" has been set to X:"+o[3]+" Y:"+o[4];
									fields[c].set(A.get(x), new Vec2(Double.parseDouble(o[3]), Double.parseDouble(o[4])));
									break;
									
								default:
									command = "Error couldn't find "+o[2]+" in "+o[1]+"!";
									break;
							}
						}
					}
				}
			}

			if(o.length < 3)
				this.box.addText("");
			else
				this.box.addText(command);
			
		} catch (Exception e) {
			this.box.addText("Error object not found!");
		}
	}

	public void add(String[] o){
		if(o.length < 3){
			box.addText("Error command not completed, usage: 'add object x y'");
			return;
		}
	/*
		try {
			Class<GameObject> A = (Class<GameObject>) Class.forName("game_objects."+o[1]);
			Class[] x = A.getConstructors()[0].getParameterTypes();
			Class[] fields = new Class[x.length-1];
			Field[] args = new Field[x.length-1];
			for(int i = 0; i < x.length; i++){
				switch(x[i].getName()){
					case "int":
						fields[i] = int.class;
//						args[i] = o[i+1];
						break;
					case "":
						
				}
			}
		} catch (ClassNotFoundException e) {
			this.box.addText("Error object "+o[1]+" not found");
		}*/
		
		
		if(o[2].equals("player")){
			String[] t = o;
			if(o.length > 3){
				o = new String[5];
				o[4] = t[3];
			} else
				o = new String[4];
			o[1] = t[1];
			o[2] = String.valueOf(Variables.player.get_x_cord());
			o[3] = String.valueOf(Variables.player.get_y_cord());
		}
		
		if(o.length < 4){
			box.addText("Error command not completed, usage: 'add object x y'");
			return;
		}
		
		switch(o[1]){
			case "lazer":
				Variables.handler.add_game_object(new Lazer(Variables.player, Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0.0, 0.0, 100, 0));
				box.addText("Spawned lazer on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "ff":
				Variables.handler.add_game_object(new ForceField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, new Vec2(50.0, 0.0)));
				box.addText("Spawned forcefield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "gf":
				Variables.handler.add_game_object(new GravityField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, new Vec2(1, 0.0)));
				box.addText("Spawned gravityfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "hf":
				Variables.handler.add_game_object(new PassiveField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, PASSIVES.Regenerate));
				box.addText("Spawned healingfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "bf":
				Variables.handler.add_game_object(new PassiveField(Double.parseDouble(o[2]), Double.parseDouble(o[3]), Double.parseDouble(o[4]), 50, PASSIVES.Boost));
				box.addText("Spawned boostfield on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "zuldrak":
				Variables.handler.add_game_object(new Zuldrak(Double.parseDouble(o[2]), Double.parseDouble(o[3])));
				this.box.addText("Spawned boss on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "asteroid":
				Vec2[] verts2 = Vec2.arrayOf( 20 );
				for (int i = 0; i < 20; i++)
				{
					verts2[i].set( Functions.random( -40, 40 ), Functions.random( -40, 40 ) );
				}
				Variables.handler.add_game_object(new Asteroid(Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0, 0, 0, new Polygon(verts2)));
				this.box.addText("Spawned asteroid on x: "+o[2]+" y: "+o[3]);
				break;
				
			case "planet":
				Variables.handler.add_game_object(new Planet(Double.parseDouble(o[2]), Double.parseDouble(o[3]), 0, new Circle(75)));
				this.box.addText("Spawned planet on x: "+o[2]+" y: "+o[3]);
				break;
				
			default:
				this.box.addText("Error object "+o[1]+" not found");
				break;
		}
	}
	
	private void give(String[] o) {
		if(!this.cheats){
			this.box.addText("Error cheats deactivated! Cant perform: "+o);
			return;
		}
		
		if(o.length < 3){
			box.addText("Error command not completed, usage: 'give item amount'");
			return;
		}
			
		switch(o[1]){
			case "hydrogen":
				Variables.player.resources[0] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" hydrogen to player");
				break;
			case "carbon":
				Variables.player.resources[1] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" carbon to player");
				break;
			case "oxygen":
				Variables.player.resources[2] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" oxygen to player");
				break;
			case "iron":
				Variables.player.resources[3] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" iron to player");
				break;
			case "gold":
				Variables.player.resources[4] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" gold to player");
				break;
			case "uranium":
				Variables.player.resources[5] += Double.parseDouble(o[2]);
				box.addText("Added "+o[2]+" uranium to player");
				break;
			default:
				this.box.addText("Error item "+o[1]+" not found");
				break;
		}
	}
	
	public void teleport(String[] o){
		if(!this.cheats){
			this.box.addText("Error cheats deactivated! Cant perform: "+o);
			return;
		}
		
		if(o.length < 3){
			this.box.addText("Error command not completed, usage: 'tp x y'");
			return;
		}
		
		Variables.player.set_x_cord(Double.parseDouble(o[1]));
		Variables.player.set_y_cord(Double.parseDouble(o[2]));
	}
	
	public void remove(String p){
		if(!this.cheats){
			this.box.addText("Error cheats deactivated! Cant perform: "+p);
			return;
		}
		
	}

	public void tick(double delta) {
		this.scroll.tick(delta);
	}

	public void render(Graphics g) {
		this.scroll.render(g);
	}
	
	public void set_x_cord(double x){
		super.set_x_cord(x);
		scroll.update();
	}
	
	public void set_y_cord(double y){
		super.set_y_cord(y);
		scroll.update();
	}

	public void on_click(MouseEvent e) {
		Variables.menu_handler.focus_item = scroll;
	}

	public void on_key(KeyEvent e) {
		if(KeyEvent.VK_ENTER == e.getKeyCode()){
			this.execute(this.text.getString(), true);
			this.commands.add(this.text.getString());
			this.position++;
			this.text.clear();
		}
		else{
			if(e.getKeyCode() == 38){
				if(this.position != 0){
					this.text.text = this.commands.get(this.position-1);
					this.position--;
				}
			}else if(e.getKeyCode() == 40){
				if(this.commands.size() > this.position+1){
					this.text.text = this.commands.get(this.position+1);
					this.position++;
				}else if(this.position == this.commands.size()-1){
					this.text.text = "";
					this.position++;
				}
			}else{
				this.text.on_key(e);
			}
		}
	}
	
	public String getCommands(String alias){
		for(int i = 0; i < this.alias.size(); i++){
			if(this.alias.get(i).equals(alias)){
				return this.alias_command.get(i).substring(1, this.alias_command.get(i).length()-1);
			}
		}
		
		return "";
	}

	public void on_scroll(MouseWheelEvent e) {
		this.box.on_scroll(e);
	}

	public boolean contains(MenuObject o) {
		if(o == this || o == this.box || o == this.text)return true;
		return false;
	}
}
