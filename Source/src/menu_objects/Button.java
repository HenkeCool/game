package menu_objects;
import java.awt.Color;

public abstract class Button extends MenuObject{
	protected Color color;
	
	public Button(double x_cord, double y_cord, double width, double height, Color color){
		super(x_cord, y_cord, width, height);
		this.color = color;
	}
}
