package menu_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import statics.Settings;
import statics.Variables;

public abstract class InputArea extends MenuObject {
	protected Color color;
	protected double border;
	protected float fontSize;
	protected String text;
	protected int i;
	protected int maxLength;
	
	public InputArea(double x_cord, double y_cord, double width, double height, Color color, double border, float fontSize, int maxLength){
		super(x_cord, y_cord, width, height);
		this.color = color;
		this.border = border;
		this.fontSize = fontSize;
		this.text = "";
		this.i = 0;
		this.maxLength = maxLength;
	}

	public void tick(double delta) {
		i++;
		if(i > 80)i = 0;
	}

	public void render(Graphics g) {
		Color temp = color;
		g.setColor(this.color.darker());
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2)), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2)), (int)this.get_width(), (int)this.get_height());
		
		if(Variables.menu_handler.focus_item != null){
			if(Variables.menu_handler.focus_item.contains(this))temp = color.brighter();
		}
		g.setColor(temp);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2) + this.border), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2) + this.border), (int)(this.get_width() -  2 * this.border), (int)(this.get_height() - 2 * this.border));
		
		g.setColor(Color.black);
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(this.fontSize);
		g.setFont(newFont);
		
		int stringWidth = g.getFontMetrics().stringWidth(text);
		
		g.drawString(this.text, (int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 10), (int)(Settings.HEIGHT / 2 + this.get_y_cord() + this.fontSize / 4));
		if(Variables.menu_handler.focus_item == this && i > 40){
			g.drawLine((int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 12 + stringWidth), (int)(Settings.HEIGHT / 2 + this.get_y_cord() - this.fontSize / 2 - 1), (int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 12 + stringWidth), (int)(Settings.HEIGHT / 2 + this.get_y_cord() + fontSize / 2 - 1));
		}
	}

	public void on_click(MouseEvent e) {
		Variables.menu_handler.focus_item = this;
	}

	public void on_key(KeyEvent e){
		text = decodeKey(text, e);
	}

	protected abstract String decodeKey(String text, KeyEvent e);

	public void on_scroll(MouseWheelEvent e) {
		
	}

	public String getString(){
		return text;
	}
	
	public void clear(){
		text = "";
	}

	public boolean contains(MenuObject o) {
		if(o == this)return true;
		return false;
	}
}