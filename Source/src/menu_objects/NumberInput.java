package menu_objects;

import java.awt.Color;
import java.awt.event.KeyEvent;

public class NumberInput extends InputArea {

	public NumberInput(double x_cord, double y_cord, double width, double height, Color color, double border, float fontSize, int maxLength) {
		super(x_cord, y_cord, width, height, color, border, fontSize, maxLength);
	}
	
	protected String decodeKey(String text, KeyEvent e){
		if(KeyEvent.VK_BACK_SPACE == e.getKeyCode()){
			if (text != null && text.length() > 0) {
				text = text.substring(0, text.length()-1);
		    }
		    return text;
		}
		
		if(!Character.isDigit(e.getKeyChar()))return text;
		
		if(text.length() < maxLength){
			text += e.getKeyChar();
		}
		return text;
	}
}
