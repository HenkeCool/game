package menu_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.LinkedList;

import statics.Settings;

public class VerticalBox extends MenuObject {
	
	int scroll_length;
	
	private LinkedList<MenuObject> objects;
	private LinkedList<Double> offsets;
	private int padding;
	private int border;
	private Color color;
	private Color border_color;
	private int offset;
	private double max_height;
	public double inner_height;
	public MenuObject active_item;
	
	
	public VerticalBox(double x, double y, double max_height, int padding, int border, Color color, Color border_color){
		super(x, y, 0, 0);
		objects = new LinkedList<MenuObject>();
		offsets = new LinkedList<Double>();
		this.padding = padding;
		this.border = border;
		this.color = color;
		this.border_color = border_color;
		this.max_height = max_height;
		
		this.scroll_length = 10;
		this.set_width(2 * padding + 2 * border);
		this.set_height(2 * padding + 2 * border);
		this.inner_height = 2 * padding + 2 * border;
		this.offset = 0;
	}
	
	public void tick(double delta) {
		for(MenuObject o: objects){
			o.tick(delta);
		}
	}

	public void render(Graphics g){		
		g.setColor(border_color);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2)), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (get_height() / 2)), (int)this.get_width(), (int)get_height());
		
		g.setColor(color);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2) + border), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (get_height() / 2) + border), (int)this.get_width() - 2 * border, (int)get_height() - 2 * border);
		
		Rectangle clip = g.getClipBounds();
		g.setClip(clip.intersection(new Rectangle((int)(get_x_cord() - get_width() / 2 + Settings.WIDTH / 2 + border), (int)(get_y_cord() - get_height() / 2 + Settings.HEIGHT / 2 + border), (int)(get_width() - border * 2),  (int)(get_height() - border * 2))));
		
		for(MenuObject o: objects){
			o.render(g);
		}
		
		g.setClip(clip);
	}
	
	public void set_x_cord(double x){
		super.set_x_cord(x);
		re_position_objects();
	}
	
	public void set_y_cord(double y){
		super.set_y_cord(y);
		re_position_objects();
	}

	public void set_width(double width) {
		super.set_width(width);
		re_position_objects();
	}
	
	public void set_height(double height) {
		super.set_height(height);
		re_position_objects();
	}
	
	public void set_max_height(double height) {
		max_height = height;
		re_position_objects();
	}
	
	public void add(MenuObject o){
		double y_offset = 0;
		double widest = 0;
		
		for(MenuObject ob: objects){
			y_offset += ob.get_height() + padding;
			if(ob.get_width() > widest){
				widest = ob.get_width();
			}
		}
		
		if(o.get_width() > widest)widest = o.get_width();
		
		set_height(get_height() + (o.get_height() + padding));
		inner_height += o.get_height() + padding;
		set_width(widest + border * 2 + padding * 2);
		
		if(get_height() > max_height){
			set_height(max_height);
		}
		
		objects.add(o);
		offsets.add(y_offset);
		
		re_position_objects();
	}
	
	public void re_position_objects(){
		for(int i = 0; i < objects.size(); i++){
			MenuObject temp = objects.get(i);
			
			temp.set_x_cord(get_x_cord());
			temp.set_y_cord(get_y_cord() - (get_height() - padding - border) / 2 + temp.get_height() / 2 + padding + offsets.get(i) + offset);
		}
	}
	
	public void update(){
		LinkedList<MenuObject> temp = (LinkedList<MenuObject>) objects.clone();
		objects.clear();
		this.set_width(2 * padding + 2 * border);
		this.set_height(2 * padding + 2 * border);
		this.inner_height = 2 * padding + 2 * border;
		for(MenuObject o:temp){
			add(o);
		}
	}

	public void on_click(MouseEvent e){
		for(int i = objects.size() - 1; i >= 0; i--){
			MenuObject tempObject = objects.get(i);
			if(tempObject.occupies(e.getX(), e.getY())){
				active_item = tempObject;
				tempObject.on_click(e);
				break;
			}
		}
	}

	public void on_key(KeyEvent e) {
		if(active_item != null){
			active_item.on_key(e);
		}
	}

	public void on_scroll(MouseWheelEvent e) {
		for(int i = objects.size() - 1; i >= 0; i--){
			MenuObject tempObject = objects.get(i);
			if(tempObject.occupies(e.getX(), e.getY())){
				tempObject.on_scroll(e);
				break;
			}
		}
		
		if(get_height() == inner_height) return;
		
		offset += e.getWheelRotation() * scroll_length * -1;
		
		if(offset > 0){
			offset = 0;
		}
		
		if(offset < (max_height - inner_height)){
			offset = (int) (max_height - inner_height);
		}
		
		re_position_objects();
	}

	public boolean contains(MenuObject o){
		if(o == this)return true;
		for(MenuObject ob:objects){
			if(o == ob || ob.contains(o))return true;
		}
		return false;
	}
}
