package menu_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

import statics.Settings;
import statics.Variables;

public class TextBox extends MenuObject {
	private Color color;
	private double border;
	private float fontSize;
	private ArrayList<String> text;
	private int i;
	private int maxLength;
	private int items;
	private int position;
	
	public TextBox(double x_cord, double y_cord, double width, double height, Color color, double border, float fontSize, int maxLength){
		super(x_cord, y_cord, width, height);
		this.color = color;
		this.border = border;
		this.fontSize = fontSize;
		this.text = new ArrayList<String>();
		this.i = 0;
		this.maxLength = maxLength;
		this.position = 0;
		this.items = (int)((this.get_height() - 20)/10);
	}

	public void tick(double delta) {
		i++;
		if(i > 80)i = 0;
	}

	public void render(Graphics g) {
		Color temp = color;
		g.setColor(this.color.darker());
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2)), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2)), (int)this.get_width(), (int)this.get_height());
		
		if(Variables.menu_handler.focus_item != null){
			if(Variables.menu_handler.focus_item.contains(this))temp = color.brighter();
		}
		g.setColor(temp);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2) + this.border), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2) + this.border), (int)(this.get_width() -  2 * this.border), (int)(this.get_height() - 2 * this.border));
		
		g.setColor(Color.black);
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(this.fontSize);
		g.setFont(newFont);
		
		List<String> z;
		
		if(this.text.size() > this.items){
			z = this.text.subList(this.text.size()-this.items, this.text.size());
		}else
			z = this.text;

		int diff = this.text.size();
		
		if(this.position > 0 || this.text.size() > this.items){
			if(this.position + this.items != this.text.size()){
				diff = this.position + this.items;
			}
		}
		
		if(this.position < 0)
			this.position = 0;
		
		int k = 0;
		for(int i = this.position; i < diff; i++){
			if(this.text.get(i) == null)
				return;
			
			int stringWidth = g.getFontMetrics().stringWidth(this.text.get(i));
			
			String str = "";
			
			if(stringWidth < (this.get_width()-30))
				str = this.text.get(i);
			else{
				for(int c = 0; c < this.text.get(i).length(); c++){
					stringWidth = g.getFontMetrics().stringWidth(str);
					if(stringWidth > (this.get_width()-30))
						break;
					str = str + this.text.get(i).charAt(c);
				}
			}
			
			g.drawString(str, (int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2) + this.border) + 10, (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2) + this.border) + 18 + k * newFont.getSize());
			
			if(Variables.menu_handler.active_item.contains(this) && k > 40){
				g.drawLine((int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 12 + stringWidth), (int)(Settings.HEIGHT / 2 + this.get_y_cord() - this.fontSize / 2 - 1), (int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 12 + stringWidth), (int)(Settings.HEIGHT / 2 + this.get_y_cord() + fontSize / 2 - 1));
			}
			k++;
		}/*
		if(z.size() > 1)
			g.drawString(""+g.getFontMetrics().stringWidth(z.get(z.size()-1)), (int)(width/2), 10);
		*/
	}

	public void on_click(MouseEvent e) {
		
	}

	public void on_key(KeyEvent e){
		
	}

	public void on_scroll(MouseWheelEvent e) {
		if(e.getWheelRotation() == -1){
			if(this.position > 0)
				this.position = this.position + e.getWheelRotation();
		}else{
			if(this.position+this.items < this.text.size())
				this.position = this.position + e.getWheelRotation();
		}
	}
	
	public void addText(String t){
		this.text.add(t);
		if(this.position > 0)
			this.position++;
		else
			this.position = this.text.size()-this.items;
		
		if(this.position < 0)
			this.position = 0;
	}

	public String getString(){
		return text.get(text.size()-1);
	}
	
	public void clear(){
		text.clear();
	}

	public boolean contains(MenuObject o) {
		if(o == this)return true;
		return false;
	}
}
