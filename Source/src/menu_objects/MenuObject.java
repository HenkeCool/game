package menu_objects;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import statics.Settings;

public abstract class MenuObject {
	private double x_cord, y_cord;
	private double width, height;
	
	public abstract void tick(double delta);
	public abstract void render(Graphics g);
	public abstract void on_click(MouseEvent e);
	public abstract void on_key(KeyEvent e);
	public abstract void on_scroll(MouseWheelEvent e);
	public abstract boolean contains(MenuObject o);
	
	public MenuObject(double x_cord, double y_cord, double width, double height){
		this.x_cord = x_cord;
		this.y_cord = y_cord;
		this.width = width;
		this.height = height;
	}
	
	public void set_x_cord(double x){
		this.x_cord = x;
	}
	
	public void set_y_cord(double y){
		this.y_cord = y;
	}
	
	public double get_x_cord(){
		return this.x_cord;
	}
	
	public double get_y_cord(){
		return this.y_cord;
	}
	
	public double get_width() {
		return width;
	}
	
	public void set_width(double width) {
		this.width = width;
	}
	
	public double get_height() {
		return height;
	}
	
	public void set_height(double height) {
		this.height = height;
	}
	
	public boolean occupies(int x_cord, int y_cord) {
		if(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 < x_cord && x_cord < (Settings.WIDTH / 2 + this.get_x_cord() + this.get_width() / 2) && Settings.HEIGHT / 2 + this.get_y_cord() - this.get_height() / 2 < y_cord && y_cord < (Settings.HEIGHT / 2 + this.get_y_cord() + this.get_height() / 2)) return true;
		else return false;
	}
}
