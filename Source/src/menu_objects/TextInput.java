package menu_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;

import statics.Settings;
import statics.Variables;

public class TextInput extends InputArea {
	public TextInput(double x_cord, double y_cord, double width, double height, Color color, double border, float fontSize, int maxLength) {
		super(x_cord, y_cord, width, height, color, border, fontSize, maxLength);
	}

	protected String decodeKey(String text, KeyEvent e){
        
		if ((e.getKeyCode() == KeyEvent.VK_V) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
			try {
				String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
				return text+data;
			} catch (HeadlessException e1) {
				e1.printStackTrace();
			} catch (UnsupportedFlavorException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} 
        }
        
		if(KeyEvent.VK_BACK_SPACE == e.getKeyCode()){
			if (text != null && text.length() > 0) {
				text = text.substring(0, text.length()-1);
		    }
		    return text;
		}
		
		if(e.getKeyChar() == ' ')return text = text + " ";
		if(e.getKeyChar() == '.')return text = text + ".";
		if(e.getKeyChar() == '/')return text = text + "/";
		if(e.getKeyChar() == '\\')return text = text + "\\";
		if(e.getKeyChar() == '#')return text = text + "#";
		if(e.getKeyChar() == ';')return text = text + ";";
		if(e.getKeyChar() == '-')return text = text + "-";
		if(e.getKeyChar() == ':')return text = text + ":";
		if(e.getKeyChar() == '_')return text = text + "_";
		if(!Character.isLetterOrDigit(e.getKeyChar()))return text;
		
		if(text.length() < maxLength){
			text += e.getKeyChar();
		}
		return text;
	}
}