package menu_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import statics.Settings;

public class TextButton extends Button {
	private String text;
	private float fontSize;

	public TextButton(double x_cord, double y_cord, double width, double height, String text, Color color, float fontSize) {
		super(x_cord, y_cord, width, height, color);
		this.text = text;
		this.fontSize = fontSize;
	}

	public void tick(double delta){

	}

	public void render(Graphics g){
		g.setColor(this.color);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2)), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2)), (int)this.get_width(), (int)this.get_height());
		g.setColor(Color.black);
		
		Font currentFont = g.getFont();
		Font newFont = currentFont.deriveFont(this.fontSize);
		g.setFont(newFont);
		g.drawString(this.text, (int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2 + 10), (int)(Settings.HEIGHT / 2 + this.get_y_cord() + this.fontSize / 4));
	}

	public void on_click(MouseEvent e) {
		
	}

	public void on_key(KeyEvent e) {
		
	}
	
	public void on_scroll(MouseWheelEvent e) {
		
	}

	public boolean contains(MenuObject o) {
		if(o == this)return true;
		return false;
	}
}
