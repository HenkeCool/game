package menu_objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import statics.Settings;

public class MenuOverlay extends MenuObject {
	private Color color;
	private double opacity;

	public MenuOverlay(Color color, double d) {
		super(0, 0, Settings.WIDTH, Settings.HEIGHT);
		this.color = color;
		this.opacity = d;
	}

	public void tick(double delta) {
		
	}

	public void render(Graphics g) {
		g.setColor(new Color(this.color.getRed(), this.color.getGreen(), this.color.getBlue(), (int)(255 * this.opacity)));
		g.fillRect(0, 0, Settings.WIDTH, Settings.HEIGHT);
	}
	
	public void on_click(MouseEvent e) {
		
	}

	public void on_key(KeyEvent e) {
		
	}
	
	public void on_scroll(MouseWheelEvent e) {
		
	}

	public boolean contains(MenuObject o) {
		if(o == this)return true;
		return false;
	}
}
