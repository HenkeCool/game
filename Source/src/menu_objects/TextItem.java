package menu_objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferStrategy;

import statics.Settings;
import statics.Variables;

public class TextItem extends Button {
	private String text;
	private int padding;
	private Color text_color;

	public TextItem(double x_cord, double y_cord, Color color, Color text_color, String text, int padding) {
		super(x_cord, y_cord, 0, 0, color);
		this.text = text;
		this.padding = padding;
		this.text_color = text_color;
		
		BufferStrategy bs = Variables.game.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		
		set_width((g.getFontMetrics().stringWidth(text)) * 1.56 + padding * 2);
		set_height(g.getFontMetrics().getHeight() + padding * 2);
	}

	public void tick(double delta) {
		
	}

	public void render(Graphics g) {
		g.setColor(this.color);
		g.fillRect((int)((Settings.WIDTH / 2) + this.get_x_cord() - (this.get_width() / 2)), (int)((Settings.HEIGHT / 2) + this.get_y_cord() - (this.get_height() / 2)), (int)this.get_width(), (int)this.get_height());
		
		g.setColor(this.text_color);
		g.drawString(this.text, (int)(Settings.WIDTH / 2 + this.get_x_cord() - this.get_width() / 2) + padding, (int)(Settings.HEIGHT / 2 + this.get_y_cord() + this.get_height() / 2) - padding);
	}

	public void on_click(MouseEvent e) {
		
	}

	public void on_key(KeyEvent e) {
		
	}

	public void on_scroll(MouseWheelEvent e) {
		
	}

	public boolean contains(MenuObject o) {
		if(o == this)return true;
		return false;
	}

}
