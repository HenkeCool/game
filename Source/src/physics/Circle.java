/*
    Copyright (c) 2013 Randy Gaul http://RandyGaul.net

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
      
    Port to Java by Philip Diffenderfer http://magnos.org
*/

package physics;

import org.json.JSONException;
import org.json.JSONObject;

public class Circle extends Shape
{

	public Circle(double r){
		radius = r;
	}

	public Shape clone(){
		return new Circle(radius);
	}

	public void initialize()
	{
		computeMass(game_object.density);
	}

	public void computeMass(double density)
	{
		game_object.mass = ImpulseMath.PI * radius * radius * density;
		game_object.invMass = (game_object.mass != 0.0) ? 1.0 / game_object.mass : 0.0;
		game_object.inertia = game_object.mass * radius * radius;
		game_object.invInertia = (game_object.inertia != 0.0) ? 1.0 / game_object.inertia : 0.0;
	}

	public void setOrient(double radians){
		
	}

	public Type getType(){
		return Type.Circle;
	}

	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		try {
			json.put("radius", radius);
			json.put("u", u.toJSON());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return json;
	}

}
