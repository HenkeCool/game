package physics;

import org.json.JSONException;
import org.json.JSONObject;

public class Field extends Shape{
	
	double radius;
	
	public Field(double radius){
		this.radius = radius;
	}
	
	public Shape clone() {
		return new Field(radius);
	}

	public void initialize()
	{
		computeMass(game_object.density);
	}

	public void computeMass(double density)
	{
		game_object.mass = ImpulseMath.PI * radius * radius * density;
		game_object.invMass = (game_object.mass != 0.0) ? 1.0 / game_object.mass : 0.0;
		game_object.inertia = game_object.mass * radius * radius;
		game_object.invInertia = (game_object.inertia != 0.0) ? 1.0 / game_object.inertia : 0.0;
	}

	public void setOrient(double radians) {
		
	}

	public Type getType() {
		return Type.Field;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("radius", radius);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}

}
