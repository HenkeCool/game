package physics;

import org.json.JSONObject;

public class NoShape extends Shape{

	public NoShape(){
	}

	@Override
	public Shape clone(){
		return new NoShape();
	}

	@Override
	public void initialize() {
	}

	@Override
	public void computeMass(double density) {
	}

	@Override
	public void setOrient(double radians) {
	}

	@Override
	public Type getType() {
		return Type.None;
	}

	@Override
	public JSONObject toJSON() {
		return new JSONObject();
	}

}
