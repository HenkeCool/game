package physics;

import game_objects.GameObject;

public class CollisionCircleField implements CollisionCallback 
{

	public static final CollisionCircleField instance = new CollisionCircleField();
	
	public void handleCollision(Manifold m, GameObject a, GameObject b) 
	{
		CollisionFieldCircle.instance.handleCollision(m, b, a); 
		
		if (m.contactCount > 0)
		{
			m.normal.negi();
		}
	}

}
