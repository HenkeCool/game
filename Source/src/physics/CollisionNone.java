package physics;

import game_objects.GameObject;

public class CollisionNone implements CollisionCallback {
	
	public static final CollisionNone instance = new CollisionNone();
	
	public void handleCollision(Manifold m, GameObject a, GameObject b) {
		return;
	}

}
