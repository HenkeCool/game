package physics;

import game_objects.GameObject;

public class CollisionPolygonField implements CollisionCallback 
{

	public static final CollisionPolygonField instance = new CollisionPolygonField();
	
	public void handleCollision(Manifold m, GameObject a, GameObject b) 
	{
		CollisionFieldPolygon.instance.handleCollision(m, b, a); 
		
		if (m.contactCount > 0)
		{
			m.normal.negi();
		}
	}

}
