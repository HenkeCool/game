package bosses;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import boss_moves.BossMove;
import boss_moves.ExpandingParadox;
import enums.BOSSES;
import game_objects.GameObject;
import game_objects.PassiveFieldRemoverField;
import physics.Circle;
import physics.Shape;
import statics.Variables;

abstract public class Boss extends GameObject{

	public BOSSES type;
	public int max_energy, index;
	public double energy, increase, damage_multiplier;
	
	public ArrayList<BossMove> moves = new ArrayList<BossMove>();
	public BossMove finalmove, activemove, passivemove;
	public Random r;
	
	public Boss(BOSSES b, double x_cord, double y_cord, double radius, int hp, int sp, BossMove finalmove) {
		super(x_cord, y_cord, 0, 0, 0, 0, 0, new Circle(radius));
		this.type = b;
		this.health = hp;
		this.max_energy = sp;
		this.energy = 0;
		this.finalmove = finalmove;
		this.r = new Random();
		this.damage_multiplier = 1;
		this.passivemove = null;
	}
	
	public void finalMove(){
		if(this.energy >= this.max_energy){
			this.finalmove.activate();
			this.energy = 0;
		}
	}
	
	public void tick(double delta){
		if(this.health < 0)
			this.remove();

		if(this.activemove != null)
			this.activemove.tick(delta);
		
		if(this.passivemove != null)
			this.passivemove.tick(delta);
	}

	public BOSSES getType(){
		return this.type;
	}
	
	public double getEnergy(){
		return this.energy;
	}
	
	public double getHealth(){
		return this.health;
	}
	
	public void activateMove(int i){
		this.moves.get(i).reset();
		this.moves.get(i).activate();
		this.activemove = this.moves.get(i);
		if(this.moves.size()-1 <= i)
			this.index = 0;
		else
			this.index = i+1;
	}
	
	public void removeActive(){
		this.activemove = null;
	}
	
	public ArrayList<BossMove> getMoves(){
		return this.moves;
	}
	
	public void addMove(BossMove m){
		this.moves.add(m);
	}
	
	public void addMove(BossMove[] m){
		for(int i = 0; i < m.length; i++){
			this.moves.add(m[i]);
		}
	}
	
	public void remove(){
		Variables.handler.add_game_object(new PassiveFieldRemoverField(this.get_x_cord(), this.get_y_cord(), this.shape.radius, 500));
		for(BossMove i : this.moves)
			i.remove();
		
		if(this.passivemove != null){
			this.passivemove.remove();
		}

		Variables.handler.remove_game_object(this);
	}
	
	abstract protected void setupMoves();
}
