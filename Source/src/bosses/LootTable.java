package bosses;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

import enums.BOSSES;
import game_objects.GameObject;

public class LootTable {

	private JSONObject table;
	
	public LootTable(BOSSES bossname){
		this.table = new JSONObject();
		try {
			String temp = "";
			Scanner z = new Scanner(new File("bosses/"+bossname.name()+"/loot.json"));

			while (z.hasNextLine()) {
				temp += z.nextLine();
			}
			this.table = new JSONObject(temp);
			System.out.println("Loaded loottable: "+this.table.toString());
		}catch(FileNotFoundException e){this.table=new JSONObject();}catch(JSONException e){this.table=new JSONObject();}
	}
	
	public JSONObject getTable(){
		return this.table;
	}
	
	public void roll(GameObject p){
		
	}
}
