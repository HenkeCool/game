package bosses;

import java.awt.Graphics;

import org.json.JSONObject;

import boss_moves.BossMove;
import enums.BOSSES;
import game_objects.GameObject;

/**
 * Peter ska vara en boss som är liten i början och får mer och mer health vilket leder till större massa (radie).
 * Tillslut får han en så stor massa (inte för stor) så att han imploderar till ett svart hål.
 * Där blir Peter mycket mer impulsiv och hatar sitt liv därför suger han.
 */

public class Peter extends Boss{

	public Peter(BOSSES b, double x_cord, double y_cord, double radius, int hp, int sp, BossMove finalmove) {
		super(b, x_cord, y_cord, radius, hp, sp, finalmove);
		// TODO Auto-generated constructor stub
	}

	protected void setupMoves() {
		
	}

	public void render(Graphics g) {
		
	}

	public void hit(GameObject object) {
		
	}

	public JSONObject toJSON() {
		return null;
	}

}
