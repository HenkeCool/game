package bosses;

import java.awt.Color;
import java.awt.Graphics;

import org.json.JSONException;
import org.json.JSONObject;

import boss_moves.*;
import enums.BOSSES;
import game_objects.GameObject;
import game_objects.Lazer;
import game_objects.PassiveFieldRemoverField;
import graphics_objects.HealthBar;
import statics.Functions;
import statics.Variables;

public class Zuldrak extends Boss{
	private int t = 0;
	private PassiveFieldRemoverField pfrf;
	private LootTable loottable;
	
	public int perm = 0;
	public boolean hardmode = false;
	
	protected void setupMoves(){
		this.passivemove = new EnergyField(this);
		addMove(new BlobsOfRevenge(this, 10));
		addMove(new BlobsCheck(this, 0));
		System.out.println("List loaded containing "+this.moves.size()+" moves");
	}
	
	public Zuldrak(double x, double y) {
		super(BOSSES.Zuldrak, x, y, 50, 100000, 10000, new DreamWay());
		this.increase = 12;
		Variables.handler.add_game_graphics_object(new HealthBar(this, 100, Color.green));
		this.loottable = new LootTable(BOSSES.Zuldrak);	
		
		setupMoves();
	}

	public void tick(double delta) {
		super.tick(delta);
		
		if(this.t > 30){
			double ang;
			
			if(this.hardmode){
				double x = Variables.player.get_x_cord()-this.position.x;
				double y = Variables.player.get_y_cord()-this.position.y;
				
				double force_x = -Variables.player.get_x_vel();
				double force_y = -Variables.player.get_y_vel();
	
				double offset = Variables.player.position.distance(this.position)/10;
				
				x -= (offset*force_x);
				y -= (offset*force_y);
				
				ang = Math.atan2(y, x);
				Variables.handler.add_game_object(new Lazer(this, get_x_cord()+Math.cos(ang), get_y_cord()+Math.sin(ang), 10, ang, 100, 120*(int)this.damage_multiplier));
			}else{
				ang = Math.atan2(Variables.player.get_y_cord()-this.get_y_cord(), Variables.player.get_x_cord()-this.get_x_cord());
				Variables.handler.add_game_object(new Lazer(this, get_x_cord()+Math.cos(ang), get_y_cord()+Math.sin(ang), 10, ang, 100, 120*(int)this.damage_multiplier));
			}
			
			if(this.activemove == null){
				finalMove();
				for(int i = this.index; i < this.moves.size(); i++){
					if(this.moves.get(i).getEnergyRequired() < this.getEnergy()){
						System.out.println("Activating: "+this.moves.get(i).getType().name()+" it will be removed in "+this.moves.get(i).time/60+"s");
						this.activateMove(i);
						break;
					}
				}
				this.energy += this.increase;

			}
			this.t = 0;
		}

		this.t++;
	}

	public void render(Graphics g) {
		g.setColor(Color.magenta);
		int real_x = Functions.game_x_cord(get_x_cord());
		int real_y = Functions.game_y_cord(get_y_cord());
		g.drawOval(real_x - (int)this.shape.radius, real_y - (int) this.shape.radius, (int) this.shape.radius * 2, (int) this.shape.radius * 2);
		g.drawString("Damage multiplier: "+this.damage_multiplier, real_x, real_y);
	}

	public void hit(GameObject object) {
		if(object instanceof Lazer){
			if(!((Lazer) object).entity.equals(this)){
				this.health -= ((Lazer)object).damage;
				if(this.health < 0){
					this.loottable.roll(((Lazer)object).entity);
				}
			}
		}
	}
	
	public JSONObject toJSON(){
		return toJSON(this);
	}
}
